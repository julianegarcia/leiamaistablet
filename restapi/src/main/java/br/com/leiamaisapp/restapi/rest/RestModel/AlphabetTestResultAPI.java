package br.com.leiamaisapp.restapi.rest.RestModel;

import org.parceler.Parcel;

import br.com.leiamaisapp.database.Model.AlphabetTestResult;

/**
 * Created by erickson on 20/11/16.
 */

@Parcel
public class AlphabetTestResultAPI extends TestResultAPI {
    String letter;

    public void convertFromAlphabetTestResult(AlphabetTestResult result) {
        setTag(result.getTag());
        setIdBase(result.getIdBase());
        setStart(result.getStart());
        setHit(result.isHit());
        setLetter(result.getLetter());
        setStop(result.getStop());
    }

    public AlphabetTestResult convertToAlphabetTestResult() {
        AlphabetTestResult result = new AlphabetTestResult();

        result.setTag(getTag());
        result.setIdBase(getIdBase());
        result.setStart(getStart());
        result.setHit(isHit());
        result.setLetter(getLetter());
        result.setStop(getStop());

        return result;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }
}
