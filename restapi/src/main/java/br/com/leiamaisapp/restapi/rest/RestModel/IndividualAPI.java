package br.com.leiamaisapp.restapi.rest.RestModel;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.Date;

import br.com.leiamaisapp.database.Model.Individual;

/**
 * Created by erickson on 19/11/16.
 */

@Parcel
public class IndividualAPI implements Serializable {

    @SerializedName("_id")
    String idBase;
    @SerializedName("createdBy")
    String createdBy;
    @SerializedName("lastUpdate")
    Date lastUpdate;
    @SerializedName("photo")
    String photo;
    @SerializedName("name")
    String name;
    @SerializedName("birthday")
    Date birthday;
    @SerializedName("sex")
    String sex;
    @SerializedName("cpf")
    String cpf;
    @SerializedName("parentCpf")
    String parentCpf;
    @SerializedName("rg")
    String rg;
    @SerializedName("motherName")
    String motherName;
    @SerializedName("fatherName")
    String fatherName;
    @SerializedName("telephone")
    String telephone;
    @SerializedName("address")
    String address;
    @SerializedName("email")
    String email;
    @SerializedName("familyHistoric")
    String familyHistoric;
    @SerializedName("consanguinityHistoric")
    String consanguinityHistoric;
    @SerializedName("desiredGestation")
    String desiredGestation;
    @SerializedName("pregnancyComplications")
    String pregnancyComplications;
    @SerializedName("maternalHistoric")
    String maternalHistoric;
    @SerializedName("neonatalComplications")
    String neonatalComplications;
    @SerializedName("comorbidities")
    String comorbidities;
    @SerializedName("grade")
    String grade;
    @SerializedName("schoolSupport")
    String schoolSupport;
    @SerializedName("tdhaTreatment")
    String tdhaTreatment;
    @SerializedName("regularSchool")
    String regularSchool;
    @SerializedName("speakProblem")
    String speakProblem;

    @SerializedName("individualExclusion")
    IndividualExclusionAPI individualExclusion;
    @SerializedName("individualTDHA")
    IndividualTDHAAPI individualTDHA;

    public void convertFromIndividual(Individual individual) {
        setIdBase(individual.getIdBase());
        setCreatedBy(individual.getCreatedBy());
        setLastUpdate(individual.getLastUpdate());
        setPhoto(individual.getPhoto());
        setName(individual.getName());
        setBirthday(individual.getBirthday());
        setSex(individual.getSex());
        setCpf(individual.getCpf());
        setParentCpf(individual.getParentCpf());
        setRg(individual.getRg());
        setMotherName(individual.getMotherName());
        setFatherName(individual.getFatherName());
        setTelephone(individual.getTelephone());
        setAddress(individual.getAddress());
        setEmail(individual.getEmail());
        setFamilyHistoric(individual.getFamilyHistoric());
        setConsanguinityHistoric(individual.getConsanguinityHistoric());
        setDesiredGestation(individual.getDesiredGestation());
        setPregnancyComplications(individual.getPregnancyComplications());
        setMaternalHistoric(individual.getMaternalHistoric());
        setNeonatalComplications(individual.getNeonatalComplications());
        setComorbidities(individual.getComorbidities());
        setGrade(individual.getGrade());
        setSchoolSupport(individual.getSchoolSupport());
        setTdhaTreatment(individual.getTdhaTreatment());
        setRegularSchool(individual.getRegularSchool());
        setSpeakProblem(individual.getSpeakProblem());

        IndividualExclusionAPI exclusionAPI = new IndividualExclusionAPI();
        exclusionAPI.convertFromIndividualExclusion(individual.getIndividualExclusion());
        setIndividualExclusion(exclusionAPI);

        IndividualTDHAAPI individualTDHAAPI = new IndividualTDHAAPI();
        individualTDHAAPI.convertFromIndividualTDHA(individual.getIndividualTDHA());
        setIndividualTDHA(individualTDHAAPI);
    }

    public Individual convertToIndividual() {
        Individual individual = new Individual();

        individual.setIdBase(getIdBase());
        individual.setCreatedBy(getCreatedBy());
        individual.setLastUpdate(getLastUpdate());
        individual.setPhoto(getPhoto());
        individual.setName(getName());
        individual.setBirthday(getBirthday());
        individual.setSex(getSex());
        individual.setCpf(getCpf());
        individual.setParentCpf(getParentCpf());
        individual.setRg(getRg());
        individual.setMotherName(getMotherName());
        individual.setFatherName(getFatherName());
        individual.setTelephone(getTelephone());
        individual.setAddress(getAddress());
        individual.setEmail(getEmail());
        individual.setFamilyHistoric(getFamilyHistoric());
        individual.setConsanguinityHistoric(getConsanguinityHistoric());
        individual.setDesiredGestation(getDesiredGestation());
        individual.setPregnancyComplications(getPregnancyComplications());
        individual.setMaternalHistoric(getMaternalHistoric());
        individual.setNeonatalComplications(getNeonatalComplications());
        individual.setComorbidities(getComorbidities());
        individual.setGrade(getGrade());
        individual.setSchoolSupport(getSchoolSupport());
        individual.setTdhaTreatment(getTdhaTreatment());
        individual.setRegularSchool(getRegularSchool());
        individual.setSpeakProblem(getSpeakProblem());

        individual.setIndividualExclusion(getIndividualExclusion().convertToIndividualExclusion());
        individual.setIndividualTDHA(getIndividualTDHA().convertToIndividualTDHA());

        return individual;
    }

    public String getIdBase() {
        return idBase;
    }

    public void setIdBase(String idBase) {
        this.idBase = idBase;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getParentCpf() {
        return parentCpf;
    }

    public void setParentCpf(String parentCpf) {
        this.parentCpf = parentCpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFamilyHistoric() {
        return familyHistoric;
    }

    public void setFamilyHistoric(String familyHistoric) {
        this.familyHistoric = familyHistoric;
    }

    public String getConsanguinityHistoric() {
        return consanguinityHistoric;
    }

    public void setConsanguinityHistoric(String consanguinityHistoric) {
        this.consanguinityHistoric = consanguinityHistoric;
    }

    public String getDesiredGestation() {
        return desiredGestation;
    }

    public void setDesiredGestation(String desiredGestation) {
        this.desiredGestation = desiredGestation;
    }

    public String getPregnancyComplications() {
        return pregnancyComplications;
    }

    public void setPregnancyComplications(String pregnancyComplications) {
        this.pregnancyComplications = pregnancyComplications;
    }

    public String getMaternalHistoric() {
        return maternalHistoric;
    }

    public void setMaternalHistoric(String maternalHistoric) {
        this.maternalHistoric = maternalHistoric;
    }

    public String getNeonatalComplications() {
        return neonatalComplications;
    }

    public void setNeonatalComplications(String neonatalComplications) {
        this.neonatalComplications = neonatalComplications;
    }

    public String getComorbidities() {
        return comorbidities;
    }

    public void setComorbidities(String comorbidities) {
        this.comorbidities = comorbidities;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSchoolSupport() {
        return schoolSupport;
    }

    public void setSchoolSupport(String schoolSupport) {
        this.schoolSupport = schoolSupport;
    }

    public String getTdhaTreatment() {
        return tdhaTreatment;
    }

    public void setTdhaTreatment(String tdhaTreatment) {
        this.tdhaTreatment = tdhaTreatment;
    }

    public String getRegularSchool() {
        return regularSchool;
    }

    public void setRegularSchool(String regularSchool) {
        this.regularSchool = regularSchool;
    }

    public String getSpeakProblem() {
        return speakProblem;
    }

    public void setSpeakProblem(String speakProblem) {
        this.speakProblem = speakProblem;
    }

    public IndividualExclusionAPI getIndividualExclusion() {
        return individualExclusion;
    }

    public void setIndividualExclusion(IndividualExclusionAPI individualExclusion) {
        this.individualExclusion = individualExclusion;
    }

    public IndividualTDHAAPI getIndividualTDHA() {
        return individualTDHA;
    }

    public void setIndividualTDHA(IndividualTDHAAPI individualTDHA) {
        this.individualTDHA = individualTDHA;
    }
}
