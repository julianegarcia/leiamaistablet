package br.com.leiamaisapp.restapi.rest.RestModel;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.leiamaisapp.database.Model.Individual;

/**
 * Created by erickson on 19/11/16.
 */

@Parcel
public class GetIndividualsReturn implements Serializable {
    @SerializedName("total")
    int total;
    @SerializedName("filtred")
    int filtred;
    @SerializedName("individuals")
    List<IndividualAPI> individuals;

    public List<Individual> convertToIndividuals() {
        List<Individual> individuals = new ArrayList<>();

        for (IndividualAPI individualAPI : getIndividuals()) {
            individuals.add(individualAPI.convertToIndividual());
        }

        return individuals;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getFiltred() {
        return filtred;
    }

    public void setFiltred(int filtred) {
        this.filtred = filtred;
    }

    public List<IndividualAPI> getIndividuals() {
        return individuals;
    }

    public void setIndividuals(List<IndividualAPI> individuals) {
        this.individuals = individuals;
    }
}
