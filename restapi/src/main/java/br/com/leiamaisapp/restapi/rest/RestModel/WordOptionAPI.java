package br.com.leiamaisapp.restapi.rest.RestModel;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.Date;

import br.com.leiamaisapp.database.Model.WordOption;

/**
 * Created by erickson on 20/11/16.
 */
@Parcel
public class WordOptionAPI implements Serializable {
    public WordOptionAPI() {
    }

    public WordOptionAPI(String idWordTest, boolean correct, String option) {
        this.idWordTest = idWordTest;
        this.correct = correct;
        this.option = option;
    }

    String idWordTest;
    boolean correct;
    String option;
    Date updatedAt;

    public void convertFromWordOption(WordOption wordOption) {
        setIdWordTest(wordOption.getIdWordTest());
        setOption(wordOption.getOption());
        setCorrect(wordOption.isCorrect());
        setUpdatedAt(wordOption.getUpdatedAt());
    }

    public WordOption convertToWordOption() {
        WordOption wordOption = new WordOption();
        
        wordOption.setIdWordTest(getIdWordTest());
        wordOption.setOption(getOption());
        wordOption.setCorrect(isCorrect());
        wordOption.setUpdatedAt(getUpdatedAt());
        
        return wordOption;
    }

    public String getIdWordTest() {
        return idWordTest;
    }

    public void setIdWordTest(String idWordTest) {
        this.idWordTest = idWordTest;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
