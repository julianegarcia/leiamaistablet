package br.com.leiamaisapp.restapi.rest.RestModel;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.Date;

import br.com.leiamaisapp.database.Model.Professional;

/**
 * Created by erickson on 21/11/16.
 */

@Parcel
public class ProfessionalAPI implements Serializable {

    @SerializedName("_id")
    String idBase;
    String photo;
    String title;
    String name;
    Date birthday;
    String sex;
    String cpf;
    String rg;
    String telephone;
    String address;
    String email;
    String function;
    String field;
    @SerializedName("android_password")
    String password;
    @SerializedName("created_at")
    Date createdAt;
    @SerializedName("updated_at")
    Date updatedAt;

    public void convertFromProfessional(Professional professional) {
        setIdBase(professional.getIdBase());
        setPhoto(professional.getPhoto());
        setTitle(professional.getTitle());
        setName(professional.getName());
        setBirthday(professional.getBirthday());
        setSex(professional.getSex());
        setCpf(professional.getCpf());
        setRg(professional.getRg());
        setTelephone(professional.getTelephone());
        setAddress(professional.getAddress());
        setEmail(professional.getEmail());
        setFunction(professional.getFunction());
        setField(professional.getField());
        setPassword(professional.getPassword());
        setCreatedAt(professional.getCreatedAt());
        setUpdatedAt(professional.getUpdatedAt());
    }

    public Professional convertToProfessional() {
        Professional professional = new Professional();

        professional.setIdBase(getIdBase());
        professional.setPhoto(getPhoto());
        professional.setTitle(getTitle());
        professional.setName(getName());
        professional.setBirthday(getBirthday());
        professional.setSex(getSex());
        professional.setCpf(getCpf());
        professional.setRg(getRg());
        professional.setTelephone(getTelephone());
        professional.setAddress(getAddress());
        professional.setEmail(getEmail());
        professional.setFunction(getFunction());
        professional.setField(getField());
        professional.setPassword(getPassword());
        professional.setCreatedAt(getCreatedAt());
        professional.setUpdatedAt(getUpdatedAt());

        return professional;
    }

    public String getIdBase() {
        return idBase;
    }

    public void setIdBase(String idBase) {
        this.idBase = idBase;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
