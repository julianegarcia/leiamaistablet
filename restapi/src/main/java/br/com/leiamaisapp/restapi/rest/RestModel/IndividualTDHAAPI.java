package br.com.leiamaisapp.restapi.rest.RestModel;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

import br.com.leiamaisapp.database.Model.IndividualTDHA;

/**
 * Created by erickson on 19/11/16.
 */
@Parcel
public class IndividualTDHAAPI implements Serializable {

    @SerializedName("question1")
    boolean question1;
    @SerializedName("question2")
    boolean question2;
    @SerializedName("question3")
    boolean question3;
    @SerializedName("question4")
    boolean question4;
    @SerializedName("question5")
    boolean question5;
    @SerializedName("question6")
    boolean question6;
    @SerializedName("question7")
    boolean question7;
    @SerializedName("question8")
    boolean question8;
    @SerializedName("question9")
    boolean question9;
    @SerializedName("question10")
    boolean question10;
    @SerializedName("question11")
    boolean question11;
    @SerializedName("question12")
    boolean question12;
    @SerializedName("question13")
    boolean question13;
    @SerializedName("question14")
    boolean question14;
    @SerializedName("question15")
    boolean question15;
    @SerializedName("question16")
    boolean question16;
    @SerializedName("question17")
    boolean question17;
    @SerializedName("question18")
    boolean question18;

    public void convertFromIndividualTDHA(IndividualTDHA tdha) {
        setQuestion1(tdha.getQuestion1());
        setQuestion2(tdha.getQuestion2());
        setQuestion3(tdha.getQuestion3());
        setQuestion4(tdha.getQuestion4());
        setQuestion5(tdha.getQuestion5());
        setQuestion6(tdha.getQuestion6());
        setQuestion7(tdha.getQuestion7());
        setQuestion8(tdha.getQuestion8());
        setQuestion9(tdha.getQuestion9());
        setQuestion10(tdha.getQuestion10());
        setQuestion11(tdha.getQuestion11());
        setQuestion12(tdha.getQuestion12());
        setQuestion13(tdha.getQuestion13());
        setQuestion14(tdha.getQuestion14());
        setQuestion15(tdha.getQuestion15());
        setQuestion16(tdha.getQuestion16());
        setQuestion17(tdha.getQuestion17());
        setQuestion18(tdha.getQuestion18());
    }
    public IndividualTDHA convertToIndividualTDHA() {
        IndividualTDHA tdha = new IndividualTDHA();
        
        tdha.setQuestion1(getQuestion1());
        tdha.setQuestion2(getQuestion2());
        tdha.setQuestion3(getQuestion3());
        tdha.setQuestion4(getQuestion4());
        tdha.setQuestion5(getQuestion5());
        tdha.setQuestion6(getQuestion6());
        tdha.setQuestion7(getQuestion7());
        tdha.setQuestion8(getQuestion8());
        tdha.setQuestion9(getQuestion9());
        tdha.setQuestion10(getQuestion10());
        tdha.setQuestion11(getQuestion11());
        tdha.setQuestion12(getQuestion12());
        tdha.setQuestion13(getQuestion13());
        tdha.setQuestion14(getQuestion14());
        tdha.setQuestion15(getQuestion15());
        tdha.setQuestion16(getQuestion16());
        tdha.setQuestion17(getQuestion17());
        tdha.setQuestion18(getQuestion18());
        
        return tdha;
    }

    public boolean getQuestion1() {
        return question1;
    }

    public void setQuestion1(boolean question1) {
        this.question1 = question1;
    }

    public boolean getQuestion2() {
        return question2;
    }

    public void setQuestion2(boolean question2) {
        this.question2 = question2;
    }

    public boolean getQuestion3() {
        return question3;
    }

    public void setQuestion3(boolean question3) {
        this.question3 = question3;
    }

    public boolean getQuestion4() {
        return question4;
    }

    public void setQuestion4(boolean question4) {
        this.question4 = question4;
    }

    public boolean getQuestion5() {
        return question5;
    }

    public void setQuestion5(boolean question5) {
        this.question5 = question5;
    }

    public boolean getQuestion6() {
        return question6;
    }

    public void setQuestion6(boolean question6) {
        this.question6 = question6;
    }

    public boolean getQuestion7() {
        return question7;
    }

    public void setQuestion7(boolean question7) {
        this.question7 = question7;
    }

    public boolean getQuestion8() {
        return question8;
    }

    public void setQuestion8(boolean question8) {
        this.question8 = question8;
    }

    public boolean getQuestion9() {
        return question9;
    }

    public void setQuestion9(boolean question9) {
        this.question9 = question9;
    }

    public boolean getQuestion10() {
        return question10;
    }

    public void setQuestion10(boolean question10) {
        this.question10 = question10;
    }

    public boolean getQuestion11() {
        return question11;
    }

    public void setQuestion11(boolean question11) {
        this.question11 = question11;
    }

    public boolean getQuestion12() {
        return question12;
    }

    public void setQuestion12(boolean question12) {
        this.question12 = question12;
    }

    public boolean getQuestion13() {
        return question13;
    }

    public void setQuestion13(boolean question13) {
        this.question13 = question13;
    }

    public boolean getQuestion14() {
        return question14;
    }

    public void setQuestion14(boolean question14) {
        this.question14 = question14;
    }

    public boolean getQuestion15() {
        return question15;
    }

    public void setQuestion15(boolean question15) {
        this.question15 = question15;
    }

    public boolean getQuestion16() {
        return question16;
    }

    public void setQuestion16(boolean question16) {
        this.question16 = question16;
    }

    public boolean getQuestion17() {
        return question17;
    }

    public void setQuestion17(boolean question17) {
        this.question17 = question17;
    }

    public boolean getQuestion18() {
        return question18;
    }

    public void setQuestion18(boolean question18) {
        this.question18 = question18;
    }
}
