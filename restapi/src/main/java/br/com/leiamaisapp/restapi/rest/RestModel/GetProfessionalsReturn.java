package br.com.leiamaisapp.restapi.rest.RestModel;

import java.util.ArrayList;
import java.util.List;

import br.com.leiamaisapp.database.Model.Professional;

/**
 * Created by erickson on 21/11/16.
 */

public class GetProfessionalsReturn {
    List<ProfessionalAPI> professionals;

    public List<Professional> convertToProfessionals() {
        List<Professional> professionals = new ArrayList<>();

        for (ProfessionalAPI professionalAPI : getProfessionals()) {
            professionals.add(professionalAPI.convertToProfessional());
        }

        return professionals;
    }

    public List<ProfessionalAPI> getProfessionals() {
        return professionals;
    }
}
