package br.com.leiamaisapp.restapi.rest.RestModel;

import org.parceler.Parcel;

import br.com.leiamaisapp.database.Model.WordToImageTestResult;

/**
 * Created by erickson on 20/11/16.
 */

@Parcel
public class WordToImageTestResultAPI extends SpellingTestResultAPI {
    public void convertFromWordToImageTestResult(WordToImageTestResult result) {
        setTag(result.getTag());
        setIdBase(result.getIdBase());
        setStart(result.getStart());
        setHit(result.isHit());
        setWord(result.getWord());
        setStop(result.getStop());
    }
    
    public WordToImageTestResult convertToWordToImageTestResult() {
        WordToImageTestResult result = new WordToImageTestResult();

        result.setTag(getTag());
        result.setIdBase(getIdBase());
        result.setStart(getStart());
        result.setHit(isHit());
        result.setWord(getWord());
        result.setStop(getStop());
                
        return result;
    }
}
