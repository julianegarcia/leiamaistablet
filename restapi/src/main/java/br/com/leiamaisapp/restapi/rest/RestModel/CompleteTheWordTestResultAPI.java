package br.com.leiamaisapp.restapi.rest.RestModel;

import br.com.leiamaisapp.database.Model.CompleteTheWordTestResult;

/**
 * Created by erickson on 20/11/16.
 */

public class CompleteTheWordTestResultAPI extends SpellingTestResultAPI {

    public void convertFromCompleteTheWordTestResult(CompleteTheWordTestResult result) {
        setTag(result.getTag());
        setIdBase(result.getIdBase());
        setStart(result.getStart());
        setHit(result.isHit());
        setWord(result.getWord());
        setStop(result.getStop());
    }

    public CompleteTheWordTestResult convertToCompleteTheWordTestResult() {
        CompleteTheWordTestResult result = new CompleteTheWordTestResult();

        result.setTag(getTag());
        result.setIdBase(getIdBase());
        result.setStart(getStart());
        result.setHit(isHit());
        result.setWord(getWord());
        result.setStop(getStop());

        return result;
    }
}
