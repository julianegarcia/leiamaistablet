package br.com.leiamaisapp.restapi.rest.RestModel;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by erickson on 20/11/16.
 */

@Parcel
public class TestResultAPI implements Serializable {

    String idBase;
    String idIndividual;
    String idProfessional;
    String tag;
    Date start;
    Date stop;
    boolean hit;

    public String getIdBase() {
        return idBase;
    }

    public void setIdBase(String idBase) {
        this.idBase = idBase;
    }

    public String getIdIndividual() {
        return idIndividual;
    }

    public void setIdIndividual(String idIndividual) {
        this.idIndividual = idIndividual;
    }

    public String getIdProfessional() {
        return idProfessional;
    }

    public void setIdProfessional(String idProfessional) {
        this.idProfessional = idProfessional;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStop() {
        return stop;
    }

    public void setStop(Date stop) {
        this.stop = stop;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }
}
