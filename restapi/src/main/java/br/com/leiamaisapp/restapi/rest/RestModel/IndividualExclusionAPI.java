package br.com.leiamaisapp.restapi.rest.RestModel;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

import br.com.leiamaisapp.database.Model.IndividualExclusion;

/**
 * Created by erickson on 19/11/16.
 */
@Parcel
public class IndividualExclusionAPI implements Serializable {

    @SerializedName("mentalDisability")
    boolean mentalDisability;
    @SerializedName("neurologicalDisorder")
    boolean neurologicalDisorder;
    @SerializedName("hearingDeficiency")
    boolean hearingDeficiency;
    @SerializedName("visualImpairment")
    boolean visualImpairment;
    @SerializedName("nervousSystemDisease")
    boolean nervousSystemDisease;
    @SerializedName("psychosis")
    boolean psychosis;

    public void convertFromIndividualExclusion(IndividualExclusion exclusion) {
        setMentalDisability(exclusion.getMentalDisability());
        setNeurologicalDisorder(exclusion.getNeurologicalDisorder());
        setHearingDeficiency(exclusion.getHearingDeficiency());
        setVisualImpairment(exclusion.getVisualImpairment());
        setNervousSystemDisease(exclusion.getNervousSystemDisease());
        setPsychosis(exclusion.getPsychosis());
    }
    
    public IndividualExclusion convertToIndividualExclusion() {
        IndividualExclusion exclusion = new IndividualExclusion();

        exclusion.setMentalDisability(getMentalDisability());
        exclusion.setNeurologicalDisorder(getNeurologicalDisorder());
        exclusion.setHearingDeficiency(getHearingDeficiency());
        exclusion.setVisualImpairment(getVisualImpairment());
        exclusion.setNervousSystemDisease(getNervousSystemDisease());
        exclusion.setPsychosis(getPsychosis());
        
        return exclusion;
    }

   public boolean getMentalDisability() {
        return mentalDisability;
    }

    public void setMentalDisability(boolean mentalDisability) {
        this.mentalDisability = mentalDisability;
    }

    public boolean getNeurologicalDisorder() {
        return neurologicalDisorder;
    }

    public void setNeurologicalDisorder(boolean neurologicalDisorder) {
        this.neurologicalDisorder = neurologicalDisorder;
    }

    public boolean getHearingDeficiency() {
        return hearingDeficiency;
    }

    public void setHearingDeficiency(boolean hearingDeficiency) {
        this.hearingDeficiency = hearingDeficiency;
    }

    public boolean getVisualImpairment() {
        return visualImpairment;
    }

    public void setVisualImpairment(boolean visualImpairment) {
        this.visualImpairment = visualImpairment;
    }

    public boolean getNervousSystemDisease() {
        return nervousSystemDisease;
    }

    public void setNervousSystemDisease(boolean nervousSystemDisease) {
        this.nervousSystemDisease = nervousSystemDisease;
    }

    public boolean getPsychosis() {
        return psychosis;
    }

    public void setPsychosis(boolean psychosis) {
        this.psychosis = psychosis;
    }
}
