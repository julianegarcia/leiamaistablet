package br.com.leiamaisapp.restapi.rest.RestModel;

import android.support.annotation.Nullable;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.leiamaisapp.database.Model.WordOption;
import br.com.leiamaisapp.database.Model.WordTest;
import io.realm.RealmCollection;
import io.realm.RealmList;

/**
 * Created by erickson on 20/11/16.
 */

@Parcel
public class WordTestAPI implements Serializable {
    String idWordTest;
    String word;
    String incompleteWord;
    String image;
    Date updatedAt;
    List<WordOptionAPI> options;

    public String getIdWordTest() {
        return idWordTest;
    }

    public void convertFromWordTest(WordTest wordTest) {
        setIdWordTest(wordTest.getIdBase());
        setWord(wordTest.getWord());
        setIncompleteWord(wordTest.getIncompleteWord());
        setImage(wordTest.getImage());
        options = new ArrayList<>();
        WordOptionAPI option = null;
        for (WordOption wordOption :
                wordTest.getOptions()) {
            option = new WordOptionAPI(
                    wordOption.getIdWordTest()
                    , wordOption.isCorrect()
                    , wordOption.getOption()
            );
            options.add(option);
        }
    }

    public WordTest convertToWordTest() {
        WordTest wordTest = new WordTest();

        wordTest.setIdBase(getIdWordTest());
        wordTest.setWord(getWord());
        wordTest.setIncompleteWord(getIncompleteWord());
        wordTest.setImage(getImage());

        RealmCollection<WordOption> options = new RealmList<>();

        for (WordOptionAPI optionAPI :
                getOptions()) {
            options.add(optionAPI.convertToWordOption());
        }

        wordTest.setOptions((RealmList<WordOption>) options);

        return wordTest;
    }

    public void setIdWordTest(@Nullable String idWordTest) {
        this.idWordTest = idWordTest;
    }

    public List<WordOptionAPI> getOptions() {
        return options;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getIncompleteWord() {
        return incompleteWord;
    }

    public void setIncompleteWord(String incompleteWord) {
        this.incompleteWord = incompleteWord;
    }

    public void setOptions(List<WordOptionAPI> options) {
        this.options = options;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
