package br.com.leiamaisapp.restapi.rest.RestModel;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.leiamaisapp.database.Model.AlphabetTestResult;
import br.com.leiamaisapp.database.Model.CompleteTheWordTestResult;
import br.com.leiamaisapp.database.Model.Session;
import br.com.leiamaisapp.database.Model.SpellingTestResult;
import br.com.leiamaisapp.database.Model.WordToImageTestResult;
import io.realm.RealmCollection;
import io.realm.RealmList;

/**
 * Created by erickson on 20/11/16.
 */

@Parcel
public class SessionAPI implements Serializable {
    String idBase;
    String idProfessional;
    String idIndividual;
    Date start;
    Date stop;
    boolean broke;
    List<AlphabetTestResultAPI> alphabetTestResults;
    List<SpellingTestResultAPI> spellingTestResults;
    List<WordToImageTestResultAPI> wordToImageTestResults;
    List<CompleteTheWordTestResultAPI> completeTheWordTestResults;

    public void convertFromSession(Session session) {
        setIdBase(session.getIdBase());
        setIdProfessional(session.getIdProfessional());
        setIdIndividual(session.getIdIndividual());
        setStart(session.getStart());
        setStop(session.getStop());
        setBroke(session.isBroke());

        AlphabetTestResultAPI alphabetTestResultAPI = null;
        List<AlphabetTestResultAPI> alphabetTestResultAPIs = new ArrayList<>();

        for (AlphabetTestResult alphabetTestResult :
                session.getAlphabetTestResults()) {
            alphabetTestResultAPI = new AlphabetTestResultAPI();
            alphabetTestResultAPI.convertFromAlphabetTestResult(alphabetTestResult);
            alphabetTestResultAPIs.add(alphabetTestResultAPI);
        }
        setAlphabetTestResults(alphabetTestResultAPIs);

        WordToImageTestResultAPI wordToImageTestResultAPI = null;
        List<WordToImageTestResultAPI> wordToImageTestResultAPIs = new ArrayList<>();

        for (WordToImageTestResult wordToImageTestResult :
                session.getWordToImageTestResults()) {
            wordToImageTestResultAPI = new WordToImageTestResultAPI();
            wordToImageTestResultAPI.convertFromWordToImageTestResult(wordToImageTestResult);
            wordToImageTestResultAPIs.add(wordToImageTestResultAPI);
        }
        setWordToImageTestResults(wordToImageTestResultAPIs);

        SpellingTestResultAPI spellingTestResultAPI = null;
        List<SpellingTestResultAPI> spellingTestResultAPIs = new ArrayList<>();

        for (SpellingTestResult spellingTestResult :
                session.getSpellingTestResults()) {
            spellingTestResultAPI = new SpellingTestResultAPI();
            spellingTestResultAPI.convertFromSpellingTestResult(spellingTestResult);
            spellingTestResultAPIs.add(spellingTestResultAPI);
        }
        setSpellingTestResults(spellingTestResultAPIs);

        CompleteTheWordTestResultAPI completeTheWordTestResultAPI = null;
        List<CompleteTheWordTestResultAPI> completeTheWordTestResultAPIs = new ArrayList<>();

        for (CompleteTheWordTestResult completeTheWordTestResult :
                session.getCompleteTheWordTestResults()) {
            completeTheWordTestResultAPI = new CompleteTheWordTestResultAPI();
            completeTheWordTestResultAPI.convertFromCompleteTheWordTestResult(completeTheWordTestResult);
            completeTheWordTestResultAPIs.add(completeTheWordTestResultAPI);
        }
        setCompleteTheWordTestResults(completeTheWordTestResultAPIs);

    }

    public Session convertToSession() {
        Session session = new Session();

        session.setIdBase(getIdBase());
        session.setIdProfessional(getIdProfessional());
        session.setIdIndividual(getIdIndividual());
        session.setStart(getStart());
        session.setStop(getStop());
        session.setBroke(isBroke());

        RealmCollection<AlphabetTestResult> alphabetTestResults = new RealmList<>();

        for (AlphabetTestResultAPI alphabetTestResultAPI :
                getAlphabetTestResults()) {
            alphabetTestResults.add(alphabetTestResultAPI.convertToAlphabetTestResult());
        }
        session.setAlphabetTestResults((RealmList<AlphabetTestResult>) alphabetTestResults);

        RealmCollection<WordToImageTestResult> wordToImageTestResults = new RealmList<>();

        for (WordToImageTestResultAPI wordToImageTestResultAPI :
                getWordToImageTestResults()) {
            wordToImageTestResults.add(wordToImageTestResultAPI.convertToWordToImageTestResult());
        }
        session.setWordToImageTestResults((RealmList<WordToImageTestResult>) wordToImageTestResults);

        RealmCollection<SpellingTestResult> spellingTestResults = new RealmList<>();

        for (SpellingTestResultAPI spellingTestResultAPI :
                getSpellingTestResults()) {
            spellingTestResults.add(spellingTestResultAPI.convertToSpellingTestResult());
        }
        session.setSpellingTestResults((RealmList<SpellingTestResult>) spellingTestResults);

        RealmCollection<CompleteTheWordTestResult> completeTheWordTestResults = new RealmList<>();

        for (CompleteTheWordTestResultAPI completeTheWordTestResultAPI :
                getCompleteTheWordTestResults()) {
            completeTheWordTestResults.add(completeTheWordTestResultAPI.convertToCompleteTheWordTestResult());
        }
        session.setCompleteTheWordTestResults((RealmList<CompleteTheWordTestResult>) completeTheWordTestResults);

        return session;
    }

    public String getIdBase() {
        return idBase;
    }

    public void setIdBase(String idBase) {
        this.idBase = idBase;
    }

    public String getIdIndividual() {
        return idIndividual;
    }

    public void setIdIndividual(String idIndividual) {
        this.idIndividual = idIndividual;
    }

    public String getIdProfessional() {
        return idProfessional;
    }

    public void setIdProfessional(String idProfessional) {
        this.idProfessional = idProfessional;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStop() {
        return stop;
    }

    public void setStop(Date stop) {
        this.stop = stop;
    }

    public boolean isBroke() {
        return broke;
    }

    public void setBroke(boolean broke) {
        this.broke = broke;
    }

    public List<AlphabetTestResultAPI> getAlphabetTestResults() {
        return alphabetTestResults;
    }

    public void setAlphabetTestResults(List<AlphabetTestResultAPI> alphabetTestResults) {
        this.alphabetTestResults = alphabetTestResults;
    }

    public List<SpellingTestResultAPI> getSpellingTestResults() {
        return spellingTestResults;
    }

    public void setSpellingTestResults(List<SpellingTestResultAPI> spellingTestResults) {
        this.spellingTestResults = spellingTestResults;
    }

    public List<WordToImageTestResultAPI> getWordToImageTestResults() {
        return wordToImageTestResults;
    }

    public void setWordToImageTestResults(List<WordToImageTestResultAPI> wordToImageTestResults) {
        this.wordToImageTestResults = wordToImageTestResults;
    }

    public List<CompleteTheWordTestResultAPI> getCompleteTheWordTestResults() {
        return completeTheWordTestResults;
    }

    public void setCompleteTheWordTestResults(List<CompleteTheWordTestResultAPI> completeTheWordTestResults) {
        this.completeTheWordTestResults = completeTheWordTestResults;
    }
}
