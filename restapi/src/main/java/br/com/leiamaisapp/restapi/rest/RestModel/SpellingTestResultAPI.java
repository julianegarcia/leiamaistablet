package br.com.leiamaisapp.restapi.rest.RestModel;

import org.parceler.Parcel;

import br.com.leiamaisapp.database.Model.SpellingTestResult;

/**
 * Created by erickson on 20/11/16.
 */

@Parcel
public class SpellingTestResultAPI extends TestResultAPI {

    String word;

    public void convertFromSpellingTestResult(SpellingTestResult result) {
        setTag(result.getTag());
        setIdBase(result.getIdBase());
        setStart(result.getStart());
        setHit(result.isHit());
        setWord(result.getWord());
        setStop(result.getStop());
    }

    public SpellingTestResult convertToSpellingTestResult() {
        SpellingTestResult result = new SpellingTestResult();

        result.setTag(getTag());
        result.setIdBase(getIdBase());
        result.setStart(getStart());
        result.setHit(isHit());
        result.setWord(getWord());
        result.setStop(getStop());

        return result;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
