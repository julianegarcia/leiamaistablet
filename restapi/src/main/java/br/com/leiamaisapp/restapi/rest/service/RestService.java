package br.com.leiamaisapp.restapi.rest.service;

import java.util.List;

import br.com.leiamaisapp.database.Model.Professional;
import br.com.leiamaisapp.restapi.rest.RestModel.GetIndividualsReturn;
import br.com.leiamaisapp.restapi.rest.RestModel.GetProfessionalsReturn;
import br.com.leiamaisapp.restapi.rest.RestModel.IndividualAPI;
import br.com.leiamaisapp.restapi.rest.RestModel.ProfessionalAPI;
import br.com.leiamaisapp.restapi.rest.RestModel.SessionAPI;
import br.com.leiamaisapp.restapi.rest.RestModel.WordTestAPI;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by erickson on 28/10/16.
 */
public interface RestService {
    @POST("individuals/create")
    Call<IndividualAPI> postIndividual(@Body IndividualAPI individual);

    @POST("testes/stores")
    Call<WordTestAPI> postWordTest(@Body WordTestAPI wordTestAPI);

    @POST("individuals ")
    Call<GetIndividualsReturn> getIndividuals();

    @POST("testes/store")
    Call<SessionAPI> postSession(@Body SessionAPI session);

    @POST("users/get-all")
    Call<List<ProfessionalAPI>> getProfessionals();

    @POST("individuals/update")
    Call<String> updateIndividual(@Body IndividualAPI individualAPI);
}
