package br.com.leiamaisapp.database.Common;

import java.util.Date;

/**
 * Created by erickson on 20/11/16.
 */

public interface TestResult {
    Long getId();

    void setId(Long id);

    String getIdBase();

    void setIdBase(String idBase);

    String getTag();

    void setTag(String tag);

    Date getStart();

    void setStart(Date start);

    Date getStop();

    void setStop(Date stop);

    boolean isHit();

    void setHit(boolean hit);
}
