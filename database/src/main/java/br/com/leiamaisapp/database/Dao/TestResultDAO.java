package br.com.leiamaisapp.database.Dao;

import android.support.annotation.NonNull;

import br.com.leiamaisapp.database.Model.AlphabetTestResult;
import br.com.leiamaisapp.database.Model.SpellingTestResult;
import br.com.leiamaisapp.database.Model.WordToImageTestResult;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by erickson on 20/11/16.
 */

public class TestResultDAO {

    Realm realm;

    public void saveManyAlphabetTestResult(RealmList<AlphabetTestResult> alphabetTestResults) {
        for (AlphabetTestResult result :
                alphabetTestResults) {
            saveAlphabetTestResult(result);
        }
    }

    public void saveManySpellingTestResult(RealmList<SpellingTestResult> alphabetTestResults) {
        for (SpellingTestResult result :
                alphabetTestResults) {
            saveSpellingTestResult(result);
        }
    }

    public void saveManyWordToImageTestResult(RealmList<WordToImageTestResult> alphabetTestResults) {
        for (WordToImageTestResult result :
                alphabetTestResults) {
            saveWordToImageTestResult(result);
        }
    }

    public void saveAlphabetTestResult(AlphabetTestResult testResult) {
        this.realm.beginTransaction();
        if (testResult.getId() == null)
            testResult.setId(getNewAlphabetTestResultId());
        this.realm.copyToRealmOrUpdate(testResult);
        this.realm.commitTransaction();
    }

    public void saveSpellingTestResult(SpellingTestResult testResult) {
        this.realm.beginTransaction();
        if (testResult.getId() == null)
            testResult.setId(getNewSpellingTestResultId());
        this.realm.copyToRealmOrUpdate(testResult);
        this.realm.commitTransaction();
    }

    public void saveWordToImageTestResult(WordToImageTestResult testResult) {
        this.realm.beginTransaction();
        if (testResult.getId() == null)
            testResult.setId(getNewWordToImageTestResultId());
        this.realm.copyToRealmOrUpdate(testResult);
        this.realm.commitTransaction();
    }

    @NonNull
    private Long getNewAlphabetTestResultId() {
        try { // On the first object, it will return null and throw the exception, so we must catch it
            return this.realm.where(AlphabetTestResult.class).max("id").longValue() + 1;
        } catch (NullPointerException e) {
            return 1l; // Return PK 1 for the first object
        }
    }

    @NonNull
    private Long getNewSpellingTestResultId() {
        try { // On the first object, it will return null and throw the exception, so we must catch it
            return this.realm.where(SpellingTestResult.class).max("id").longValue() + 1;
        } catch (NullPointerException e) {
            return 1l; // Return PK 1 for the first object
        }
    }

    @NonNull
    private Long getNewWordToImageTestResultId() {
        try { // On the first object, it will return null and throw the exception, so we must catch it
            return this.realm.where(WordToImageTestResult.class).max("id").longValue() + 1;
        } catch (NullPointerException e) {
            return 1l; // Return PK 1 for the first object
        }
    }
}
