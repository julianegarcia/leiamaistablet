package br.com.leiamaisapp.database.Dao;

import android.support.annotation.NonNull;

import br.com.leiamaisapp.database.Model.WordOption;
import br.com.leiamaisapp.database.Model.WordTest;
import io.realm.Realm;
import io.realm.RealmQuery;

/**
 * Created by erickson on 20/11/16.
 */

public class WordTestDAO {

    Realm realm;

    public void save(WordTest wordTest) {
        if (wordTest.getId() == null)
            wordTest.setId(getNewId());
        this.realm.beginTransaction();
        if (wordTest.getOptions() != null) {
            for (WordOption option :
                    wordTest.getOptions()) {
                option.setIdWordTest(wordTest.getIdBase());
                this.realm.copyToRealmOrUpdate(option);
            }
        }

        this.realm.copyToRealmOrUpdate(wordTest);
        this.realm.commitTransaction();
    }

    public WordTest findById(Long id) {
        RealmQuery<WordTest> query = realm.where(WordTest.class);
        query.equalTo("id", id);
        WordTest wordTest = query.findFirst();
        return realm.copyFromRealm(wordTest);
    }

    public WordTest findByIdBase(String idBase) {
        RealmQuery<WordTest> query = realm.where(WordTest.class);
        query.equalTo("idBase", idBase);
        WordTest wordTest = query.findFirst();
        return realm.copyFromRealm(wordTest);
    }

    @NonNull
    private Long getNewId() {
        try { // On the first object, it will return null and throw the exception, so we must catch it
            return this.realm.where(WordTest.class).max("id").longValue() + 1;
        } catch (NullPointerException e) {
            return 1l; // Return PK 1 for the first object
        }
    }
}
