package br.com.leiamaisapp.database.Model;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by erickson on 09/07/16.
 */
public class Individual extends RealmObject {

    @PrimaryKey
    private Long id;
    private String idBase;
    private String createdBy;
    private Date lastUpdate;
    private String photo;
    private String name;
    private Date birthday;
    private String sex;
    private String cpf;
    private String parentCpf;
    private String rg;
    private String motherName;
    private String fatherName;
    private String telephone;
    private String address;
    private String email;
    private String familyHistoric;
    private String consanguinityHistoric;
    private String desiredGestation;
    private String pregnancyComplications;
    private String maternalHistoric;
    private String neonatalComplications;
    private String comorbidities;
    private String grade;
    private String schoolSupport;
    private String tdhaTreatment;
    private String regularSchool;
    private String speakProblem;
    private IndividualExclusion individualExclusion;
    private IndividualTDHA individualTDHA;

    public String getFamilyHistoric() {
        return familyHistoric;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getTdhaTreatment() {
        return tdhaTreatment;
    }

    public void setTdhaTreatment(String tdha_treatment) {
        this.tdhaTreatment = tdha_treatment;
    }

    public String getSchoolSupport() {
        return schoolSupport;
    }

    public void setSchoolSupport(String school_support) {
        this.schoolSupport = school_support;
    }

    public String getRegularSchool() {
        return regularSchool;
    }

    public void setRegularSchool(String regular_school) {
        this.regularSchool = regular_school;
    }

    public String getSpeakProblem() {
        return speakProblem;
    }

    public void setSpeakProblem(String speakProblem) {
        this.speakProblem = speakProblem;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdBase() {
        return idBase;
    }

    public void setIdBase(String idBase) {
        this.idBase = idBase;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getParentCpf() {
        return parentCpf;
    }

    public void setParentCpf(String parentCpf) {
        this.parentCpf = parentCpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFamilyHistoric(String familyHistoric) {
        this.familyHistoric = familyHistoric;
    }

    public String getConsanguinityHistoric() {
        return consanguinityHistoric;
    }

    public void setConsanguinityHistoric(String consanguinityHistoric) {
        this.consanguinityHistoric = consanguinityHistoric;
    }

    public String getDesiredGestation() {
        return desiredGestation;
    }

    public void setDesiredGestation(String desiredGestation) {
        this.desiredGestation = desiredGestation;
    }

    public String getPregnancyComplications() {
        return pregnancyComplications;
    }

    public void setPregnancyComplications(String pregnancyComplications) {
        this.pregnancyComplications = pregnancyComplications;
    }

    public String getMaternalHistoric() {
        return maternalHistoric;
    }

    public void setMaternalHistoric(String maternalHistoric) {
        this.maternalHistoric = maternalHistoric;
    }

    public String getNeonatalComplications() {
        return neonatalComplications;
    }

    public void setNeonatalComplications(String neonatalComplications) {
        this.neonatalComplications = neonatalComplications;
    }

    public String getComorbidities() {
        return comorbidities;
    }

    public void setComorbidities(String comorbidities) {
        this.comorbidities = comorbidities;
    }

    public IndividualExclusion getIndividualExclusion() {
        return individualExclusion;
    }

    public void setIndividualExclusion(IndividualExclusion individualExclusion) {
        this.individualExclusion = individualExclusion;
    }

    public IndividualTDHA getIndividualTDHA() {
        return individualTDHA;
    }

    public void setIndividualTDHA(IndividualTDHA individualTDHA) {
        this.individualTDHA = individualTDHA;
    }
}