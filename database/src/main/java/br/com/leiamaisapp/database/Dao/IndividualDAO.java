package br.com.leiamaisapp.database.Dao;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import br.com.leiamaisapp.database.Model.Individual;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by uriel-miranda on 14/08/16.
 */
public class IndividualDAO {

    public IndividualDAO() {
        this.realm = Realm.getDefaultInstance();
    }

    Realm realm;

    public void save(Individual individual) {
        try {
            deleteByIdBase(individual.getIdBase());
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.realm.beginTransaction();
        if (individual.getId() == null)
            individual.setId(getNewId());
        if (individual.getIndividualExclusion() != null) {
            individual.getIndividualExclusion().setIndividualId(individual.getId());
            this.realm.copyToRealmOrUpdate(individual.getIndividualExclusion());
        }
        if (individual.getIndividualTDHA() != null) {
            individual.getIndividualTDHA().setIndividualId(individual.getId());
            this.realm.copyToRealmOrUpdate(individual.getIndividualTDHA());
        }
        this.realm.copyToRealmOrUpdate(individual);
        this.realm.commitTransaction();
    }

    public void saveMany(List<Individual> individuals) {
        for (Individual individual : individuals) {
            save(individual);
        }
    }

    public void delete(Individual individual) {
        this.realm.beginTransaction();
        individual.deleteFromRealm();
        individual.getIndividualTDHA().deleteFromRealm();
        individual.getIndividualExclusion().deleteFromRealm();
        this.realm.commitTransaction();
    }

    public void deleteByIdBase(String idBase) {
        RealmQuery<Individual> query = realm.where(Individual.class);
        query.equalTo("idBase", idBase);
        Individual individual = query.findFirst();
        if (individual != null) {
            this.realm.beginTransaction();
            individual.deleteFromRealm();
            this.realm.commitTransaction();
        }
    }

    public Individual findRealByIdBase(String idBase) {
        RealmQuery<Individual> query = realm.where(Individual.class);
        query.equalTo("cpf", idBase);
        Individual individual = query.findFirst();
        return individual;
    }

    public Individual findById(Long id) {
        RealmQuery<Individual> query = realm.where(Individual.class);
        query.equalTo("id", id);
        Individual individual = query.findFirst();
        return realm.copyFromRealm(individual);
    }

    public Individual findByIdBase(String idBase) {
        RealmQuery<Individual> query = realm.where(Individual.class);
        query.equalTo("idBase", idBase);
        Individual individual = query.findFirst();
        return realm.copyFromRealm(individual);
    }

    public RealmResults<Individual> findAll(@Nullable String letter) {

        final RealmResults<Individual> result = letter == null ? realm.where(Individual.class).findAllSorted("name")
                : realm.where(Individual.class).beginsWith("name", letter, Case.INSENSITIVE).findAllSorted("name");
        return result;
    }

    public RealmResults<Individual> findAllByName(@Nullable String name) {

        final RealmResults<Individual> result = name == null ? realm.where(Individual.class).findAll()
                : realm.where(Individual.class).contains("name", name, Case.INSENSITIVE).findAll();
        result.sort("name");
        return result;
    }

    @NonNull
    private Long getNewId() {
        try { // On the first object, it will return null and throw the exception, so we must catch it
            return this.realm.where(Individual.class).max("id").longValue() + 1;
        } catch (NullPointerException e) {
            return 1l; // Return PK 1 for the first object
        }
    }

}
