package br.com.leiamaisapp.database.Model;

import android.support.annotation.Nullable;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by erickson on 20/11/16.
 */

public class WordOption extends RealmObject {
    public WordOption() {
    }

    public WordOption(String idWordTest, boolean correct, String option) {
        this.idWordTest = idWordTest;
        this.correct = correct;
        this.option = option;
    }

    @PrimaryKey
    private Long id;
    private String idWordTest;
    private boolean correct;
    private String option;
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdWordTest() {
        return idWordTest;
    }

    public void setIdWordTest(@Nullable String idWordTest) {
        this.idWordTest = idWordTest;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
