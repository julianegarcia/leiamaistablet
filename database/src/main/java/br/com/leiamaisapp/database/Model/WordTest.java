package br.com.leiamaisapp.database.Model;

import java.util.Date;

import io.realm.RealmCollection;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by erickson on 20/11/16.
 */

public class WordTest extends RealmObject {
    @PrimaryKey
    private Long id;
    private String idBase;
    private String word;
    private String incompleteWord;
    private RealmList<WordOption> options;
    private String image;
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdBase() {
        return idBase;
    }

    public void setIdBase(String idBase) {
        this.idBase = idBase;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getIncompleteWord() {
        return incompleteWord;
    }

    public void setIncompleteWord(String incompleteWord) {
        this.incompleteWord = incompleteWord;
    }

    public RealmCollection<WordOption> getOptions() {
        return options;
    }

    public void setOptions(RealmList<WordOption> options) {
        this.options = options;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
