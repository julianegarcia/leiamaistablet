package br.com.leiamaisapp.database.Dao;

import br.com.leiamaisapp.database.Model.Session;
import io.realm.Realm;

/**
 * Created by erickson on 23/11/16.
 */

public class SessionDAO {

    public SessionDAO() {
        this.realm = Realm.getDefaultInstance();
    }

    Realm realm;

    public void save(Session session) {
        TestResultDAO testsDao = new TestResultDAO();

        testsDao.saveManyAlphabetTestResult(session.getAlphabetTestResults());
        testsDao.saveManySpellingTestResult(session.getSpellingTestResults());
        testsDao.saveManyWordToImageTestResult(session.getWordToImageTestResults());

        if (session.getId() == null)
            session.setId(getNewId());


        this.realm.beginTransaction();
        this.realm.copyToRealmOrUpdate(session);
        this.realm.commitTransaction();
    }

    private Long getNewId() {
        try { // On the first object, it will return null and throw the exception, so we must catch it
            return this.realm.where(Session.class).max("id").longValue() + 1;
        } catch (NullPointerException e) {
            return 1l; // Return PK 1 for the first object
        }
    }
}
