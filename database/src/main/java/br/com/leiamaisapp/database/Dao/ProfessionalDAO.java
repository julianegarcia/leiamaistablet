package br.com.leiamaisapp.database.Dao;

import android.util.Log;

import java.util.List;

import br.com.leiamaisapp.database.Model.Professional;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by remoto on 24/07/2016.
 */
public class ProfessionalDAO {
    Realm realm;

    public ProfessionalDAO() {
        this.realm = Realm.getDefaultInstance();
    }

    public Professional findByEmail(String email) {
        Professional professional = this.realm
                .where(Professional.class)
                .equalTo("email", email)
                .findFirst();
        return professional;
    }

    public RealmResults<Professional> findAll() {
        return this.realm.where(Professional.class).findAll();
    }

    public Professional findById(Long id) {
        Professional professional = this.realm
                .where(Professional.class)
                .equalTo("id", id)
                .findFirst();
        return realm.copyFromRealm(professional);
    }

    public void save(Professional professional) {
        try {
            deleteByIdBase(professional.getIdBase());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (professional.getId() == null)
            professional.setId(getNewId());

        this.realm.beginTransaction();
        this.realm.copyToRealmOrUpdate(professional);
        this.realm.commitTransaction();
    }

    private void deleteByIdBase(String idBase) {
        RealmQuery<Professional> query = realm.where(Professional.class);
        query.equalTo("idBase", idBase);
        Professional professional = query.findFirst();
        if (professional != null) {
            this.realm.beginTransaction();
            professional.deleteFromRealm();
            this.realm.commitTransaction();
        }
    }

    public void saveMany(List<Professional> professionals) {
        for (Professional professional : professionals) {
            save(professional);
        }
    }

    private Long getNewId() {
        try { // On the first object, it will return null and throw the exception, so we must catch it
            return this.realm.where(Professional.class).max("id").longValue() + 1;
        } catch (NullPointerException e) {
            return 1l; // Return PK 1 for the first object
        }
    }
}
