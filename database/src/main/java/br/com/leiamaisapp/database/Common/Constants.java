package br.com.leiamaisapp.database.Common;

/**
 * Created by erickson on 20/11/16.
 */
public class Constants {
    public static final String ALPHABET_TEST_TAG = "alphabet";
    public static final String SPELLING_TEST_TAG = "spell-the-word";
    public static final String COMPLETE_WORD_TEST_TAG = "complete-word";
    public static final String WORD_TO_IMAGE_TEST_TAG = "link-word-to-image";
}
