package br.com.leiamaisapp.database.Model;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by erickson on 20/11/16.
 */

public class Session extends RealmObject {
    @PrimaryKey
    private Long id;
    private String idBase;
    private String idProfessional;
    private String idIndividual;
    private Date start;
    private Date stop;
    private boolean broke;
    private RealmList<AlphabetTestResult> alphabetTestResults;
    private RealmList<SpellingTestResult> spellingTestResults;
    private RealmList<WordToImageTestResult> wordToImageTestResults;
    private RealmList<CompleteTheWordTestResult> completeTheWordTestResults;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdBase() {
        return idBase;
    }

    public void setIdBase(String idBase) {
        this.idBase = idBase;
    }

    public String getIdProfessional() {
        return idProfessional;
    }

    public void setIdProfessional(String idProfessional) {
        this.idProfessional = idProfessional;
    }

    public String getIdIndividual() {
        return idIndividual;
    }

    public void setIdIndividual(String idIndividual) {
        this.idIndividual = idIndividual;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStop() {
        return stop;
    }

    public void setStop(Date stop) {
        this.stop = stop;
    }

    public boolean isBroke() {
        return broke;
    }

    public void setBroke(boolean broke) {
        this.broke = broke;
    }

    public RealmList<AlphabetTestResult> getAlphabetTestResults() {
        return alphabetTestResults;
    }

    public void setAlphabetTestResults(RealmList<AlphabetTestResult> alphabetTestResults) {
        this.alphabetTestResults = alphabetTestResults;
    }

    public RealmList<SpellingTestResult> getSpellingTestResults() {
        return spellingTestResults;
    }

    public void setSpellingTestResults(RealmList<SpellingTestResult> spellingTestResults) {
        this.spellingTestResults = spellingTestResults;
    }

    public RealmList<WordToImageTestResult> getWordToImageTestResults() {
        return wordToImageTestResults;
    }

    public void setWordToImageTestResults(RealmList<WordToImageTestResult> wordToImageTestResults) {
        this.wordToImageTestResults = wordToImageTestResults;
    }

    public RealmList<CompleteTheWordTestResult> getCompleteTheWordTestResults() {
        return completeTheWordTestResults;
    }

    public void setCompleteTheWordTestResults(RealmList<CompleteTheWordTestResult> completeTheWordTestResults) {
        this.completeTheWordTestResults = completeTheWordTestResults;
    }
}
