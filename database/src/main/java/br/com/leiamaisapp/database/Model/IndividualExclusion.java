package br.com.leiamaisapp.database.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by uriel-miranda on 14/08/16.
 */
public class IndividualExclusion extends RealmObject {

    @PrimaryKey
    private Long individualId;
    private boolean mentalDisability;
    private boolean neurologicalDisorder;
    private boolean hearingDeficiency;
    private boolean visualImpairment;
    private boolean nervousSystemDisease;
    private boolean psychosis;

    public Long getIndividualId() {
        return individualId;
    }

    public void setIndividualId(Long individualId) {
        this.individualId = individualId;
    }

    public boolean getMentalDisability() {
        return mentalDisability;
    }

    public void setMentalDisability(boolean mentalDisability) {
        this.mentalDisability = mentalDisability;
    }

    public boolean getNeurologicalDisorder() {
        return neurologicalDisorder;
    }

    public void setNeurologicalDisorder(boolean neurologicalDisorder) {
        this.neurologicalDisorder = neurologicalDisorder;
    }

    public boolean getHearingDeficiency() {
        return hearingDeficiency;
    }

    public void setHearingDeficiency(boolean hearingDeficiency) {
        this.hearingDeficiency = hearingDeficiency;
    }

    public boolean getVisualImpairment() {
        return visualImpairment;
    }

    public void setVisualImpairment(boolean visualImpairment) {
        this.visualImpairment = visualImpairment;
    }

    public boolean getNervousSystemDisease() {
        return nervousSystemDisease;
    }

    public void setNervousSystemDisease(boolean nervousSystemDisease) {
        this.nervousSystemDisease = nervousSystemDisease;
    }

    public boolean getPsychosis() {
        return psychosis;
    }

    public void setPsychosis(boolean psychosis) {
        this.psychosis = psychosis;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("individualId = " + getIndividualId());

        sb.append("mentalDisability" + getMentalDisability());
        sb.append("neurologicalDisorder" + getNeurologicalDisorder());
        sb.append("hearingDeficiency" + getHearingDeficiency());
        sb.append("visualImpairment" + getVisualImpairment());
        sb.append("nervousSystemDisease" + getNervousSystemDisease());
        sb.append("psychosis" + getPsychosis());

        return sb.toString();
    }
}
