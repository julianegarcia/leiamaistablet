package br.com.leiamaisapp.database.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by uriel-miranda on 14/08/16.
 */
public class IndividualTDHA extends RealmObject {

    @PrimaryKey
    private Long individualId;
    private boolean question1;
    private boolean question2;
    private boolean question3;
    private boolean question4;
    private boolean question5;
    private boolean question6;
    private boolean question7;
    private boolean question8;
    private boolean question9;
    private boolean question10;
    private boolean question11;
    private boolean question12;
    private boolean question13;
    private boolean question14;
    private boolean question15;
    private boolean question16;
    private boolean question17;
    private boolean question18;

    public boolean getQuestion1() {
        return question1;
    }

    public void setQuestion1(boolean question1) {
        this.question1 = question1;
    }

    public boolean getQuestion2() {
        return question2;
    }

    public void setQuestion2(boolean question2) {
        this.question2 = question2;
    }

    public boolean getQuestion3() {
        return question3;
    }

    public void setQuestion3(boolean question3) {
        this.question3 = question3;
    }

    public boolean getQuestion4() {
        return question4;
    }

    public void setQuestion4(boolean question4) {
        this.question4 = question4;
    }

    public boolean getQuestion5() {
        return question5;
    }

    public void setQuestion5(boolean question5) {
        this.question5 = question5;
    }

    public boolean getQuestion6() {
        return question6;
    }

    public void setQuestion6(boolean question6) {
        this.question6 = question6;
    }

    public boolean getQuestion7() {
        return question7;
    }

    public void setQuestion7(boolean question7) {
        this.question7 = question7;
    }

    public boolean getQuestion8() {
        return question8;
    }

    public void setQuestion8(boolean question8) {
        this.question8 = question8;
    }

    public boolean getQuestion9() {
        return question9;
    }

    public void setQuestion9(boolean question9) {
        this.question9 = question9;
    }

    public boolean getQuestion10() {
        return question10;
    }

    public void setQuestion10(boolean question10) {
        this.question10 = question10;
    }

    public boolean getQuestion11() {
        return question11;
    }

    public void setQuestion11(boolean question11) {
        this.question11 = question11;
    }

    public boolean getQuestion12() {
        return question12;
    }

    public void setQuestion12(boolean question12) {
        this.question12 = question12;
    }

    public boolean getQuestion13() {
        return question13;
    }

    public void setQuestion13(boolean question13) {
        this.question13 = question13;
    }

    public boolean getQuestion14() {
        return question14;
    }

    public void setQuestion14(boolean question14) {
        this.question14 = question14;
    }

    public boolean getQuestion15() {
        return question15;
    }

    public void setQuestion15(boolean question15) {
        this.question15 = question15;
    }

    public boolean getQuestion16() {
        return question16;
    }

    public void setQuestion16(boolean question16) {
        this.question16 = question16;
    }

    public boolean getQuestion17() {
        return question17;
    }

    public void setQuestion17(boolean question17) {
        this.question17 = question17;
    }

    public boolean getQuestion18() {
        return question18;
    }

    public void setQuestion18(boolean question18) {
        this.question18 = question18;
    }

    public long getIndividualId() {
        return individualId;
    }

    public void setIndividualId(long individualId) {
        this.individualId = individualId;
    }

}
