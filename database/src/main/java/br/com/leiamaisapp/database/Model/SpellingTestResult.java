package br.com.leiamaisapp.database.Model;

import java.util.Date;

import br.com.leiamaisapp.database.Common.Constants;
import br.com.leiamaisapp.database.Common.TestResult;
import io.realm.RealmObject;


/**
 * Created by erickson on 20/11/16.
 */

public class SpellingTestResult extends RealmObject implements TestResult {
    public SpellingTestResult() {
        setTag(Constants.SPELLING_TEST_TAG);
    }

    public SpellingTestResult(String word) {
        setTag(Constants.SPELLING_TEST_TAG);
        this.word = word;
    }

    private Long id;
    private String idBase;
    private String tag;
    private Date start;
    private Date stop;
    private boolean hit;
    private String word;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdBase() {
        return idBase;
    }

    public void setIdBase(String idBase) {
        this.idBase = idBase;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStop() {
        return stop;
    }

    public void setStop(Date stop) {
        this.stop = stop;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
