package br.com.leiamaisapp.leiamais.Presenters.Individual;

import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.util.ArrayList;

import br.com.leiamaisapp.database.Model.Individual;
import br.com.leiamaisapp.database.Model.IndividualExclusion;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Common.MVPInterfaces;
import br.com.leiamaisapp.leiamais.Events.Common.ImageEvents;
import br.com.leiamaisapp.leiamais.Events.Individual.IndividualMenuOptionEvents;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualMenuFragment;

/**
 * Created by erickson on 13/10/16.
 */

public class IndividualMenuPresenter extends IndividualFragmentPresenter implements MVPInterfaces.IndividualMenuInterface {

    public IndividualMenuPresenter(IndividualMenuFragment fragment) {
        this.fragment = fragment;
    }

    private IndividualMenuFragment fragment;
    private IndividualMenuOptionEvents event;

    @Override
    public void startPresenter() {
        event = new IndividualMenuOptionEvents(this);
        setIndividualPresentation();
        setButtons();
        hideApplyTest();
    }

    private void setIndividualPresentation() {
        Individual individual = getIndividual();
        ImageEvents events = new ImageEvents(fragment.getActivity());

        try {
            final File file = new File(individual.getPhoto());
            if (file.exists()) {
                events.setImageViewPhoto(
                        fragment.getPhoto()
                        , file
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        fragment.getName().setText(individual.getName().toString());
    }

    private void setButtons() {
        ArrayList<Button> buttons = new ArrayList<>();
        ArrayList<String> button_tags = new ArrayList<>();

        buttons.add(fragment.getApplyTest());
        buttons.add(fragment.getPersonalData());
        buttons.add(fragment.getDevelopment());
        buttons.add(fragment.getExclusionCriteria());
        buttons.add(fragment.getReport());

        button_tags.add(Constants.INDIVIDUAL_APPLY_TEST_TAG);
        button_tags.add(Constants.INDIVIDUAL_PERSONAL_DATA_TAG);
        button_tags.add(Constants.INDIVIDUAL_DEVELOPMENT_TAG);
        button_tags.add(Constants.INDIVIDUAL_EXCLUSION_CRITERIA_TAG);
        button_tags.add(Constants.INDIVIDUAL_REPORT_TAG);

        for (int i = 0; i < buttons.size(); i++) {
            setButton(buttons.get(i), button_tags.get(i));
        }
    }

    private void setButton(Button button, final String tag) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                event.onMenuOptionSelect(tag);
            }
        });
    }

    private void hideApplyTest() {
        IndividualExclusion exclusion = getIndividual().getIndividualExclusion();

        if (exclusion.getHearingDeficiency() || exclusion.getMentalDisability() ||
                exclusion.getNervousSystemDisease() || exclusion.getNeurologicalDisorder()
                || exclusion.getPsychosis() || exclusion.getVisualImpairment())
            fragment.getApplyTest().setVisibility(View.INVISIBLE);

    }
}
