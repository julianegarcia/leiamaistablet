package br.com.leiamaisapp.leiamais.Events.Common;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.R;

/**
 * Created by erickson on 06/08/16.
 */
public class ImageEvents {

    public ImageEvents(Activity activity) {
        this.activity = activity;
    }

    private Activity activity;

    public String takePhoto() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File imageFile = getPicture();

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));

        activity.startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);

        return imageFile.getAbsolutePath();
    }

    private File getPicture() {
        File directory = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        SimpleDateFormat sdf = new SimpleDateFormat(Constants.IMAGE_DATE_FORMAT);
        String timestamp = sdf.format(new Date());

        File myPhoto = new File(directory, Constants.IMAGE_NAME + timestamp + ".jpg");

        return myPhoto;
    }

    public void setImageViewPhoto(ImageView mImageView, File photo) {
        Picasso.with(activity).load(photo)
                .placeholder(R.drawable.user_placehoder)
                .error(R.drawable.user_placehoder)
                .into(mImageView);
    }

}
