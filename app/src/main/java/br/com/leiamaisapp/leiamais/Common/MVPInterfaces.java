package br.com.leiamaisapp.leiamais.Common;

import java.io.Serializable;

import br.com.leiamaisapp.database.Model.Individual;
import br.com.leiamaisapp.leiamais.Presenters.Individual.MainWindowIndividualActivityPresenter;

/**
 * Created by erickson on 24/07/16.
 */
public interface MVPInterfaces {

    /*
    * Required methods for the fragments
    * (Extends Serializable, so it can be passed in the Intent)
     */
    interface FragmentRequires extends Serializable {
    }

    interface IndividualFragmentPresenterRequires {
        Individual getIndividual();
    }

    interface IndividualMenuInterface {
        MainWindowIndividualActivityPresenter getMActivityPresenter();
    }
}
