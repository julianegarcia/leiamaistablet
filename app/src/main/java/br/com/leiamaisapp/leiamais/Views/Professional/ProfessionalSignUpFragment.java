package br.com.leiamaisapp.leiamais.Views.Professional;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import br.com.leiamaisapp.leiamais.Common.MVPInterfaces;
import br.com.leiamaisapp.leiamais.Presenters.Professional.ProfessionalSignUpFragmentPresenter;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by erickson on 23/07/16.
 */
public class ProfessionalSignUpFragment extends LeiaMaisFragment implements MVPInterfaces.FragmentRequires {

    public ProfessionalSignUpFragment() {
        setPresenter(new ProfessionalSignUpFragmentPresenter(this));
    }

    @BindView(R.id.frg_professional_signup_imageViewPlaceHolderImage)
    CircleImageView imageViewPlaceHolderImage;
    @BindView(R.id.frg_professional_signup_buttonTakePhoto)
    Button takePhoto;
    @BindView(R.id.frg_professional_signup_spinnerTitle)
    Spinner title;
    @BindView(R.id.frg_professional_signup_editTextName)
    EditText name;
    @BindView(R.id.frg_professional_signup_editTextEmail)
    EditText email;
    @BindView(R.id.frg_professional_signup_editTextPassword)
    EditText password;
    @BindView(R.id.frg_professional_signup_editTextPasswordConfirmation)
    EditText passwordConfirmation;
    @BindView(R.id.frg_professional_signup_dataPickerBirthday)
    EditText birthday;
    @BindView(R.id.frg_professional_signup_spinnerSex)
    Spinner sex;
    @BindView(R.id.frg_professional_signup_editTextRg)
    EditText rg;
    @BindView(R.id.frg_professional_signup_editTextCpf)
    EditText cpf;
    @BindView(R.id.frg_professional_signup_editTextFunction)
    EditText function;
    @BindView(R.id.frg_professional_signup_spinnerProfissionalArea)
    Spinner professionalArea;
    @BindView(R.id.frg_professional_signup_buttonSubmit)
    Button submit;

    public Button getSubmit() {
        return submit;
    }

    public Button getTakePhoto() {
        return takePhoto;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profissional_signup, container, false);
        ButterKnife.bind(this, view);

        getPresenter().startPresenter();
        return view;
    }

    public ProfessionalSignUpFragmentPresenter getPresenter() {
        return (ProfessionalSignUpFragmentPresenter) super.getPresenter();
    }

    public ImageView getImageViewPlaceHolderImage() {
        return imageViewPlaceHolderImage;
    }

    public Spinner getTitle() {
        return title;
    }

    public EditText getName() {
        return name;
    }

    public EditText getEmail() {
        return email;
    }

    public EditText getPassword() {
        return password;
    }

    public EditText getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public EditText getBirthday() {
        return birthday;
    }

    public Spinner getSex() {
        return sex;
    }

    public EditText getRg() {
        return rg;
    }

    public EditText getCpf() {
        return cpf;
    }

    public EditText getFunction() {
        return function;
    }

    public Spinner getProfessionalArea() {
        return professionalArea;
    }

}
