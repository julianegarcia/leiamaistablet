package br.com.leiamaisapp.leiamais.Tests;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisActivity;
import br.com.leiamaisapp.leiamais.Views.Individual.MainWindowIndividualActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EndAllTest extends LeiaMaisActivity {

    Long idProfessional;

    @BindView(R.id.reiniciar_testes)
    Button reiniciar;

    @BindView(R.id.voltar_testes)
    Button voltar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        idProfessional = getIntent().getLongExtra(Constants.PROFESSIONAL_EMAIL_TAG, 66);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_end_allt_tests);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.voltar_testes)
    void onClickVoltar() {
        Intent mIntent = new Intent(this, MainWindowIndividualActivity.class);
        mIntent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, idProfessional);
        startActivity(mIntent);
    }

    @OnClick(R.id.reiniciar_testes)
    void onClickReiniciar() {
        Intent mIntent = new Intent(this, Alphabet.class);
        mIntent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, idProfessional);
        startActivity(mIntent);
    }
}
