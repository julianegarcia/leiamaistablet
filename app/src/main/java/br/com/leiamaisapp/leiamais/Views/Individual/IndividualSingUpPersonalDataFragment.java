package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;

import br.com.leiamaisapp.leiamais.Forms.DataPicker;
import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualSingUpPersonalDataFragmentPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by erickson on 20/08/16.
 */
public class IndividualSingUpPersonalDataFragment extends IndividualFragment {

    public IndividualSingUpPersonalDataFragment() {
        setPresenter(new IndividualSingUpPersonalDataFragmentPresenter(this));
    }

    @BindView(R.id.frg_individual_signup_imageViewPlaceHolderImage)
    CircleImageView placeHolderImage;
    @BindView(R.id.frg_individual_signup_buttonTakePhoto)
    Button takePhoto;
    @BindView(R.id.frg_individual_signup_editTextName)
    EditText name;
    @BindView(R.id.frg_individual_signup_dataPickerBirthday)
    DataPicker birthday;
    @BindView(R.id.frg_individual_signup_spinnerSex)
    Spinner sex;
    @BindView(R.id.frg_individual_signup_editTextCpf)
    EditText cpf;
    @BindView(R.id.frg_individual_signup_editTextResponsibleName)
    EditText responsibleName;
    @BindView(R.id.frg_individual_signup_editTextFatherName)
    EditText fatherName;
    @BindView(R.id.frg_individual_signup_switchFamiliarHistoric)
    Switch hasFamiliarHistoric;
    @BindView(R.id.frg_individual_signup_editTextFamiliarHistoric)
    EditText familiarHistoric;
    @BindView(R.id.frg_individual_signup_switchConsanguinityHistoric)
    Switch hasConsanguinityHistoric;
    @BindView(R.id.frg_individual_signup_editTextConsanguinityHistoric)
    EditText consanguinityHistoric;
    @BindView(R.id.frg_individual_signup_switchDesiredGestation)
    Switch hasDesiredGestation;
    @BindView(R.id.frg_individual_signup_editTextDesiredGestation)
    EditText desiredGestation;
    @BindView(R.id.frg_individual_signup_switchPregnancyComplications)
    Switch hasPregnancyComplications;
    @BindView(R.id.frg_individual_signup_editTextPregnancyComplications)
    EditText pregnancyComplications;
    @BindView(R.id.frg_individual_signup_switchMaternalHistory)
    Switch hasMaternalHistory;
    @BindView(R.id.frg_individual_signup_editTextMaternalHistory)
    EditText maternalHistory;
    @BindView(R.id.frg_individual_signup_switchNeonatalComplications)
    Switch hasNeonatalComplications;
    @BindView(R.id.frg_individual_signup_editTextNeonatalComplications)
    EditText neonatalComplications;
    @BindView(R.id.frg_individual_signup_switchComorbidities)
    Switch hasComorbidities;
    @BindView(R.id.frg_individual_signup_editTextComorbidities)
    EditText comorbidities;
    @BindView(R.id.frg_individual_signup_editTextTelephone)
    EditText telephone;
    @BindView(R.id.frg_individual_signup_editTextAddress)
    EditText address;
    @BindView(R.id.frg_individual_signup_editTextResponsibleEmail)
    EditText responsibleEmail;
    @BindView(R.id.frg_individual_signup_buttonPDNext)
    Button next;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_individual_signup_personal_data, container, false);
        ButterKnife.bind(this, view);

        getPresenter().startPresenter();
        return view;
    }

    public ImageView getPlaceHolderImage() {
        return placeHolderImage;
    }

    public Button getTakePhoto() {
        return takePhoto;
    }

    public EditText getName() {
        return name;
    }

    public DataPicker getBirthday() {
        return birthday;
    }

    public Spinner getSex() {
        return sex;
    }

    public EditText getCpf() {
        return cpf;
    }

    public EditText getResponsibleName() {
        return responsibleName;
    }

    public EditText getFatherName() {
        return fatherName;
    }

    public Switch getHasFamiliarHistoric() {
        return hasFamiliarHistoric;
    }

    public EditText getFamiliarHistoric() {
        return familiarHistoric;
    }

    public Switch getHasConsanguinityHistoric() {
        return hasConsanguinityHistoric;
    }

    public EditText getConsanguinityHistoric() {
        return consanguinityHistoric;
    }

    public Switch getHasDesiredGestation() {
        return hasDesiredGestation;
    }

    public EditText getDesiredGestation() {
        return desiredGestation;
    }

    public Switch getHasPregnancyComplications() {
        return hasPregnancyComplications;
    }

    public EditText getPregnancyComplications() {
        return pregnancyComplications;
    }

    public Switch getHasMaternalHistory() {
        return hasMaternalHistory;
    }

    public EditText getMaternalHistory() {
        return maternalHistory;
    }

    public Switch getHasNeonatalComplications() {
        return hasNeonatalComplications;
    }


    public EditText getNeonatalComplications() {
        return neonatalComplications;
    }

    public Switch getHasComorbidities() {
        return hasComorbidities;
    }

    public EditText getComorbidities() {
        return comorbidities;
    }

    public EditText getTelephone() {
        return telephone;
    }

    public EditText getAddress() {
        return address;
    }

    public EditText getResponsibleEmail() {
        return responsibleEmail;
    }

    public Button getNext() {
        return next;
    }
}
