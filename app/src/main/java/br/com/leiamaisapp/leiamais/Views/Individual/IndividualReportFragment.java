package br.com.leiamaisapp.leiamais.Views.Individual;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualReportPresenter;
import br.com.leiamaisapp.leiamais.R;

/**
 * Created by erickson on 04/12/16.
 */

public class IndividualReportFragment extends IndividualFragment {

    WebView wvGrafico;

    public IndividualReportFragment() {
        setPresenter(new IndividualReportPresenter());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_individual_report, container, false);

        wvGrafico = (WebView) view.findViewById(R.id.wvGrafico);

        final ProgressDialog progDailog = ProgressDialog.show(getContext(), "Loading", "Please wait...", true);
        progDailog.setCancelable(false);

        String url = "http://apileiamais.itcalendar.com.br/api/individuals/charts?id=" + getMActivityPresenter().getIndividual().getIdBase();

        wvGrafico.getSettings().setJavaScriptEnabled(true);
        wvGrafico.getSettings().setLoadWithOverviewMode(true);
        wvGrafico.getSettings().setUseWideViewPort(true);
        wvGrafico.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        wvGrafico.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progDailog.show();
                view.loadUrl(url);

                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                progDailog.dismiss();
            }
        });

        wvGrafico.loadUrl(url);
        return view;
    }
}
