package br.com.leiamaisapp.leiamais.Tests;

import android.view.View;
import android.widget.ImageView;

/**
 * Created by uriel-miranda on 18/09/16.
 */
public interface Testable {

    public int getLayout();

    public void start(View v);

    public void stop(View v);

    public void restart(View v);

    public void exit(View v);

    public void info(View v);

    public boolean finished(View v);

    public String getTestInfo();

    public int getTotalAnimationDuration();

}
