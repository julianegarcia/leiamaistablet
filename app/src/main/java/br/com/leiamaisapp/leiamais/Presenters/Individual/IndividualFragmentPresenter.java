package br.com.leiamaisapp.leiamais.Presenters.Individual;

import android.content.Intent;

import br.com.leiamaisapp.database.Model.Individual;
import br.com.leiamaisapp.leiamais.Common.MVPInterfaces;
import br.com.leiamaisapp.leiamais.Presenters.Common.LeiaMaisPresenter;

/**
 * Created by erickson on 04/09/16.
 */
public abstract class IndividualFragmentPresenter extends LeiaMaisPresenter
        implements MVPInterfaces.IndividualFragmentPresenterRequires {

    private MainWindowIndividualActivityPresenter mActivityPresenter;

    public Individual getIndividual() {
        return mActivityPresenter.getIndividual();
    }

    public void setIndividual(Individual individual) {
        mActivityPresenter.setIndividual(individual);
    }
    public void setIndividual(String id) {
        mActivityPresenter.setIndividual(id);
    }

    public MainWindowIndividualActivityPresenter getMActivityPresenter() {
        return mActivityPresenter;
    }

    public void setMActivityPresenter(MainWindowIndividualActivityPresenter mActivityPresenter) {
        this.mActivityPresenter = mActivityPresenter;
    }

    public boolean saveIndividual() {
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

}
