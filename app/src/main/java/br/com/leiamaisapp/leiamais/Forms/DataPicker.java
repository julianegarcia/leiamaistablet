package br.com.leiamaisapp.leiamais.Forms;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by erickson on 24/07/2016.
 */
public class DataPicker extends EditText implements View.OnClickListener {

    private Context context;

    private SimpleDateFormat dateFormat;
    private String title = "Set a Date";
    private String buttonPositive = "Ok";
    private String buttonNegative = "Cancel";
    private String mDateFormat = "dd-MM-yyyy";

    public void setMDateFormat(String mDateFormat) {
        this.mDateFormat = mDateFormat;
        dateFormat = new SimpleDateFormat(mDateFormat);
    }

    public void setButtonPositive(String buttonPositive) {
        this.buttonPositive = buttonPositive;
    }

    public void setButtonNegative(String buttonNegative) {
        this.buttonNegative = buttonNegative;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DataPicker(Context context) {
        super(context);

        init(context);
    }

    public DataPicker(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    public DataPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);
    }

    private void init(Context context) {
        this.context = context;

        dateFormat = new SimpleDateFormat(mDateFormat);

        setInputType(InputType.TYPE_NULL);
        setRawInputType(InputType.TYPE_CLASS_TEXT);

        setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        showDataPicker();
    }

    private void showDataPicker() {
        String mConteudo = getText().toString();

        Calendar mDate = Calendar.getInstance();
        mDate.setTimeZone(TimeZone.getDefault());

        try {
            mDate.setTime(dateFormat.parse(mConteudo));
        } catch (Exception e) {
        }

        int year = mDate.get(Calendar.YEAR);
        int month = mDate.get(Calendar.MONTH);
        int dayOfMonth = mDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(
                context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar mCInterno = Calendar.getInstance();

                        mCInterno.set(year, monthOfYear, dayOfMonth);

                        setText(dateFormat.format(mCInterno.getTime()));
                    }
                },
                year,
                month,
                dayOfMonth
        );

        mDatePicker.setTitle(title);
        mDatePicker.setButton(DatePickerDialog.BUTTON_POSITIVE, buttonPositive, mDatePicker);
        mDatePicker.setButton(DatePickerDialog.BUTTON_NEGATIVE, buttonNegative, (DialogInterface.OnClickListener) null);

        mDatePicker.show();
    }

}
