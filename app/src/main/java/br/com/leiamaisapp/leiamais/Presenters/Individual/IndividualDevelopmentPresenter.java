package br.com.leiamaisapp.leiamais.Presenters.Individual;

import br.com.leiamaisapp.database.Model.Individual;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualDevelopmentFragment;

/**
 * Created by erickson on 21/10/16.
 */

public class IndividualDevelopmentPresenter extends IndividualFragmentPresenter {

    public IndividualDevelopmentPresenter(IndividualDevelopmentFragment fragment) {
        this.fragment = fragment;
    }

    private IndividualDevelopmentFragment fragment;

    @Override
    public void startPresenter() {
        setTextViews();
    }

    private void setTextViews() {
        Individual individual = getIndividual();

        fragment.getGrade().setText(individual.getGrade() != null ? individual.getGrade() : "Não");
        fragment.getRegularSchool().setText(individual.getRegularSchool() != null ? individual.getRegularSchool() : "Não");
        fragment.getSchoolSupport().setText(individual.getSchoolSupport() != null ? individual.getSchoolSupport() : "Não");
        fragment.getTdhaTreatment().setText(individual.getTdhaTreatment() != null ? individual.getTdhaTreatment() : "Não");
        fragment.getSpeakProblem().setText(individual.getSpeakProblem() != null ? individual.getSpeakProblem() : "Não");
    }
}
