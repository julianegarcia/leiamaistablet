package br.com.leiamaisapp.leiamais.Presenters.Common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisActivity;

/**
 * Created by erickson on 24/07/16.
 */
public class FragmentPresenter {

    FragmentManager fragmentManager;

    public void changeFragments(LeiaMaisActivity activity, String fragmentTag, int viewId, Fragment fragmentReplacer) {
        if (fragmentManager == null)
            fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(viewId, fragmentReplacer, fragmentTag);
        ft.commit();
    }

}
