package br.com.leiamaisapp.leiamais.Presenters.Professional;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import br.com.leiamaisapp.database.Dao.ProfessionalDAO;
import br.com.leiamaisapp.database.Model.Professional;
import br.com.leiamaisapp.leiamais.Common.CPFValidator;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Common.DateValidator;
import br.com.leiamaisapp.leiamais.Events.Common.CommonEvents;
import br.com.leiamaisapp.leiamais.Events.Common.ImageEvents;
import br.com.leiamaisapp.leiamais.Events.LoginEvent;
import br.com.leiamaisapp.leiamais.Forms.DataPicker;
import br.com.leiamaisapp.leiamais.Forms.MaskEditTextTChangedListener;
import br.com.leiamaisapp.leiamais.Presenters.Common.LeiaMaisPresenter;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Professional.ProfessionalSignUpFragment;

/**
 * Created by erickson on 24/07/16.
 */
public class ProfessionalSignUpFragmentPresenter extends LeiaMaisPresenter {

    private ProfessionalSignUpFragment fragment;
    private Context context;

    private ImageEvents fEvent;

    private ArrayAdapter<String> titleAdapter;
    private ArrayList<String> titleStrings;

    private ArrayAdapter<String> sexAdapter;
    private ArrayList<String> sexStrings;

    private ArrayAdapter<String> professionalAreaAdapter;
    private ArrayList<String> professionalAreaStrings;

    private Professional professional;
    private HashMap<String, String> params;

    private String photoPath = "";

    public ProfessionalSignUpFragmentPresenter(ProfessionalSignUpFragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public void startPresenter() {
        this.context = fragment.getActivity().getApplicationContext();
        fEvent = new ImageEvents(fragment.getActivity());
        setProfessional();
//        Set the Take Photo button
        setButtonTakePhoto();
//        Set the birthday date values according with the values in string.xml
        setDateFormatBirthDay(new DateValidator().getDateValue(professional.getBirthday()));
//        Set the title spinner
        setSpinnerTitle(professional.getTitle());
//        Set the sex spinner
        setSpinnerSex(professional.getSex());
//        Set the Professional Area spinner
        setSpinnerProfessionalArea(professional.getField());
//        Set the Buttons Listeners
        setButtonSubmit();
//        Set the EditText Masks
        setMasks();
    }

    private void setProfessional() {
        Bundle bundle = fragment.getActivity().getIntent().getExtras();
        if (bundle != null) {
            Long id = bundle.getLong(Constants.PROFESSIONAL_EMAIL_TAG);

            if (id != null) {
                ProfessionalDAO dao = new ProfessionalDAO();
                professional = dao.findById(id);
            }


            if (professional != null)
                setEditTexts();
        }

        if (professional == null)
            professional = new Professional();
        else
            photoPath = professional.getPhoto();
    }

    private void setEditTexts() {
        fragment.getName().setText(professional.getName());
        fragment.getCpf().setText(professional.getCpf());
        fragment.getRg().setText(professional.getRg());
        fragment.getFunction().setText(professional.getFunction());
        fragment.getEmail().setText(professional.getEmail());
    }

    private void setButtonTakePhoto() {
        fragment.getTakePhoto().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoPath = fEvent.takePhoto();
            }
        });
        setPlaceHolderImagem();
    }

    private void setPlaceHolderImagem() {
        try {
            final File file = new File(photoPath);
            fEvent.setImageViewPhoto(
                    fragment.getImageViewPlaceHolderImage()
                    , file
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setButtonSubmit() {
        fragment.getSubmit().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfessionalDAO dao = new ProfessionalDAO();
                if (validateProfessional(dao)) {
                    dao.save(professional);
                    fragment.getActivity().onBackPressed();
                }

            }
        });
    }

    private void setDateFormatBirthDay(@Nullable String value) {
        DataPicker dataPickerBirthday = (DataPicker) fragment.getBirthday();
        dataPickerBirthday.setMDateFormat(fragment.getString(R.string.dt_picker_date_format));
        dataPickerBirthday.setTitle(fragment.getString(R.string.dt_picker_select_a_date));
        dataPickerBirthday.setButtonPositive(fragment.getString(R.string.dt_picker_button_positive));
        dataPickerBirthday.setButtonNegative(fragment.getString(R.string.dt_picker_button_negative));
        dataPickerBirthday.setText(value);
    }

    private void setSpinnerTitle(@Nullable String title) {
        titleStrings = loadTitleStrings();
        titleAdapter = new ArrayAdapter<>(
                context,
                R.layout.spiner_cell,
                titleStrings
        );

        titleAdapter.setDropDownViewResource(R.layout.spinner_cell_drop);

        fragment.getTitle().setAdapter(titleAdapter);
        fragment.getTitle().setSelection(title != null ? titleStrings.indexOf(title) : 0);
    }

    private void setSpinnerSex(@Nullable String sex) {
        sexStrings = loadSexStrings();
        sexAdapter = new ArrayAdapter<>(
                context,
                R.layout.spiner_cell,
                sexStrings
        );

        sexAdapter.setDropDownViewResource(R.layout.spinner_cell_drop);

        fragment.getSex().setAdapter(sexAdapter);
        fragment.getSex().setSelection(sex != null ? sexStrings.indexOf(sex) : 0);
    }

    private void setSpinnerProfessionalArea(@Nullable String area) {
        professionalAreaStrings = loadProfessionalAreaStrings();
        professionalAreaAdapter = new ArrayAdapter<>(
                context,
                R.layout.spiner_cell,
                professionalAreaStrings
        );

        professionalAreaAdapter.setDropDownViewResource(R.layout.spinner_cell_drop);

        fragment.getProfessionalArea().setAdapter(professionalAreaAdapter);
        fragment.getProfessionalArea().setSelection(area != null ? professionalAreaStrings.indexOf(area) : 0);
    }

    private void setMasks() {
        MaskEditTextTChangedListener maskBirthday = new MaskEditTextTChangedListener(
                context.getString(R.string.date_mask), fragment.getBirthday());
        MaskEditTextTChangedListener maskCPF = new MaskEditTextTChangedListener(
                context.getString(R.string.cpf_mask), fragment.getCpf());
        MaskEditTextTChangedListener maskRG = new MaskEditTextTChangedListener(
                context.getString(R.string.rg_mask), fragment.getRg());
        fragment.getBirthday().addTextChangedListener(maskBirthday);
        fragment.getCpf().addTextChangedListener(maskCPF);
        fragment.getRg().addTextChangedListener(maskRG);
    }

    private boolean validateProfessional(ProfessionalDAO dao) {
        LoginEvent lEvent = new LoginEvent();
        String name = fragment.getName().getText().toString();
        String birthday = fragment.getBirthday().getText().toString();
        String password = fragment.getPassword().getText().toString();
        String passwordConfirmation = fragment.getPasswordConfirmation().getText().toString();
        String cpf = fragment.getCpf().getText().toString();
        String rg = fragment.getRg().getText().toString();
        String title = loadTitleStrings().get(fragment.getTitle().getSelectedItemPosition());
        String sex = loadSexStrings().get(fragment.getSex().getSelectedItemPosition());
        String field = loadProfessionalAreaStrings().get(
                fragment.getProfessionalArea().getSelectedItemPosition());
        String function = fragment.getFunction().getText().toString();
        String email = fragment.getEmail().getText().toString();
        Professional mProfessional = dao.findByEmail(email);
        Date mBirthday;

        CommonEvents event = new CommonEvents(fragment.getActivity());

        if (name.isEmpty()) {
            event.showMessage(context.getString(R.string.error_empty_name));
        } else if (email.isEmpty()) {
            event.showMessage(context.getString(R.string.error_empty_email));
        } else if (mProfessional != null && professional.getId() != mProfessional.getId()) {
            event.showMessage(context.getString(R.string.error_email_already_exists));
        } else if (title.equals(fragment.getString(R.string.frg_professional_signup_title))) {
            event.showMessage(context.getString(R.string.error_select_title));
        } else if (title.equals(fragment.getString(R.string.frg_professional_signup_professional_area))) {
            event.showMessage(context.getString(R.string.error_select_professional_area));
        } else if (password.isEmpty()) {
            event.showMessage(context.getString(R.string.error_empty_password));
        } else if (!password.equals(passwordConfirmation)) {
            event.showMessage(context.getString(R.string.error_different_password));
        } else if (cpf.isEmpty()) {
            event.showMessage(context.getString(R.string.error_empty_cpf));
        } else if (!new DateValidator().isThisDateValid(
                birthday, context.getString(R.string.dt_picker_date_format))) {
            event.showMessage(context.getString(R.string.error_invalid_date));
        } else if (sex.equals(fragment.getString(R.string.form_common_sex))) {
            event.showMessage(context.getString(R.string.error_select_sex));
        } else if (!new CPFValidator().isCPF(cpf)) {
            event.showMessage(context.getString(R.string.error_invalid_cpf));
        } else {
            try {
                mBirthday = new SimpleDateFormat(context.getString(R.string.dt_picker_date_format)).parse(birthday);
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            }

            password = lEvent.textMD5(password);

            professional.setName(name);
            professional.setTitle(title);
            professional.setBirthday(mBirthday);
            professional.setSex(sex);
            professional.setCpf(cpf);
            professional.setRg(rg);
            professional.setField(field);
            professional.setFunction(function);
            professional.setEmail(email);
            professional.setPassword(password);
            professional.setPhoto(photoPath);

            return true;
        }

        return false;
    }

    private ArrayList<String> loadTitleStrings() {
//        TODO: Bring it from the database

        ArrayList<String> strings = new ArrayList<>();

        strings.add(fragment.getString(R.string.frg_professional_signup_title));
        strings.add("Dr.");
        strings.add("Msc.");
        strings.add("Esp.");

        return strings;
    }

    private ArrayList<String> loadSexStrings() {
//        TODO: Bring it from the database

        ArrayList<String> strings = new ArrayList<>();

        strings.add(fragment.getString(R.string.form_common_sex));
        strings.add(fragment.getString(R.string.form_common_sex_f));
        strings.add(fragment.getString(R.string.form_common_sex_m));

        return strings;
    }

    private ArrayList<String> loadProfessionalAreaStrings() {
//        TODO: Bring it from the database

        ArrayList<String> strings = new ArrayList<>();

        strings.add(fragment.getString(R.string.frg_professional_signup_professional_area));
        strings.add("Neurologia");
        strings.add("Pediatria");
        strings.add("Fonoaudiologia");
        strings.add("Pedagogia");

        return strings;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.CAMERA_REQUEST:
                setPlaceHolderImagem();
                break;
        }
    }

}
