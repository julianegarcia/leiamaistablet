package br.com.leiamaisapp.leiamais.Presenters.Individual;

import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;

import java.util.ArrayList;

import br.com.leiamaisapp.database.Model.Individual;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Events.Individual.IndividualSingUpEvents;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSingUpPersonalDevelopmentFragment;

/**
 * Created by erickson on 27/08/16.
 */
public class IndividualSingUpPersonalDevelopmentFragmentPresenter extends IndividualFragmentPresenter {

    public IndividualSingUpPersonalDevelopmentFragmentPresenter(IndividualSingUpPersonalDevelopmentFragment fragment) {
        this.fragment = fragment;
    }

    IndividualSingUpPersonalDevelopmentFragment fragment;

    Context context;

    private ArrayAdapter<String> gradeAdapter;
    private ArrayList<String> gradeStrings;

    private void setGradeSpinner() {
        gradeStrings = loadGradeStrings();
        gradeAdapter = new ArrayAdapter<>(
                context,
                R.layout.spiner_cell,
                gradeStrings
        );

        gradeAdapter.setDropDownViewResource(R.layout.spinner_cell_drop);

        fragment.getGrade().setAdapter(gradeAdapter);
        fragment.getGrade().setSelection(0);
    }

    private ArrayList<String> loadGradeStrings() {
//        TODO: Bring it from the database
        ArrayList<String> strings = new ArrayList<>();

        strings.add(fragment.getString(R.string.frg_individual_signup_form_development_grade_select));

        for (int i = 2; i < 6; i++)
            strings.add(i + "º ano");

        return strings;
    }

    @Override
    public void startPresenter() {
        context = fragment.getActivity().getApplicationContext();
        setIndividual(fragment.getMActivityPresenter().getIndividual());
        setGradeSpinner();
        setRegularSchoolSwitch();
        setIndividualForm();
        setTDHASwitch();
        setBackButton();
        setNextButton();
    }

    private void setBackButton() {
        fragment.getBack().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveIndividual())
                    getMActivityPresenter().changeMainWindowFragment(Constants.INDIVIDUAL_SIGN_UP_PERSONAL_DATA_TAG, true);
            }
        });
    }

    private void setNextButton() {
        fragment.getNext().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveIndividual())
                    getMActivityPresenter().changeMainWindowFragment(
                            (
                                    fragment.getTdhaTreatment().isChecked() ?
                                            Constants.INDIVIDUAL_SIGN_UP_EXCLUSION_TAG :
                                            Constants.INDIVIDUAL_SIGN_UP_TDHA_TAG
                            ), true
                    );
            }
        });
    }

    private void setRegularSchoolSwitch() {
        fragment.getRegularSchool().setVisibility(View.INVISIBLE);
        fragment.getHasRegularSchool().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                IndividualSingUpEvents event = new IndividualSingUpEvents();
                event.changeEditTextVisibility(compoundButton, fragment.getRegularSchool());
            }
        });
    }

    private void setIndividualForm() {
        Individual individual = getIndividual();

        int ind = loadGradeStrings().indexOf(individual.getGrade());

        fragment.getGrade().setSelection(ind);

        fragment.getSchoolSupport().setChecked(individual.getSchoolSupport() != null ? true : false);

        fragment.getTdhaTreatment().setChecked(individual.getTdhaTreatment() != null ? true : false);

        if (individual.getRegularSchool() != null) {
            fragment.getHasRegularSchool().setChecked(true);
            fragment.getRegularSchool().setText(individual.getRegularSchool());
        }

        fragment.getSpeakProblem().setChecked(individual.getSpeakProblem() != null ? true : false);
    }


    private void setTDHASwitch() {
        fragment.getTdhaTreatment().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getMActivityPresenter().hideTDHAButton(isChecked);
            }
        });
    }

    @Override
    public boolean saveIndividual() {

        Individual individual = getIndividual();

        String responsePositive = fragment.getString(R.string.frg_individual_signup_switch_on);

        int ind = fragment.getGrade().getSelectedItemPosition();

        individual.setGrade(loadGradeStrings().get(ind));

        individual.setSchoolSupport(fragment.getSchoolSupport().isChecked() ? responsePositive : null);
        individual.setTdhaTreatment(fragment.getTdhaTreatment().isChecked() ? responsePositive : null);
        individual.setRegularSchool(fragment.getHasRegularSchool().isChecked() ?
                fragment.getRegularSchool().getText().toString() : null);
        individual.setSpeakProblem(fragment.getSpeakProblem().isChecked() ? responsePositive : null);

        return true;
    }
}
