package br.com.leiamaisapp.leiamais.Views.Common;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import br.com.leiamaisapp.leiamais.Presenters.Common.LeiaMaisPresenter;

/**
 * Created by erickson on 24/07/16.
 */
public abstract class LeiaMaisActivity extends AppCompatActivity {
    private LeiaMaisPresenter presenter;

    public LeiaMaisPresenter getPresenter() {
        return presenter;
    }

    public void setPresenter(LeiaMaisPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
    }
}
