package br.com.leiamaisapp.leiamais.Views.Professional;

import android.content.Intent;
import android.os.Bundle;

import br.com.leiamaisapp.leiamais.Presenters.Professional.MainWindowProfessionalActivityPresenter;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisActivity;

public class MainWindowProfessionalActivity extends LeiaMaisActivity {

    public MainWindowProfessionalActivity() {
        this.presenter = new MainWindowProfessionalActivityPresenter(this);
    }

    private MainWindowProfessionalActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_window);
        presenter.startPresenter();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }
}
