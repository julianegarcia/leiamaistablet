package br.com.leiamaisapp.leiamais.Presenters.Individual;

import br.com.leiamaisapp.leiamais.Views.Individual.DefaultIndividualMenuFragment;

/**
 * Created by erickson on 10/10/16.
 */

public class DefaultIndividualMenuPresenter extends IndividualFragmentPresenter {
    public DefaultIndividualMenuPresenter(DefaultIndividualMenuFragment fragment) {
        this.fragment = fragment;
    }

    DefaultIndividualMenuFragment fragment;

    @Override
    public void startPresenter() {

    }

    @Override
    public boolean saveIndividual() {
        return true;
    }

}
