package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualSearchDrawerPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by erickson on 09/10/16.
 */

public class IndividualSearchDrawer extends IndividualFragment {

    public IndividualSearchDrawer() {
        setPresenter(new IndividualSearchDrawerPresenter(this));
    }

    @BindView(R.id.drw_alphabet_views)
    LinearLayout alphabet_views;
    @BindView(R.id.drw_list_individual)
    RecyclerView individualsList;
    @BindView(R.id.drw_search_go_back)
    Button goBack;
    @BindView(R.id.drw_search_by_name)
    EditText searchByName;
    @BindView(R.id.drw_search)
    Button search;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drawer_individual_search, container, false);
        ButterKnife.bind(this, view);

        generateAlphabetViews();

        getPresenter().startPresenter();

        return view;
    }

    public RecyclerView getIndividualsList() {
        return individualsList;
    }

    public Button getGoBack() {
        return goBack;
    }

    public EditText getSearchByName() {
        return searchByName;
    }

    public Button getSearch() {
        return search;
    }

    private void generateAlphabetViews() {
        int max = 7;
        int s = 1;
        LinearLayout ll = new LinearLayout(this.getActivity());
        ll.setGravity(Gravity.CENTER);
        for (char c = 'A'; c <= 'Z'; c++) {
            TextView letter = new TextView(this.getActivity());
            letter.setText(String.valueOf(c));
            letter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView b = (TextView) v;
                    ((IndividualSearchDrawerPresenter) getPresenter()).swapIndividualsList(b.getText().toString());
                }
            });

            final float scale = getContext().getResources().getDisplayMetrics().density;
            letter.setWidth((int) (30 * scale));
            letter.setHeight((int) (30 * scale));

            letter.setTextSize(12);
            ll.addView(letter);
            if (++s > max || c == 'Z') {
                alphabet_views.addView(ll);
                ll = new LinearLayout(this.getActivity());
                ll.setGravity(Gravity.CENTER);
                s = 1;
            }
        }
    }

    public void notifyDataSetChanged() {
        individualsList.getAdapter().notifyDataSetChanged();
    }
}
