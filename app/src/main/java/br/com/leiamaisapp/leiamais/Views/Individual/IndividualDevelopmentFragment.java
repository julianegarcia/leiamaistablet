package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualDevelopmentPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by erickson on 21/10/16.
 */

public class IndividualDevelopmentFragment extends IndividualFragment {

    public IndividualDevelopmentFragment() {
        setPresenter(new IndividualDevelopmentPresenter(this));
    }

    @BindView(R.id.frg_individual_development_textViewGrade)
    TextView grade;
    @BindView(R.id.frg_individual_development_textViewRegularSchool)
    TextView regularSchool;
    @BindView(R.id.frg_individual_development_textViewSchoolSupport)
    TextView schoolSupport;
    @BindView(R.id.frg_individual_development_textViewTdhaTreatment)
    TextView tdhaTreatment;
    @BindView(R.id.frg_individual_development_textViewSpeakProblem)
    TextView speakProblem;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_individual_development, container, false);
        ButterKnife.bind(this, view);
        getPresenter().startPresenter();
        return view;
    }

    public TextView getGrade() {
        return grade;
    }

    public TextView getRegularSchool() {
        return regularSchool;
    }

    public TextView getSchoolSupport() {
        return schoolSupport;
    }

    public TextView getTdhaTreatment() {
        return tdhaTreatment;
    }

    public TextView getSpeakProblem() {
        return speakProblem;
    }
}
