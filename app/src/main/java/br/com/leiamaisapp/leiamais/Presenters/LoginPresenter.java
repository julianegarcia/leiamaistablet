package br.com.leiamaisapp.leiamais.Presenters;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import java.util.List;

import br.com.leiamaisapp.database.Dao.IndividualDAO;
import br.com.leiamaisapp.database.Dao.ProfessionalDAO;
import br.com.leiamaisapp.database.Model.Professional;
import br.com.leiamaisapp.leiamais.Events.LoginEvent;
import br.com.leiamaisapp.leiamais.Exceptions.UserNotFoundException;
import br.com.leiamaisapp.leiamais.Exceptions.WrongPasswordException;
import br.com.leiamaisapp.leiamais.LeiaMaisApplication;
import br.com.leiamaisapp.leiamais.Presenters.Common.LeiaMaisPresenter;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.LoginActivity;
import br.com.leiamaisapp.restapi.rest.RestModel.GetIndividualsReturn;
import br.com.leiamaisapp.restapi.rest.RestModel.GetProfessionalsReturn;
import br.com.leiamaisapp.restapi.rest.RestModel.ProfessionalAPI;
import br.com.leiamaisapp.restapi.rest.service.RestService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by erickson on 26/08/16.
 */
public class LoginPresenter extends LeiaMaisPresenter {
    private LoginActivity activity;
    private LoginEvent event;
    RestService service;

    public LoginPresenter(LoginActivity activity) {
        this.activity = activity;
        event = new LoginEvent();
        service = LeiaMaisApplication.getRestClient().getService();
    }

    @Override
    public void startPresenter() {
        setLoginButtonSubmit();
        loadIndividualsFromServer();
        loadProfessionalsFromServer();
    }

    private void setLoginButtonSubmit() {
        activity.getSignIn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Professional professional = null;

                String email = activity.getEmail().getText().toString();

                String password = activity.getPassword().getText().toString();

                password = event.textMD5(password);

                try {
                    professional = event.doSignIn(email, password);
                } catch (UserNotFoundException e) {
                    activity.getErrorMessage().setText(R.string.error_wrong_login);
                } catch (WrongPasswordException e) {
                    activity.getErrorMessage().setText(R.string.error_wrong_login);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (professional != null) {
                        event.openHome(activity, professional.getId());
                    }
                }
            }
        });
    }

    public void onRestart() {
        cleanLoginFields();
    }

    private void cleanLoginFields() {
        activity.getEmail().setText("");
        activity.getPassword().setText("");
    }

    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(startMain);
    }

    private boolean loadIndividualsFromServer() {

        final boolean[] r = new boolean[1];
        try {
            Call<GetIndividualsReturn> call = service.getIndividuals();

            call.enqueue(new Callback<GetIndividualsReturn>() {
                @Override
                public void onResponse(Call<GetIndividualsReturn> call, Response<GetIndividualsReturn> response) {
                    IndividualDAO dao = new IndividualDAO();
                    dao.saveMany(response.body().convertToIndividuals());
                    r[0] = true;
                }

                @Override
                public void onFailure(Call<GetIndividualsReturn> call, Throwable t) {
                    r[0] = false;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            r[0] = false;
        }

        return r[0];
    }

    private boolean loadProfessionalsFromServer() {
        final boolean[] r = new boolean[1];

        try {
            Call<List<ProfessionalAPI>> call = service.getProfessionals();

            call.enqueue(new Callback<List<ProfessionalAPI>>() {
                @Override
                public void onResponse(Call<List<ProfessionalAPI>> call, Response<List<ProfessionalAPI>> response) {
                    ProfessionalDAO dao = new ProfessionalDAO();
                    for (ProfessionalAPI professionalAPI : response.body()) {
                        dao.save(professionalAPI.convertToProfessional());
                    }
                    r[0] = true;
                }

                @Override
                public void onFailure(Call<List<ProfessionalAPI>> call, Throwable t) {
                    r[0] = false;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            r[0] = false;
        }
        return r[0];
    }
}
