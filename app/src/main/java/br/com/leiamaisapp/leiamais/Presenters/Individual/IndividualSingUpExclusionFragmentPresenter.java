package br.com.leiamaisapp.leiamais.Presenters.Individual;

import android.view.View;

import br.com.leiamaisapp.database.Model.IndividualExclusion;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSingUpExclusionFragment;

/**
 * Created by erickson on 04/09/16.
 */
public class IndividualSingUpExclusionFragmentPresenter extends IndividualFragmentPresenter {

    public IndividualSingUpExclusionFragmentPresenter(IndividualSingUpExclusionFragment fragment) {
        this.fragment = fragment;
    }

    IndividualSingUpExclusionFragment fragment;

    @Override
    public void startPresenter() {
        setIndividualExclusionForm();
        setButtonSubmit();
        setBackButton();
    }

    private void setBackButton() {
        fragment.getBack().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveIndividual())
                    getMActivityPresenter().changeMainWindowFragment(
                            (
                                    getIndividual().getTdhaTreatment() == null ?
                                            Constants.INDIVIDUAL_SIGN_UP_TDHA_TAG :
                                            Constants.INDIVIDUAL_SIGN_UP_PERSONAL_DEVELOPMENT_TAG
                            )
                            , true);
            }
        });
    }

    private void setIndividualExclusionForm() {
        IndividualExclusion individualExclusion = getIndividual().getIndividualExclusion();

        fragment.getMentalDisability().setChecked(individualExclusion.getMentalDisability());
        fragment.getNeurologicalDisorder().setChecked(individualExclusion.getNeurologicalDisorder());
        fragment.getHearingDeficiency().setChecked(individualExclusion.getHearingDeficiency());
        fragment.getVisualImpairment().setChecked(individualExclusion.getVisualImpairment());
        fragment.getNervousSystemDisease().setChecked(individualExclusion.getNervousSystemDisease());
        fragment.getPsychosis().setChecked(individualExclusion.getPsychosis());
    }

    @Override
    public boolean saveIndividual() {
        IndividualExclusion individualExclusion = getIndividual().getIndividualExclusion();

        individualExclusion.setMentalDisability(fragment.getMentalDisability().isChecked());
        individualExclusion.setNeurologicalDisorder(fragment.getNeurologicalDisorder().isChecked());
        individualExclusion.setHearingDeficiency(fragment.getHearingDeficiency().isChecked());
        individualExclusion.setVisualImpairment(fragment.getVisualImpairment().isChecked());
        individualExclusion.setNervousSystemDisease(fragment.getNervousSystemDisease().isChecked());
        individualExclusion.setPsychosis(fragment.getPsychosis().isChecked());

        return true;
    }


    private void setButtonSubmit() {
        fragment.getSubmit().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveIndividual();
                fragment.getMActivityPresenter().saveIndividual();
            }
        });
    }
}
