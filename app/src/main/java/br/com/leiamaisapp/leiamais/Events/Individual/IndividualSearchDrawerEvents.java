package br.com.leiamaisapp.leiamais.Events.Individual;

import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualSearchDrawerPresenter;
import br.com.leiamaisapp.leiamais.Presenters.Individual.MainWindowIndividualActivityPresenter;

/**
 * Created by erickson on 19/10/16.
 */

public class IndividualSearchDrawerEvents {

    public IndividualSearchDrawerEvents(IndividualSearchDrawerPresenter presenter) {
        this.presenter = presenter;
    }

    IndividualSearchDrawerPresenter presenter;

    public void chooseIndividual(String id) {
        MainWindowIndividualActivityPresenter mPresenter = presenter.getMActivityPresenter();
        presenter.setIndividual(id);

        mPresenter.changeMainWindowFragment(Constants.INDIVIDUAL_PERSONAL_DATA_TAG, false);
        mPresenter.changeMenuFragment(Constants.INDIVIDUAL_MENU_TAG);
        mPresenter.closeDrawer();
        mPresenter.changeDrawerFragment(null);
    }
}
