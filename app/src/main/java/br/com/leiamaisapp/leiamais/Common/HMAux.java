package br.com.leiamaisapp.leiamais.Common;

import java.util.HashMap;

/**
 * Created by erickson on 09/07/16.
 */
public class HMAux extends HashMap<String, String> {

    public static final String ID = "id";
    public static final String KEY_01 = "key_01";
    public static final String KEY_02 = "key_02";
    public static final String KEY_03 = "key_03";

    public static String getID() {
        return ID;
    }

    public static String getKey01() {
        return KEY_01;
    }

    public static String getKey02() {
        return KEY_02;
    }

    public static String getKey03() {
        return KEY_03;
    }
}
