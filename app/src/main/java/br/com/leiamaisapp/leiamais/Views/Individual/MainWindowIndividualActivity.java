package br.com.leiamaisapp.leiamais.Views.Individual;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;

import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Common.MVPInterfaces;
import br.com.leiamaisapp.leiamais.Presenters.Individual.MainWindowIndividualActivityPresenter;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainWindowIndividualActivity extends LeiaMaisActivity implements MVPInterfaces.FragmentRequires {

    MainWindowIndividualActivityPresenter presenter;

    @BindView(R.id.act_main_window_fragmentDrawerLayout)
    DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    public MainWindowIndividualActivity() {
        setPresenter(new MainWindowIndividualActivityPresenter(this));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_window);

        ButterKnife.bind(this);
        getPresenter().startPresenter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().reloadProfessional();
        getPresenter().changeDrawerFragment(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        //noinspection SimplifiableIfStatement
        presenter.onMenuItemSelect(id);

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        presenter.changeMenuFragment(null);
        presenter.changeMainWindowFragment(null, false);
    }

    public MainWindowIndividualActivityPresenter getPresenter() {
        return presenter;
    }

    public DrawerLayout getmDrawerLayout() {
        return mDrawerLayout;
    }

    public ActionBarDrawerToggle getmDrawerToggle() {
        return mDrawerToggle;
    }

    public void setmDrawerToggle(ActionBarDrawerToggle mDrawerToggle) {
        this.mDrawerToggle = mDrawerToggle;
    }

    public void setPresenter(MainWindowIndividualActivityPresenter presenter) {
        this.presenter = presenter;
    }

    public Bundle getBundle() {
        return getIntent().getExtras();
    }

}
