package br.com.leiamaisapp.leiamais.Tests;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Events.Common.CommonEvents;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisActivity;
import br.com.leiamaisapp.leiamais.Views.Individual.MainWindowIndividualActivity;

public class Spell extends LeiaMaisActivity implements Testable {

    Long idProfessional;
    private List lettersTextViews;
    private LinearLayout textArea;
    private Thread changeColorThread;
    private String word;
    private Button alphabetNextButton;
    private static int letterIndex;
    private ImageButton right;
    private ImageButton wrong;
    private int letterCount;
    private boolean linkTheWordDone;
    private boolean spellTheWordDone;

    public Spell() {
        letterIndex = 1;
        lettersTextViews = new ArrayList<TextView>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_spell);
        idProfessional = getIntent().getLongExtra(Constants.PROFESSIONAL_EMAIL_TAG, 0);
        textArea = (LinearLayout) findViewById(R.id.testLinearLayoutTextArea);
        this.right = (ImageButton) findViewById(R.id.testButtonFailed);
        this.wrong = (ImageButton) findViewById(R.id.testButtonSucceeded);
        this.alphabetNextButton = (Button) findViewById(R.id.alphabetNextButton);
        this.linkTheWordDone = false;
        this.spellTheWordDone = false;
        load();
    }


    @Override
    public int getLayout() {
        return 0;
    }


    private void load() {
        word = chooseWord();
        for (char letter : word.toCharArray()) {
            TextView letterView = createWordTextView(letter);
            lettersTextViews.add(letterView);
            textArea.addView(letterView);
        }
    }

    private TextView createWordTextView(Character letter) {
        TextView letterView = new TextView(getApplicationContext());
        letterView.setText(letter.toString());
        letterView.setTextSize(200);
        letterView.setTypeface(Typeface.create("sans-serif-condensed", Typeface.BOLD));
        letterView.setTextColor(Color.parseColor("#cccccc"));
        return letterView;
    }

    private TextView createWordTextView(String letter) {
        TextView letterView = new TextView(getApplicationContext());
        letterView.setText(letter);
        letterView.setTextSize(200);
        letterView.setTypeface(Typeface.create("sans-serif-condensed", Typeface.BOLD));
        letterView.setTextColor(Color.parseColor("#cccccc"));
        return letterView;
    }

    private String chooseWord() {
        String[] words = getWords();
        int randomWord = 0 + (int) (Math.random() * words.length);
        String word = words[randomWord];
        return word;
    }

    private String[] getWords() {
        Resources res = getResources();
        return res.getStringArray(R.array.spellingarraywords);
    }


    @Override
    public void start(View v) {
        this.letterCount = textArea.getChildCount();
        v.setVisibility(View.GONE);
        this.wrong.setVisibility(View.VISIBLE);
        this.right.setVisibility(View.VISIBLE);
        this.updateLetterTextViewColor(0, "#ff8800");
        //updateLetterColors(letterCount);
    }

    public void updateLetterColors(int letterCount) {
        final int finalLetterCount = letterCount;
        changeColorThread = new Thread() {
            @Override
            public void run() {
                try {
                    changeColorOneByOneLetter("#ff8800", finalLetterCount);
                    Thread.sleep(600);
                    changeAllLettersColors("#cccccc", finalLetterCount);
                    Thread.sleep(1500);
                    changeAllLettersColors("#ff8800", finalLetterCount);
                    Thread.sleep(1500);
                    activeDragbleTextview();
                } catch (InterruptedException e) {

                }
            }
        };
        changeColorThread.start();
    }

    public void updateLetterTextViewColor(int letterIndex, String color) {
        TextView letter = (TextView) textArea.getChildAt(letterIndex);
        letter.setTextColor(Color.parseColor(color));
    }

    public void changeAllLettersColors(final String color, final int finalLetterCount) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int letterIndex = 0;
                while (letterIndex < finalLetterCount) {
                    updateLetterTextViewColor(letterIndex, color);
                    letterIndex++;
                }
            }
        });
    }

    public void changeColorOneByOneLetter(final String color, final int finalLetterCount) throws InterruptedException {
        int letterIndex = 0;
        while (letterIndex < finalLetterCount) {
            Thread.sleep(1000);
            final int finalIndex = letterIndex;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateLetterTextViewColor(finalIndex, color);
                }
            });
            letterIndex++;
        }
    }

    @Override
    public void stop(View v) {
        if (changeColorThread != null &&
                changeColorThread.isAlive()) {
            changeColorThread.interrupt();
        }
    }

    @Override
    public void restart(View v) {
        stop(v);
        showImages(false);
        clearTextArea();
        load();
        start(v);
    }

    public void clearTextArea() {
        textArea.removeAllViews();
    }

    @Override
    public void exit(View v) {
    }

    @Override
    public boolean finished(View v) {
        return false;
    }

    @Override
    public int getTotalAnimationDuration() {
        return 0;
    }

    public void preview(View v) {
        startActivity(new Intent(this, Alphabet.class));
    }

    public void next(View v) {
        Intent intent = new Intent(this, CompleteWord.class);
        intent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, idProfessional);
        startActivity(intent);
    }

    @Override
    public void info(View v) {
        Toast.makeText(getApplicationContext(), getTestInfo(), Toast.LENGTH_LONG).show();
    }

    @Override
    public String getTestInfo() {
        return "Esse é o teste da soletração, nesse teste o individuo deve soletrar a palavra conforme a letras mudam de cor.";
    }


    private void activeDragbleTextview() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                clearTextArea();
                TextView completeWordTextView = createWordTextView(word);
                completeWordTextView.setTextSize(80);
                completeWordTextView.setTextColor(Color.parseColor("#000000"));
                completeWordTextView.setOnTouchListener(new ChoiceTouchListener());
                textArea.addView(completeWordTextView);

                completeWordTextView.setGravity(View.TEXT_ALIGNMENT_CENTER);
//                textArea.setLayoutParams(lp);
                showImages(true);
                setDropPlaces();
            }
        });
    }

    private void showImages(boolean show) {
        LinearLayout images = (LinearLayout) findViewById(R.id.testSpellImagesAndTextView);
        if (show) {
            images.setVisibility(View.VISIBLE);
            return;
        }
        images.setVisibility(View.GONE);
    }


    private void setDropPlaces() {
        TextView replace1 = (TextView) findViewById(R.id.testTextViewDropPlace1);
        replace1.setOnDragListener(new ChoiceDragListener());
        replace1.setBackgroundResource(R.drawable.border_black);
        replace1.setText("");
        TextView replace2 = (TextView) findViewById(R.id.testTextViewDropPlace2);
        replace2.setOnDragListener(new ChoiceDragListener());
        replace2.setBackgroundResource(R.drawable.border_black);
        replace2.setText("");
        TextView replace3 = (TextView) findViewById(R.id.testTextViewDropPlace3);
        replace3.setOnDragListener(new ChoiceDragListener());
        replace3.setBackgroundResource(R.drawable.border_black);
        replace1.setText("");
    }


    private final class ChoiceTouchListener implements View.OnTouchListener {
        @SuppressLint("NewApi")
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                //start dragging the item touched
                view.startDrag(data, shadowBuilder, view, 0);
                return true;
            } else {
                return false;
            }
        }
    }

    @SuppressLint("NewApi")
    private class ChoiceDragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    droppingTextView(event, v);
                    linkTheWordDone = true;
//                    startActivity(new Intent(getApplicationContext(),EndAllTest.class));
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    private void droppingTextView(DragEvent event, View v) {
        View view = (View) event.getLocalState();
        TextView dropTarget = (TextView) v;
        TextView dropped = (TextView) view;
        view.setVisibility(View.INVISIBLE);
        dropTarget.setPadding(30, 0, 30, 0);
        dropped.setPadding(0, 0, 0, 0);
        dropTarget.setText(dropped.getText().toString());
        Object tag = dropTarget.getTag();
        dropTarget.setTag(dropped.getId());
        dropTarget.setOnDragListener(null);
    }


    public void right(View v) {
        //TODO: Salvar acerto na sessão
        if (letterIndex >= this.letterCount) {
            //this.finishTest();
            //this.saveTest(letterIndex,true);
            if (linkTheWordDone == false) {
                activeDragbleTextview();
                return;
            }
            saveTest(letterIndex, false);
            finishTest();
            return;
        }
        this.updateLetterTextViewColor(letterIndex++, "#ff8800");
    }

    public void wrong(View v) {
        //TODO: Salvar erro na sessão
        if (letterIndex >= this.letterCount) {
            //this.saveTest(letterIndex,false);
            if (linkTheWordDone == false) {
                activeDragbleTextview();
                return;
            }
            saveTest(letterIndex, false);
            finishTest();
            return;
        }
        this.updateLetterTextViewColor(letterIndex++, "#ff8800");

    }

    public void finishTest() {
        this.wrong.setEnabled(false);
        this.right.setEnabled(false);
        textArea.removeAllViewsInLayout();
        Button nextButton = new Button(textArea.getContext());
        nextButton.setText(R.string.next);
        nextButton.setPadding(50, 50, 50, 50);
        nextButton.setGravity(Gravity.BOTTOM);
        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CompleteWord.class);
                intent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, idProfessional);
                startActivity(intent);
            }
        });
        textArea.addView(nextButton);
    }


    public void saveTest(int letterIndex, boolean hit) {

    }

    @Override
    public void onBackPressed() {
        CommonEvents events = new CommonEvents(this);

        events.showConfirmationMessage(
                "Sair"
                , "Deseja mesmo terminar o teste?"
                , "Sim"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getBaseContext(), MainWindowIndividualActivity.class);
                        intent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, idProfessional);
                        startActivity(intent);
                    }
                }
                , "Não"
                , null
        );
    }

}