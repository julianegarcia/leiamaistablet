package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualSingUpPersonalDevelopmentFragmentPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by erickson on 20/08/16.
 */
public class IndividualSingUpPersonalDevelopmentFragment extends IndividualFragment {

    public IndividualSingUpPersonalDevelopmentFragment() {
        setPresenter(new IndividualSingUpPersonalDevelopmentFragmentPresenter(this));
    }

    @BindView(R.id.frg_individual_signup_spinnerGrade)
    Spinner grade;
    @BindView(R.id.frg_individual_signup_switchSchoolSupport)
    Switch schoolSupport;
    @BindView(R.id.frg_individual_signup_switchTdhaTreatment)
    Switch tdhaTreatment;
    @BindView(R.id.frg_individual_signup_switchRegularSchool)
    Switch hasRegularSchool;
    @BindView(R.id.frg_individual_signup_editTextRegularSchool)
    EditText regularSchool;
    @BindView(R.id.frg_individual_signup_switchSpeakProblem)
    Switch speakProblem;
    @BindView(R.id.frg_individual_signup_buttonDBack)
    Button back;
    @BindView(R.id.frg_individual_signup_buttonDNext)
    Button next;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_individual_signup_development, container, false);
        ButterKnife.bind(this, view);

        getPresenter().startPresenter();
        return view;
    }

    public Spinner getGrade() {
        return grade;
    }

    public Switch getSchoolSupport() {
        return schoolSupport;
    }

    public Switch getTdhaTreatment() {
        return tdhaTreatment;
    }

    public Switch getHasRegularSchool() {
        return hasRegularSchool;
    }

    public EditText getRegularSchool() {
        return regularSchool;
    }

    public Switch getSpeakProblem() {
        return speakProblem;
    }

    public Button getBack() {
        return back;
    }

    public Button getNext() {
        return next;
    }
}