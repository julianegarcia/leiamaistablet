package br.com.leiamaisapp.leiamais;

import android.app.Application;

import br.com.leiamaisapp.restapi.rest.RestClient;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by remoto on 24/07/2016.
 */
public class LeiaMaisApplication extends Application {

    private static RestClient restClient;

    @Override
    public void onCreate() {
        super.onCreate();
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder(getApplicationContext())
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
        restClient = new RestClient("http://api_host.aqui");
    }

    public static RestClient getRestClient() {
        return restClient;
    }
}
