package br.com.leiamaisapp.leiamais.Presenters.Individual;

import android.view.View;

import br.com.leiamaisapp.database.Model.IndividualTDHA;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSingUpTDHAFragment;

/**
 * Created by erickson on 04/09/16.
 */
public class IndividualSingUpTDHAFragmentPresenter extends IndividualFragmentPresenter {

    public IndividualSingUpTDHAFragmentPresenter(IndividualSingUpTDHAFragment fragment) {
        this.fragment = fragment;
    }

    private IndividualSingUpTDHAFragment fragment;

    @Override
    public void startPresenter() {
        setIndividualTDHAForm();
        setBackButton();
        setNextButton();
    }

    private void setBackButton() {
        fragment.getBack().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveIndividual())
                    getMActivityPresenter().changeMainWindowFragment(Constants.INDIVIDUAL_SIGN_UP_PERSONAL_DEVELOPMENT_TAG, true);
            }
        });
    }

    private void setNextButton() {
        fragment.getNext().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveIndividual())
                    getMActivityPresenter().changeMainWindowFragment(Constants.INDIVIDUAL_SIGN_UP_EXCLUSION_TAG, true);
            }
        });
    }

    private void setIndividualTDHAForm() {
        IndividualTDHA individualTDHA = getIndividual().getIndividualTDHA();

        fragment.getQuestion1().setChecked(individualTDHA.getQuestion1());
        fragment.getQuestion2().setChecked(individualTDHA.getQuestion2());
        fragment.getQuestion3().setChecked(individualTDHA.getQuestion3());
        fragment.getQuestion4().setChecked(individualTDHA.getQuestion4());
        fragment.getQuestion5().setChecked(individualTDHA.getQuestion5());
        fragment.getQuestion6().setChecked(individualTDHA.getQuestion6());
        fragment.getQuestion7().setChecked(individualTDHA.getQuestion7());
        fragment.getQuestion8().setChecked(individualTDHA.getQuestion8());
        fragment.getQuestion9().setChecked(individualTDHA.getQuestion9());
        fragment.getQuestion10().setChecked(individualTDHA.getQuestion10());
        fragment.getQuestion11().setChecked(individualTDHA.getQuestion11());
        fragment.getQuestion12().setChecked(individualTDHA.getQuestion12());
        fragment.getQuestion13().setChecked(individualTDHA.getQuestion13());
        fragment.getQuestion14().setChecked(individualTDHA.getQuestion14());
        fragment.getQuestion15().setChecked(individualTDHA.getQuestion15());
        fragment.getQuestion16().setChecked(individualTDHA.getQuestion16());
        fragment.getQuestion17().setChecked(individualTDHA.getQuestion17());
        fragment.getQuestion18().setChecked(individualTDHA.getQuestion18());
    }

    @Override
    public boolean saveIndividual() {
        IndividualTDHA individualTDHA = getIndividual().getIndividualTDHA();

        individualTDHA.setQuestion1(fragment.getQuestion1().isChecked());
        individualTDHA.setQuestion2(fragment.getQuestion2().isChecked());
        individualTDHA.setQuestion3(fragment.getQuestion3().isChecked());
        individualTDHA.setQuestion4(fragment.getQuestion4().isChecked());
        individualTDHA.setQuestion5(fragment.getQuestion5().isChecked());
        individualTDHA.setQuestion6(fragment.getQuestion6().isChecked());
        individualTDHA.setQuestion7(fragment.getQuestion7().isChecked());
        individualTDHA.setQuestion8(fragment.getQuestion8().isChecked());
        individualTDHA.setQuestion9(fragment.getQuestion9().isChecked());
        individualTDHA.setQuestion10(fragment.getQuestion10().isChecked());
        individualTDHA.setQuestion11(fragment.getQuestion11().isChecked());
        individualTDHA.setQuestion12(fragment.getQuestion12().isChecked());
        individualTDHA.setQuestion13(fragment.getQuestion13().isChecked());
        individualTDHA.setQuestion14(fragment.getQuestion14().isChecked());
        individualTDHA.setQuestion15(fragment.getQuestion15().isChecked());
        individualTDHA.setQuestion16(fragment.getQuestion16().isChecked());
        individualTDHA.setQuestion17(fragment.getQuestion17().isChecked());
        individualTDHA.setQuestion18(fragment.getQuestion18().isChecked());

        return true;
    }
}
