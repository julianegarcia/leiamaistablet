package br.com.leiamaisapp.leiamais.Presenters.Individual;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import br.com.leiamaisapp.database.Model.Individual;
import br.com.leiamaisapp.leiamais.Common.CPFValidator;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Common.DateValidator;
import br.com.leiamaisapp.leiamais.Events.Common.CommonEvents;
import br.com.leiamaisapp.leiamais.Events.Common.ImageEvents;
import br.com.leiamaisapp.leiamais.Events.Individual.IndividualSingUpEvents;
import br.com.leiamaisapp.leiamais.Forms.DataPicker;
import br.com.leiamaisapp.leiamais.Forms.MaskEditTextTChangedListener;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSingUpPersonalDataFragment;

/**
 * Created by erickson on 27/08/16.
 */
public class IndividualSingUpPersonalDataFragmentPresenter extends IndividualFragmentPresenter {

    public IndividualSingUpPersonalDataFragmentPresenter
            (IndividualSingUpPersonalDataFragment fragment) {
        this.fragment = fragment;
    }

    private Context context;

    private IndividualSingUpPersonalDataFragment fragment;

    private ArrayAdapter<String> sexAdapter;
    private ArrayList<String> sexStrings;

    private ImageEvents fEvent;

    private String photoPath = "";

    @Override
    public void startPresenter() {
        context = fragment.getActivity().getApplicationContext();
        fEvent = new ImageEvents(fragment.getActivity());
        setIndividual(fragment.getMActivityPresenter().getIndividual());
        if (getIndividual() != null)
            photoPath = getIndividual().getPhoto();
        setHistoricalStartVisibility();
        setControllers();
        setIndividualForm();
    }

    private void setControllers() {
//        Set the Take Photo button
        setButtonTakePhoto();
//        Set the birthday date values according with the values in string.xml
        setBirthDayDateFormat();
//        Set the sex spinner
        setSexSpinner();
//        Set the EditText Masks
        setMasks();
//        Set the switches listeners
        setHistoricalSwitches();
//        Set the next button
        setButtonNext();
    }

    private void setButtonNext() {
        fragment.getNext().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveIndividual()) {
                    getMActivityPresenter().changeMainWindowFragment(Constants.INDIVIDUAL_SIGN_UP_PERSONAL_DEVELOPMENT_TAG, true);
                }
            }
        });
    }

    private void setHistoricalStartVisibility() {
//        Set the EditTexts from historical to invisible, so they can be showed based on their switches
        fragment.getFamiliarHistoric().setVisibility(View.INVISIBLE);
        fragment.getConsanguinityHistoric().setVisibility(View.INVISIBLE);
        fragment.getDesiredGestation().setVisibility(View.INVISIBLE);
        fragment.getPregnancyComplications().setVisibility(View.INVISIBLE);
        fragment.getMaternalHistory().setVisibility(View.INVISIBLE);
        fragment.getNeonatalComplications().setVisibility(View.INVISIBLE);
        fragment.getComorbidities().setVisibility(View.INVISIBLE);
    }

    private void setHistoricalSwitches() {
        fragment.getHasFamiliarHistoric().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                IndividualSingUpEvents event = new IndividualSingUpEvents();
                event.changeEditTextVisibility(compoundButton, fragment.getFamiliarHistoric());
            }
        });
        fragment.getHasConsanguinityHistoric().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                IndividualSingUpEvents event = new IndividualSingUpEvents();
                event.changeEditTextVisibility(compoundButton, fragment.getConsanguinityHistoric());
            }
        });
        fragment.getHasDesiredGestation().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                IndividualSingUpEvents event = new IndividualSingUpEvents();
                event.changeEditTextVisibility(compoundButton, fragment.getDesiredGestation());
            }
        });
        fragment.getHasPregnancyComplications().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                IndividualSingUpEvents event = new IndividualSingUpEvents();
                event.changeEditTextVisibility(compoundButton, fragment.getPregnancyComplications());
            }
        });
        fragment.getHasMaternalHistory().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                IndividualSingUpEvents event = new IndividualSingUpEvents();
                event.changeEditTextVisibility(compoundButton, fragment.getMaternalHistory());
            }
        });
        fragment.getHasNeonatalComplications().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                IndividualSingUpEvents event = new IndividualSingUpEvents();
                event.changeEditTextVisibility(compoundButton, fragment.getNeonatalComplications());
            }
        });
        fragment.getHasComorbidities().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                IndividualSingUpEvents event = new IndividualSingUpEvents();
                event.changeEditTextVisibility(compoundButton, fragment.getComorbidities());
            }
        });
    }

    private void setButtonTakePhoto() {
        try {
            File file = new File(photoPath);
            if (file.exists()) {
                fEvent.setImageViewPhoto(fragment.getPlaceHolderImage(), file);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragment.getTakePhoto().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoPath = fEvent.takePhoto();
            }
        });
    }

    private void setIndividualForm() {
        Individual individual = getIndividual();

        if (individual.getName() != "")
            fragment.getName().setText(individual.getName());

        if (individual.getBirthday() != null) {
            String birthday = new SimpleDateFormat(
                    context.getString(R.string.dt_picker_date_format)).format(
                    individual.getBirthday()
            );

            if (new DateValidator().isThisDateValid(
                    birthday, context.getString(R.string.dt_picker_date_format)))
                fragment.getBirthday().setText(birthday);
        }

        int ind = loadSexStrings().indexOf(individual.getSex());

        fragment.getSex().setSelection(ind);

        if (individual.getCpf() != "")
            fragment.getCpf().setText(individual.getCpf());

        if (individual.getMotherName() != "")
            fragment.getResponsibleName().setText(individual.getMotherName());

        if (individual.getFatherName() != "")
            fragment.getFatherName().setText(individual.getFatherName());

        if (individual.getTelephone() != "")
            fragment.getTelephone().setText(individual.getTelephone());

        if (individual.getAddress() != "")
            fragment.getAddress().setText(individual.getAddress());

        if (individual.getEmail() != "")
            fragment.getResponsibleEmail().setText(individual.getEmail());

        if (individual.getFamilyHistoric() != null) {
            fragment.getHasFamiliarHistoric().setChecked(true);
            fragment.getFamiliarHistoric().setText(individual.getFamilyHistoric());
        }

        if (individual.getConsanguinityHistoric() != null) {
            fragment.getHasConsanguinityHistoric().setChecked(true);
            fragment.getConsanguinityHistoric().setText(individual.getConsanguinityHistoric());
        }

        if (individual.getDesiredGestation() != null) {
            fragment.getHasDesiredGestation().setChecked(true);
            fragment.getDesiredGestation().setText(individual.getDesiredGestation());
        }

        if (individual.getPregnancyComplications() != null) {
            fragment.getHasPregnancyComplications().setChecked(true);
            fragment.getPregnancyComplications().setText(individual.getPregnancyComplications());
        }

        if (individual.getMaternalHistoric() != null) {
            fragment.getHasMaternalHistory().setChecked(true);
            fragment.getMaternalHistory().setText(individual.getMaternalHistoric());
        }

        if (individual.getNeonatalComplications() != null) {
            fragment.getHasNeonatalComplications().setChecked(true);
            fragment.getNeonatalComplications().setText(individual.getNeonatalComplications());
        }

        if (individual.getComorbidities() != null) {
            fragment.getHasComorbidities().setChecked(true);
            fragment.getComorbidities().setText(individual.getComorbidities());
        }
    }

    private void setBirthDayDateFormat() {
        DataPicker dataPickerBirthday = fragment.getBirthday();
        dataPickerBirthday.setMDateFormat(fragment.getString(R.string.dt_picker_date_format));
        dataPickerBirthday.setTitle(fragment.getString(R.string.dt_picker_select_a_date));
        dataPickerBirthday.setButtonPositive(fragment.getString(R.string.dt_picker_button_positive));
        dataPickerBirthday.setButtonNegative(fragment.getString(R.string.dt_picker_button_negative));
    }

    private void setMasks() {
        MaskEditTextTChangedListener maskBirthday = new MaskEditTextTChangedListener(
                context.getString(R.string.date_mask), fragment.getBirthday());
        MaskEditTextTChangedListener maskCPF = new MaskEditTextTChangedListener(
                context.getString(R.string.cpf_mask), fragment.getCpf());
        MaskEditTextTChangedListener maskTel = new MaskEditTextTChangedListener(
                "(##) ##########", fragment.getTelephone());
        fragment.getBirthday().addTextChangedListener(maskBirthday);
        fragment.getCpf().addTextChangedListener(maskCPF);
        fragment.getTelephone().addTextChangedListener(maskTel);
    }

    private void setSexSpinner() {
        sexStrings = loadSexStrings();
        sexAdapter = new ArrayAdapter<>(
                context,
                R.layout.spiner_cell,
                sexStrings
        );

        sexAdapter.setDropDownViewResource(R.layout.spinner_cell_drop);

        fragment.getSex().setAdapter(sexAdapter);
        fragment.getSex().setSelection(0);
    }

    private ArrayList<String> loadSexStrings() {
//        TODO: Bring it from the database
        ArrayList<String> strings = new ArrayList<>();

        strings.add(fragment.getString(R.string.form_common_sex));
        strings.add(fragment.getString(R.string.form_common_sex_f));
        strings.add(fragment.getString(R.string.form_common_sex_m));

        return strings;
    }

    @Override
    public boolean saveIndividual() {
        return validateIndividualPersonalData();
    }

    private boolean validateIndividualPersonalData() {
        CommonEvents event = new CommonEvents(fragment.getActivity());
        Individual individual = getIndividual();

        String name = fragment.getName().getText().toString();
        String birthday = fragment.getBirthday().getText().toString();
        String sex = loadSexStrings().get(fragment.getSex().getSelectedItemPosition());
        String cpf = fragment.getCpf().getText().toString();
        String responsibleName = fragment.getResponsibleName().getText().toString();
        String fatherName = fragment.getFatherName().getText().toString();
        Boolean hasFamiliarHistoric = fragment.getHasFamiliarHistoric().isChecked();
        String familiarHistoric = fragment.getFamiliarHistoric().getText().toString();
        Boolean hasConsanguinityHistoric = fragment.getHasConsanguinityHistoric().isChecked();
        String consanguinityHistoric = fragment.getConsanguinityHistoric().getText().toString();
        Boolean hasDesiredGestation = fragment.getHasDesiredGestation().isChecked();
        String desiredGestation = fragment.getDesiredGestation().getText().toString();
        Boolean hasPregnancyComplications = fragment.getHasPregnancyComplications().isChecked();
        String pregnancyComplications = fragment.getPregnancyComplications().getText().toString();
        Boolean hasMaternalHistory = fragment.getHasMaternalHistory().isChecked();
        String maternalHistory = fragment.getMaternalHistory().getText().toString();
        Boolean hasNeonatalComplications = fragment.getHasNeonatalComplications().isChecked();
        String neonatalComplications = fragment.getNeonatalComplications().getText().toString();
        Boolean hasComorbidities = fragment.getHasComorbidities().isChecked();
        String comorbidities = fragment.getComorbidities().getText().toString();
        String telephone = fragment.getTelephone().getText().toString();
        String address = fragment.getAddress().getText().toString();
        String responsibleEmail = fragment.getResponsibleEmail().getText().toString();

        Date mBirthday;

        if (name.isEmpty()) {
            event.showMessage(context.getString(R.string.error_empty_name));
        } else if (responsibleName.isEmpty()) {
            event.showMessage(context.getString(R.string.error_empty_responsible_name));
        } else if (cpf.isEmpty()) {
            event.showMessage(context.getString(R.string.error_empty_cpf));
        } else if (!new DateValidator().isThisDateValid(
                birthday, context.getString(R.string.dt_picker_date_format))) {
            event.showMessage(context.getString(R.string.error_invalid_date));
        } else if (!new CPFValidator().isCPF(cpf)) {
            event.showMessage(context.getString(R.string.error_invalid_cpf));
        } else if (sex.equals(fragment.getString(R.string.form_common_sex))) {
            event.showMessage(context.getString(R.string.error_select_sex));
        } else {
            try {
                mBirthday = new SimpleDateFormat(context.getString(R.string.dt_picker_date_format)).parse(birthday);
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            }

            individual.setPhoto(photoPath);
            individual.setName(name);
            individual.setBirthday(mBirthday);
            individual.setSex(sex);
            individual.setCpf(cpf);
            individual.setMotherName(responsibleName);
            individual.setFatherName(fatherName);
            individual.setFamilyHistoric(hasFamiliarHistoric ? familiarHistoric : null);
            individual.setConsanguinityHistoric(hasConsanguinityHistoric ? consanguinityHistoric : null);
            individual.setDesiredGestation(hasDesiredGestation ? desiredGestation : null);
            individual.setPregnancyComplications(hasPregnancyComplications ? pregnancyComplications : null);
            individual.setMaternalHistoric(hasMaternalHistory ? maternalHistory : null);
            individual.setNeonatalComplications(hasNeonatalComplications ? neonatalComplications : null);
            individual.setComorbidities(hasComorbidities ? comorbidities : null);
            individual.setTelephone(telephone);
            individual.setAddress(address);
            individual.setEmail(responsibleEmail);

            return true;
        }

        return false;
    }

    private void setPlaceHolderImagem() {
        final File file = new File(photoPath);
        if (file.exists()) {
            fEvent.setImageViewPhoto(
                    fragment.getPlaceHolderImage()
                    , file
            );
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.CAMERA_REQUEST:
                setPlaceHolderImagem();
                break;
        }
    }
}
