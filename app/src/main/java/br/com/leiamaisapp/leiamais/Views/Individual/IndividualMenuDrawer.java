package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualMenuDrawerPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by erickson on 23/07/16.
 */
public class IndividualMenuDrawer extends IndividualFragment {
    public IndividualMenuDrawer() {
        setPresenter(new IndividualMenuDrawerPresenter(this));
    }

    @BindView(R.id.drw_professional_photo)
    CircleImageView professionalPhoto;
    @BindView(R.id.drw_individual_menu_professional_presentation)
    TextView professionalPresentation;
    @BindView(R.id.drw_add_new_individual)
    Button newIndividual;
    @BindView(R.id.drw_search_individual)
    Button searchIndividual;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drawer_individual_menu, container, false);
        ButterKnife.bind(this, view);
        getPresenter().startPresenter();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((IndividualMenuDrawerPresenter) getPresenter()).setProfessionalPresentation();
    }

    public ImageView getProfessionalPhoto() {
        return professionalPhoto;
    }

    public TextView getProfessionalPresentation() {
        return professionalPresentation;
    }

    public Button getNewIndividual() {
        return newIndividual;
    }

    public Button getSearchIndividual() {
        return searchIndividual;
    }
}
