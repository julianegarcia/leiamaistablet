package br.com.leiamaisapp.leiamais.Presenters.Individual;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import br.com.leiamaisapp.database.Dao.IndividualDAO;
import br.com.leiamaisapp.database.Dao.ProfessionalDAO;
import br.com.leiamaisapp.database.Model.Individual;
import br.com.leiamaisapp.database.Model.IndividualExclusion;
import br.com.leiamaisapp.database.Model.IndividualTDHA;
import br.com.leiamaisapp.database.Model.Professional;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Events.Common.CommonEvents;
import br.com.leiamaisapp.leiamais.LeiaMaisApplication;
import br.com.leiamaisapp.leiamais.Presenters.Common.FragmentPresenter;
import br.com.leiamaisapp.leiamais.Presenters.Common.LeiaMaisPresenter;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Tests.Alphabet;
import br.com.leiamaisapp.leiamais.Views.Individual.DefaultIndividualMenuFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualDevelopmentFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualExclusionFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualMenuDrawer;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualMenuFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualPersonalDataFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualReportFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSearchDrawer;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSignUpMenuFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSingUpExclusionFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSingUpPersonalDataFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSingUpPersonalDevelopmentFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSingUpTDHAFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualWelcomeFragment;
import br.com.leiamaisapp.leiamais.Views.Individual.MainWindowIndividualActivity;
import br.com.leiamaisapp.leiamais.Views.Professional.MainWindowProfessionalActivity;
import br.com.leiamaisapp.restapi.rest.RestModel.IndividualAPI;
import br.com.leiamaisapp.restapi.rest.service.RestService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by erickson on 27/08/16.
 */
public class MainWindowIndividualActivityPresenter extends LeiaMaisPresenter {
    public MainWindowIndividualActivityPresenter(MainWindowIndividualActivity activity) {
        super();
        this.activity = activity;
    }

    @Override
    public void startPresenter() {
        Bundle bundle = activity.getBundle();

        ProfessionalDAO professionalDAO = new ProfessionalDAO();

        professional = professionalDAO.findById(bundle.getLong(Constants.PROFESSIONAL_EMAIL_TAG));

        setProfessional(professional);

        String id = bundle.getString(Constants.INDIVIDUAL_CPF_TAG);

        setIndividual(id);

        fragmentPresenter = new FragmentPresenter();

        changeDrawerFragment(null);
        changeMenuFragment(null);
        changeMainWindowFragment(null, false);

        setMainWindowDrawer();
    }

    private Individual individual;

    private Professional professional;

    private MainWindowIndividualActivity activity;

    private IndividualFragment menuIndividualFragment;
    private IndividualFragment mainIndividualFragment;
    private IndividualFragment individualDrawer;
    private FragmentPresenter fragmentPresenter;

    private ActionBarDrawerToggle actionBarDrawerToggle;

    public Individual getIndividual() {
        return individual;
    }

    public Professional getProfessional() {
        return professional;
    }

    public ActionBarDrawerToggle getActionBarDrawerToggle() {
        return actionBarDrawerToggle;
    }

    public void setIndividual(Individual individual) {
        this.individual = individual;
        if (individual.getIndividualTDHA() == null)
            individual.setIndividualTDHA(new IndividualTDHA());
        if (individual.getIndividualExclusion() == null)
            individual.setIndividualExclusion(new IndividualExclusion());
    }

    public void setIndividual(@Nullable String id) {
        IndividualDAO dao = new IndividualDAO();
        if (id != null)
            this.individual = dao.findById(Long.parseLong(id));
        if (individual == null)  // If there's no Individual passed within the bundle, then we must create a new one
            individual = new Individual();
        if (individual.getIndividualTDHA() == null)
            individual.setIndividualTDHA(new IndividualTDHA());
        if (individual.getIndividualExclusion() == null)
            individual.setIndividualExclusion(new IndividualExclusion());

    }

    public void setProfessional(Professional professional) {
        this.professional = professional;
    }

    public IndividualFragment getMenuIndividualFragment() {
        return menuIndividualFragment;
    }

    public IndividualFragment getMainIndividualFragment() {
        return mainIndividualFragment;
    }

    public IndividualFragment getIndividualDrawer() {
        return individualDrawer;
    }

    public void setMainWindowDrawer() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                activity,
                activity.getmDrawerLayout(),
                R.string.drawer_open,
                R.string.drawer_close
        ) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                activity.invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                activity.invalidateOptionsMenu();
            }
        };

        activity.setmDrawerToggle(actionBarDrawerToggle);

        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);

        activity.getmDrawerLayout().addDrawerListener(activity.getmDrawerToggle());
        activity.getmDrawerToggle().syncState();
    }

    public void onMenuItemSelect(int opc) {
        switch (opc) {
            case R.id.action_edit_indvidual:
                if (getIndividual().getId() != null) {
                    changeMenuFragment(Constants.INDIVIDUAL_SIGN_UP_MENU_TAG);
                    changeMainWindowFragment(Constants.INDIVIDUAL_SIGN_UP_PERSONAL_DATA_TAG, false);
                } else {
                    Toast.makeText(
                            activity
                            , "Nenhum Indivíduo Selecionado"
                            , Toast.LENGTH_SHORT
                    ).show();
                }
                break;
            case R.id.action_edit_professional:
                Intent mIntent = new Intent(activity, MainWindowProfessionalActivity.class);
                mIntent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, professional.getId());
                activity.startActivity(mIntent);
                break;
        }
    }

    public void changeDrawerFragment(String tag) {
        try {
            switch (tag) {
                case Constants.INDIVIDUAL_SEARCH_MENU_TAG:
                    individualDrawer = new IndividualSearchDrawer();
                    break;
                default:
                    individualDrawer = new IndividualMenuDrawer();
            }
        } catch (NullPointerException e) {
            individualDrawer = new IndividualMenuDrawer();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Set the main presenter
        individualDrawer.setMActivityPresenter(this);

//        Change the fragment
        fragmentPresenter.changeFragments(activity,
                Constants.DRAWER_FRAGMENT,
                R.id.act_main_window_fragmentDrawerMenu,
                individualDrawer);
    }

    public void changeMenuFragment(String tag) {
        try {
            switch (tag) {
                case Constants.INDIVIDUAL_SIGN_UP_MENU_TAG:
                    menuIndividualFragment = new IndividualSignUpMenuFragment();
                    break;
                case Constants.INDIVIDUAL_MENU_TAG:
                    menuIndividualFragment = new IndividualMenuFragment();
                    break;
                default:
                    menuIndividualFragment = new DefaultIndividualMenuFragment();
            }
        } catch (NullPointerException e) {
            menuIndividualFragment = new DefaultIndividualMenuFragment();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Set the main presenter
        menuIndividualFragment.setMActivityPresenter(this);

//        Change the fragment
        fragmentPresenter.changeFragments(activity,
                Constants.MENU_FRAGMENT,
                R.id.act_main_window_fragmentPrincipalMenu,
                menuIndividualFragment);
    }

    public void changeMainWindowFragment(@Nullable String tag, boolean saveIndividual) {
//        Save the Individual changes on the actual fragment
        if (!saveIndividual || mainIndividualFragment.getPresenter().saveIndividual()) {

//        Choose witch window will be opened according with the tag passed on the call
            try {
                switch (tag) {
                    case Constants.INDIVIDUAL_APPLY_TEST_TAG:
                        Intent mIntent = new Intent(activity, Alphabet.class);
                        mIntent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, getProfessional().getId());
                        activity.startActivity(mIntent);
                        break;
                    case Constants.INDIVIDUAL_PERSONAL_DATA_TAG:
                        mainIndividualFragment = new IndividualPersonalDataFragment();
                        break;
                    case Constants.INDIVIDUAL_DEVELOPMENT_TAG:
                        mainIndividualFragment = new IndividualDevelopmentFragment();
                        break;
                    case Constants.INDIVIDUAL_EXCLUSION_CRITERIA_TAG:
                        mainIndividualFragment = new IndividualExclusionFragment();
                        break;
                    case Constants.INDIVIDUAL_REPORT_TAG:
                        mainIndividualFragment = new IndividualReportFragment();
                        break;
                    case Constants.INDIVIDUAL_SIGN_UP_PERSONAL_DATA_TAG:
                        mainIndividualFragment = new IndividualSingUpPersonalDataFragment();
                        break;
                    case Constants.INDIVIDUAL_SIGN_UP_EXCLUSION_TAG:
                        mainIndividualFragment = new IndividualSingUpExclusionFragment();
                        break;
                    case Constants.INDIVIDUAL_SIGN_UP_PERSONAL_DEVELOPMENT_TAG:
                        mainIndividualFragment = new IndividualSingUpPersonalDevelopmentFragment();
                        break;
                    case Constants.INDIVIDUAL_SIGN_UP_TDHA_TAG:
                        mainIndividualFragment = new IndividualSingUpTDHAFragment();
                        break;
                    default:
                        mainIndividualFragment = new IndividualWelcomeFragment();
                }
            } catch (NullPointerException e) {
                mainIndividualFragment = new IndividualWelcomeFragment();
            } catch (Exception e) {
                e.printStackTrace();
            }

//        Set the main presenter
            mainIndividualFragment.setMActivityPresenter(this);

//        Change the fragment
            fragmentPresenter.changeFragments(activity,
                    Constants.MAIN_FRAGMENT,
                    R.id.act_main_window_fragmentContent,
                    mainIndividualFragment);
        }
    }


    public boolean saveIndividual() {
        CommonEvents event = new CommonEvents(activity);

        event.showConfirmationMessage(
                activity.getString(R.string.frg_individual_signup_form_save_individual_title),
                activity.getString(R.string.frg_individual_signup_form_save_individual_message),
                activity.getString(R.string.form_common_yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        IndividualDAO dao = new IndividualDAO();
                        getIndividual().setCreatedBy(getProfessional().getIdBase());
                        dao.save(getIndividual());
                        IndividualAPI individualAPI = new IndividualAPI();

                        individualAPI.convertFromIndividual(getIndividual());

                        RestService service = LeiaMaisApplication.getRestClient().getService();

                        Call<String> call = service.updateIndividual(individualAPI);

                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                Log.d("REST", "OK");
                                Log.d("REST", response.body());
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                Log.d("REST", "Failure");
                            }
                        });
                        changeMenuFragment(Constants.INDIVIDUAL_MENU_TAG);
                        changeMainWindowFragment(Constants.INDIVIDUAL_PERSONAL_DATA_TAG, false);
                    }
                },
                activity.getString(R.string.form_common_no),
                null);
        return false;
    }

    public void closeDrawer() {
        activity.getmDrawerLayout().closeDrawer(GravityCompat.START);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mainIndividualFragment.getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    public void reloadProfessional() {
        professional = new ProfessionalDAO().findById(professional.getId());
    }

    public void hideTDHAButton(boolean st) {
        ((IndividualSignUpMenuPresenter) menuIndividualFragment.getPresenter()).hideTDHAButton(st);
    }
}
