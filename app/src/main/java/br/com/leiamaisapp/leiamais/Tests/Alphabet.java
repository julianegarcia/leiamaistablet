package br.com.leiamaisapp.leiamais.Tests;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Events.Common.CommonEvents;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisActivity;
import br.com.leiamaisapp.leiamais.Views.Individual.MainWindowIndividualActivity;

public class Alphabet extends LeiaMaisActivity {

    Long idProfessional;
    private TextView alphabetLetter;
    private ImageButton right;
    private ImageButton wrong;
    private ImageButton testButtonStart;
    private Button alphabetNextButton;
    private final int LETTER_SIZE = 250;
    public static Character letter;

    public Alphabet() {
        letter = 'A';
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        idProfessional = getIntent().getLongExtra(Constants.PROFESSIONAL_EMAIL_TAG, 0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_alphabet);
        setTextViewLetter();
        this.right = (ImageButton) findViewById(R.id.testButtonFailed);
        this.wrong = (ImageButton) findViewById(R.id.testButtonSucceeded);
        this.testButtonStart = (ImageButton) findViewById(R.id.testButtonStart);
        this.alphabetNextButton = (Button) findViewById(R.id.alphabetNextButton);

        testButtonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testButtonStart.setVisibility(View.INVISIBLE);
                right.setVisibility(View.VISIBLE);
                wrong.setVisibility(View.VISIBLE);
            }
        });
    }

    public void setTextViewLetter() {
        alphabetLetter = (TextView) findViewById(R.id.testTextViewAlphabetLetter);
        alphabetLetter.setTextSize(LETTER_SIZE);
        alphabetLetter.setTypeface(Typeface.create("sans-serif-condensed", Typeface.BOLD));
        alphabetLetter.setTextColor(Color.parseColor("#000000"));
    }


    public void info(View v) {
        Toast.makeText(getApplicationContext(), getTestInfo(), Toast.LENGTH_LONG).show();
    }

    public void right(View v) {
        try {
            //TODO: Salvar acerto na sessão
            if (changeLetter(letter++)) {
                this.saveLetterHitOnAlphabet(letter, true);
            }
        } catch (InterruptedException e) {
            letter = 'A';
        }
    }

    public void wrong(View v) {
        try {
            //TODO: Salvar erro na sessão
            if (changeLetter(letter++)) {
                this.saveLetterHitOnAlphabet(letter, false);
            }
        } catch (InterruptedException e) {
            letter = 'A';
        }
    }

    public void next(View v) {
        Intent intent = new Intent(this, Spell.class);
        intent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, idProfessional);
        startActivity(intent);
    }

    public boolean changeLetter(Character alphabet) throws InterruptedException {
        String l = alphabet.toString();
        Log.i("alphabet", l);

        if ("[".equals(l)) {
            alphabetLetter.setText(l);
            this.finishTest();
            return false;
        }

        alphabetLetter.setText(l);

        return true;

    }

    public String getTestInfo() {
        return "Esse é o teste do alfabeto, nesse teste o individuo deve falar a letra de acordo com o que aparece na tela.";
    }

    public void saveLetterHitOnAlphabet(Character letter, boolean hit) {

    }

    public void finishTest() {
        this.wrong.setEnabled(false);
        this.right.setEnabled(false);
        this.testButtonStart.setEnabled(false);
        this.alphabetLetter.setVisibility(View.GONE);
        this.alphabetNextButton.setVisibility(View.VISIBLE);
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onBackPressed() {
        CommonEvents events = new CommonEvents(this);

        events.showConfirmationMessage(
                "Sair"
                , "Deseja mesmo terminar o teste?"
                , "Sim"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getBaseContext(), MainWindowIndividualActivity.class);
                        intent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, idProfessional);
                        startActivity(intent);
                    }
                }
                , "Não"
                , null
        );
    }
}
