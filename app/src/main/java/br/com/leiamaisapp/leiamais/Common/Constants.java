package br.com.leiamaisapp.leiamais.Common;

/**
 * Created by erickson on 09/07/16.
 */
public class Constants {

    /* Int Patterns
    * RESOURCES = 10XX
    */

    public static final String IMAGE_DATE_FORMAT = "yyyy-MM-dd_HHmmss";
    public static final String IMAGE_NAME = "leia_mais_";
    public static final int CAMERA_REQUEST = 1010;

//    Models
    public static final String INDIVIDUAL_CPF_TAG = "individualCPFTag";
    public static final String PROFESSIONAL_EMAIL_TAG = "professionalEmailTag";
    public static final String ALPHABET_TEST_TAG = "alphabet";
    public static final String SPELLING_TEST_TAG = "spell-the-word";
    public static final String COMPLETE_WORD_TEST_TAG = "complete-word";
    public static final String WORD_TO_IMAGE_TEST_TAG = "link-word-to-image";

//    Fragments
//    Common Fragments
    public static final String MENU_FRAGMENT = "menuFragment";
    public static final String MAIN_FRAGMENT = "mainFragment";
    public static final String DRAWER_FRAGMENT = "drawerFragment";
    public static final String MAIN_WINDOW_ON_ACTIVITY_RESULT = "mainWindowOnActivityResult";
    public static final String INTENT_REQUESTCODE = "requestCode";
    public static final String INTENT_RESULTCODE = "resultCode";

//    Professional Fragments
//    Sign Up
    public static final String PROFESSIONAL_SIGN_UP_TAG = "professionalSignUp";

//    Individual Fragments TAGs
    public static final String INDIVIDUAL_APPLY_TEST_TAG = "IndividualApplyTestTag";
    public static final String INDIVIDUAL_SIGN_UP_MENU_TAG = "IndividualSignUpMenuTag";
    public static final String INDIVIDUAL_SIGN_UP_EXCLUSION_TAG = "IndividualSingUpExclusionTag";
    public static final String INDIVIDUAL_SIGN_UP_PERSONAL_DATA_TAG = "IndividualSingUpPersonalDataTag";
    public static final String INDIVIDUAL_SIGN_UP_PERSONAL_DEVELOPMENT_TAG = "IndividualSingUpPersonalDevelopmentTag";
    public static final String INDIVIDUAL_SIGN_UP_TDHA_TAG = "IndividualSingUpTDHATag";
    public static final String INDIVIDUAL_MENU_TAG = "IndividualMenuTag";
    public static final String INDIVIDUAL_SEARCH_MENU_TAG = "IndividualSearchMenuTag";
    public static final String INDIVIDUAL_PERSONAL_DATA_TAG = "IndividualPersonalDataTag";
    public static final String INDIVIDUAL_DEVELOPMENT_TAG = "IndividualDevelopmentTag";
    public static final String INDIVIDUAL_EXCLUSION_CRITERIA_TAG = "IndividualExclusionCriteriaTag";
    public static final String INDIVIDUAL_REPORT_TAG = "IndividualReportTag";

//    Debug
    public static final String DEBUG = "Debugging";
}
