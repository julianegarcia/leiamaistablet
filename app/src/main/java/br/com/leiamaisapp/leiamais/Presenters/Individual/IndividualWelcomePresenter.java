package br.com.leiamaisapp.leiamais.Presenters.Individual;

import android.util.Log;

import br.com.leiamaisapp.database.Model.Professional;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualWelcomeFragment;

/**
 * Created by erickson on 18/09/16.
 */
public class IndividualWelcomePresenter extends IndividualFragmentPresenter {

    public IndividualWelcomePresenter(IndividualWelcomeFragment fragment) {
        this.fragment = fragment;
    }

    private IndividualWelcomeFragment fragment;

    @Override
    public void startPresenter() {
        setWelcomeMessage();
    }

    private void setWelcomeMessage() {
        Log.e(Constants.DEBUG + "setWel", getMActivityPresenter().getProfessional().getName());
        Professional professional = getMActivityPresenter().getProfessional();
        fragment.getHello().setText(
                String.format(
                        fragment.getHello().getText().toString()
                        , professional.getTitle()
                        , professional.getName()
                        , (professional.getSex().equals(
                                fragment.getString(R.string.form_common_sex_f)
                        ) ? "a" : "o")
                )
        );
    }

}
