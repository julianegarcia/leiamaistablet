package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualPersonalDataPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by erickson on 21/10/16.
 */

public class IndividualPersonalDataFragment extends IndividualFragment {

    public IndividualPersonalDataFragment() {
        setPresenter(new IndividualPersonalDataPresenter(this));
    }

    @BindView(R.id.frg_individual_personal_data_age)
    TextView age;
    @BindView(R.id.frg_individual_personal_data_gender)
    TextView gender;
    @BindView(R.id.frg_individual_personal_data_cpf)
    TextView cpf;
    @BindView(R.id.frg_individual_personal_data_familiar_historic)
    TextView familiarHistoric;
    @BindView(R.id.frg_individual_personal_data_consanguinity_historic)
    TextView consanguinityHistoric;
    @BindView(R.id.frg_individual_personal_data_desired_gestation)
    TextView desiredGestation;
    @BindView(R.id.frg_individual_personal_data_neonatal_complications)
    TextView neonatalComplications;
    @BindView(R.id.frg_individual_personal_data_maternal_history)
    TextView maternalHistory;
    @BindView(R.id.frg_individual_personal_data_comorbidities)
    TextView comorbidities;
    @BindView(R.id.frg_individual_personal_data_pregnancy_complications)
    TextView pregnancyComplications;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_individual_personal_data, container, false);
        ButterKnife.bind(this, view);
        Log.d("TextoIdade", age.getText().toString());
        getPresenter().startPresenter();
        return view;
    }

    public TextView getAge() {
        return age;
    }

    public TextView getGender() {
        return gender;
    }

    public TextView getCpf() {
        return cpf;
    }

    public TextView getFamiliarHistoric() {
        return familiarHistoric;
    }

    public TextView getConsanguinityHistoric() {
        return consanguinityHistoric;
    }

    public TextView getDesiredGestation() {
        return desiredGestation;
    }

    public TextView getNeonatalComplications() {
        return neonatalComplications;
    }

    public TextView getMaternalHistory() {
        return maternalHistory;
    }

    public TextView getComorbidities() {
        return comorbidities;
    }

    public TextView getPregnancyComplications() {
        return pregnancyComplications;
    }
}
