package br.com.leiamaisapp.leiamais.Forms;

import android.graphics.Typeface;
import android.widget.EditText;

import br.com.jansenfelipe.androidmask.MaskEditTextChangedListener;

/**
 * Created by erickson on 14/09/16.
 */
public class MaskEditTextTChangedListener extends MaskEditTextChangedListener {

    private Typeface typeface;
    private EditText mEditText;

    public MaskEditTextTChangedListener(String mask, EditText editText) {
        super(mask, editText);
        mEditText = editText;
        typeface = editText.getTypeface();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        super.onTextChanged(s,start, before, count);
        mEditText.setTypeface(typeface);
    }
}
