package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualSingUpExclusionFragmentPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by erickson on 20/08/16.
 */
public class IndividualSingUpExclusionFragment extends IndividualFragment {

    public IndividualSingUpExclusionFragment() {
        setPresenter(new IndividualSingUpExclusionFragmentPresenter(this));
    }

    @BindView(R.id.frg_individual_signup_switchMentalDisability)
    Switch mentalDisability;
    @BindView(R.id.frg_individual_signup_switchNeurologicalDisorder)
    Switch neurologicalDisorder;
    @BindView(R.id.frg_individual_signup_switchHearingDeficiency)
    Switch hearingDeficiency;
    @BindView(R.id.frg_individual_signup_switchVisualImpairment)
    Switch visualImpairment;
    @BindView(R.id.frg_individual_signup_switchNervousSystemDisease)
    Switch nervousSystemDisease;
    @BindView(R.id.frg_individual_signup_switchPsychosis)
    Switch psychosis;
    @BindView(R.id.frg_individual_signup_buttonEBack)
    Button back;
    @BindView(R.id.frg_individual_signup_menu_submit)
    Button submit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_individual_signup_exclusion, container, false);
        ButterKnife.bind(this, view);

        getPresenter().startPresenter();
        return view;
    }

    public Switch getMentalDisability() {
        return mentalDisability;
    }

    public Switch getNeurologicalDisorder() {
        return neurologicalDisorder;
    }

    public Switch getHearingDeficiency() {
        return hearingDeficiency;
    }

    public Switch getVisualImpairment() {
        return visualImpairment;
    }

    public Switch getNervousSystemDisease() {
        return nervousSystemDisease;
    }

    public Switch getPsychosis() {
        return psychosis;
    }

    public Button getBack() {
        return back;
    }

    public Button getSubmit() {
        return submit;
    }
}