package br.com.leiamaisapp.leiamais.Views.Individual;

import android.support.v4.app.Fragment;

import br.com.leiamaisapp.leiamais.Common.MVPInterfaces;
import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualFragmentPresenter;
import br.com.leiamaisapp.leiamais.Presenters.Individual.MainWindowIndividualActivityPresenter;

/**
 * Created by erickson on 04/09/16.
 */
public abstract class IndividualFragment extends Fragment implements
        MVPInterfaces.FragmentRequires {

    private IndividualFragmentPresenter presenter;

    public MainWindowIndividualActivityPresenter getMActivityPresenter() {
        return presenter.getMActivityPresenter();
    }

    public void setMActivityPresenter(MainWindowIndividualActivityPresenter mActivityPresenter) {
        presenter.setMActivityPresenter(mActivityPresenter);
    }

    public IndividualFragmentPresenter getPresenter() {
        return presenter;
    }

    public void setPresenter(IndividualFragmentPresenter presenter) {
        this.presenter = presenter;
    }

}
