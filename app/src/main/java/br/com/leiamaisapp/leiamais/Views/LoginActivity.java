package br.com.leiamaisapp.leiamais.Views;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import br.com.leiamaisapp.database.Dao.IndividualDAO;
import br.com.leiamaisapp.leiamais.LeiaMaisApplication;
import br.com.leiamaisapp.leiamais.Presenters.LoginPresenter;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisActivity;
import br.com.leiamaisapp.restapi.rest.RestModel.GetIndividualsReturn;
import br.com.leiamaisapp.restapi.rest.service.RestService;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends LeiaMaisActivity {

    public LoginActivity() {
        setPresenter(new LoginPresenter(this));
    }

    @BindView(R.id.act_login_tv_mensagem_erro)
    TextView errorMessage;
    @BindView(R.id.act_login_et_email)
    EditText email;
    @BindView(R.id.act_login_et_password)
    EditText password;
    @BindView(R.id.act_login_tv_forgot_password)
    TextView forgotPassword;
    @BindView(R.id.act_login_btn_signin)
    Button signIn;

    public TextView getErrorMessage() {
        return errorMessage;
    }

    public EditText getEmail() {
        return email;
    }

    public EditText getPassword() {
        return password;
    }

    public TextView getForgotPassword() {
        return forgotPassword;
    }

    public Button getSignIn() {
        return signIn;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getPresenter().startPresenter();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        ((LoginPresenter) getPresenter()).onRestart();
    }

    @Override
    public void onBackPressed() {
        ((LoginPresenter) getPresenter()).onBackPressed();
    }
}
