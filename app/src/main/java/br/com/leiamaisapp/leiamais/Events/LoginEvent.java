package br.com.leiamaisapp.leiamais.Events;

import android.content.Intent;
import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.com.leiamaisapp.database.Dao.ProfessionalDAO;
import br.com.leiamaisapp.database.Model.Professional;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Exceptions.UserNotFoundException;
import br.com.leiamaisapp.leiamais.Exceptions.WrongPasswordException;
import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisActivity;
import br.com.leiamaisapp.leiamais.Views.Individual.MainWindowIndividualActivity;
import br.com.leiamaisapp.leiamais.Views.Professional.MainWindowProfessionalActivity;

/**
 * Created by erickson on 26/08/16.
 */
public class LoginEvent {
    private ProfessionalDAO dao;

    public LoginEvent() {
        dao = new ProfessionalDAO();
    }

    public Professional doSignIn(String email, String password) throws UserNotFoundException, WrongPasswordException {
        Professional professional = dao.findByEmail(email);


        if (professional == null)
            throw new UserNotFoundException();

        if (!professional.getPassword().equals(password))
            throw new WrongPasswordException();

        return professional;
    }

    public void openHome(LeiaMaisActivity activity, Long id) {
        Intent mIntent = new Intent(activity, MainWindowIndividualActivity.class);
        mIntent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, id);
        activity.startActivity(mIntent);
    }

    public void createAccount(LeiaMaisActivity activity) {
        Intent mIntent = new Intent(activity, MainWindowProfessionalActivity.class);
        activity.startActivity(mIntent);
    }

    public String textMD5(String s) {
        String md5 = null;
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(s.getBytes(), 0, s.length());

            md5 = new BigInteger(1, m.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return md5;
    }

}
