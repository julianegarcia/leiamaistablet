package br.com.leiamaisapp.leiamais.Tests;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Events.Common.CommonEvents;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisActivity;
import br.com.leiamaisapp.leiamais.Views.Individual.MainWindowIndividualActivity;

public class CompleteWord extends LeiaMaisActivity {


    Long idProfessional;
    Map<String, Map<String, String>> words;
    Map selectedWord;
    LinearLayout testCompleteWordTextArea;
    TextView syllableOne;
    TextView syllableTwo;
    TextView syllableThree;
    private final String replaceOptionId = "replaceWordOption";
    private final String[] wordsArray = {
            "ABELHA", "COELHO", "VARINHA", "RAINHA", "TROFEU", "ESCOLA"
    };
    private int actualWordIndex;
    private ImageButton right;
    private ImageButton wrong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_complete_word);
        idProfessional = getIntent().getLongExtra(Constants.PROFESSIONAL_EMAIL_TAG, 66);
        this.right = (ImageButton) findViewById(R.id.testButtonFailed);
        this.wrong = (ImageButton) findViewById(R.id.testButtonSucceeded);
        load();
    }

    public void load() {
        loadProperties();
        setSelectedWord();
        setImage();
        setSelectedWordInTextArea();
        setWordThatHaveToBeComplete();
        loadReplaceSyllable();
        setReplaceSyllable();
        setOnMoveSyllable();
    }


    public void setSelectedWordInTextArea() {
        testCompleteWordTextArea = (LinearLayout) findViewById(R.id.testCompleteWordTextArea);
    }


    private void setSelectedWord() {
        int randomWord = 0;
        do {
            randomWord = 0 + (int) (Math.random() * wordsArray.length);
        } while (actualWordIndex == randomWord);

        String word = wordsArray[randomWord];
        selectedWord = words.get(word);
        actualWordIndex = randomWord;
    }


    public void setImage() {
        ImageView wordImage = (ImageView) findViewById(R.id.testImageViewCompleteWordImage);
        wordImage.setImageURI(getImage());
    }

    public void setWordThatHaveToBeComplete() {
        String mainWord = (String) selectedWord.get("complete");
        String substituteSyllable = (String) selectedWord.get("subs");
        int indexOfSubstituteSyllable = mainWord.indexOf(substituteSyllable);
        int syllableLength = substituteSyllable.length();
        int wordLength = mainWord.length();
        syllableLength = indexOfSubstituteSyllable + syllableLength - 1;
        putWordInTextArea(mainWord, indexOfSubstituteSyllable, syllableLength);
    }

    private void putWordInTextArea(String mainWord, int whereSubstituteSyllableStay, int substituteSyllableLength) {
        int index = 0;
        boolean replaceDone = false;
        for (Character ch : mainWord.toCharArray()) {
            if (index <= substituteSyllableLength && index >= whereSubstituteSyllableStay) {
                index++;
                if (replaceDone)
                    continue;
                TextView letter = createReplaceTextView();
                addWordToTextArea(letter);
                replaceDone = true;
                continue;
            }
            TextView letter = createWordTextView(ch);
            addWordToTextArea(letter);
            index++;
        }
    }

    private TextView createReplaceTextView() {
        TextView letterView = new TextView(getApplicationContext());
        letterView.setTextSize(70);
        letterView.setTypeface(Typeface.create("sans-serif-condensed", Typeface.BOLD));
        letterView.setTextColor(Color.parseColor("#000000"));
        letterView.setBackgroundResource(R.drawable.border_black);
        letterView.setPadding(100, 0, 100, 0);
        letterView.setTag(replaceOptionId);
        letterView.setOnDragListener(new ChoiceDragListener());
        return letterView;
    }


    private TextView createWordTextView(Character letter) {
        TextView letterView = new TextView(getApplicationContext());
        letterView.setText(letter.toString());
        letterView.setTextSize(70);
        letterView.setTypeface(Typeface.create("sans-serif-condensed", Typeface.BOLD));
        letterView.setTextColor(Color.parseColor("#000000"));
        return letterView;
    }


    private void addWordToTextArea(TextView letter) {
        testCompleteWordTextArea.addView(letter);
    }


    public Uri getImage() {
        String image = (String) selectedWord.get("imageName");
        Uri imageUri = Uri.parse("android.resource://" + getPackageName() + "/drawable/" + image);
        return imageUri;
    }

    private void loadReplaceSyllable() {
        syllableOne = (TextView) findViewById(R.id.testTextViewSyllableOne);
        syllableTwo = (TextView) findViewById(R.id.testTextViewSyllableTwo);
        syllableThree = (TextView) findViewById(R.id.testTextViewSyllableThree);

    }


    private void setReplaceSyllable() {
        syllableOne.setText((String) selectedWord.get("wordOne"));
        syllableTwo.setText((String) selectedWord.get("wordTwo"));
        syllableThree.setText((String) selectedWord.get("wordThree"));
        syllableOne.setTextSize(70);
        syllableOne.setTypeface(Typeface.create("sans-serif-condensed", Typeface.BOLD));
        syllableTwo.setTextSize(70);
        syllableTwo.setTypeface(Typeface.create("sans-serif-condensed", Typeface.BOLD));
        syllableThree.setTextSize(70);
        syllableThree.setTypeface(Typeface.create("sans-serif-condensed", Typeface.BOLD));
        syllableOne.setVisibility(View.VISIBLE);
        syllableTwo.setVisibility(View.VISIBLE);
        syllableThree.setVisibility(View.VISIBLE);
    }


    private void setOnMoveSyllable() {
        syllableOne.setOnTouchListener(new ChoiceTouchListener());
        syllableThree.setOnTouchListener(new ChoiceTouchListener());
        syllableTwo.setOnTouchListener(new ChoiceTouchListener());
    }


    private final class ChoiceTouchListener implements View.OnTouchListener {
        @SuppressLint("NewApi")
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                //start dragging the item touched
                view.startDrag(data, shadowBuilder, view, 0);
                return true;
            } else {
                return false;
            }
        }
    }

    @SuppressLint("NewApi")
    private class ChoiceDragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    View view = (View) event.getLocalState();
                    TextView dropTarget = (TextView) v;
                    TextView dropped = (TextView) view;
                    view.setVisibility(View.INVISIBLE);
                    dropTarget.setPadding(30, 0, 30, 0);
                    dropped.setPadding(0, 0, 0, 0);
                    dropTarget.setText(dropped.getText().toString());
                    Object tag = dropTarget.getTag();
                    dropTarget.setTag(dropped.getId());
                    dropTarget.setOnDragListener(null);

//                    startActivity(new Intent(getApplicationContext(),EndAllTest.class));

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    public void restart(View view) {
        testCompleteWordTextArea.removeAllViews();
        load();
    }

    public void preview(View v) {
        startActivity(new Intent(this, Spell.class));
    }


    public void loadProperties() {
        words = new HashMap<String, Map<String, String>>();
        Map<String, String> asset1 = new HashMap<String, String>();
        asset1.put("imageName", "bee");
        asset1.put("wordOne", "LL");
        asset1.put("complete", "ABELHA");
        asset1.put("wordTwo", "NH");
        asset1.put("wordThree", "LH");
        asset1.put("subs", "LH");
        Map<String, String> asset2 = new HashMap<String, String>();
        asset2.put("imageName", "bunny");
        asset2.put("wordOne", "LL");
        asset2.put("complete", "COELHO");
        asset2.put("wordTwo", "NH");
        asset2.put("wordThree", "LH");
        asset2.put("subs", "LH");
        Map<String, String> asset3 = new HashMap<String, String>();
        asset3.put("imageName", "cup");
        asset3.put("complete", "TROFÉU");
        asset3.put("wordOne", "TRE");
        asset3.put("wordTwo", "TRO");
        asset3.put("wordThree", "TRU");
        asset3.put("subs", "TRO");
        Map<String, String> asset4 = new HashMap<String, String>();
        asset4.put("imageName", "magic_wand");
        asset4.put("complete", "VARINHA");
        asset4.put("wordOne", "LH");
        asset4.put("wordTwo", "NH");
        asset4.put("wordThree", "CH");
        asset4.put("subs", "NH");
        Map<String, String> asset5 = new HashMap<String, String>();
        asset5.put("imageName", "queen");
        asset5.put("complete", "RAINHA");
        asset5.put("wordOne", "LH");
        asset5.put("wordTwo", "NH");
        asset5.put("wordThree", "HN");
        asset5.put("subs", "NH");
        Map<String, String> asset6 = new HashMap<String, String>();
        asset6.put("imageName", "school");
        asset6.put("complete", "ESCOLA");
        asset6.put("wordOne", "LA");
        asset6.put("wordTwo", "LHA");
        asset6.put("wordThree", "LLA");
        asset6.put("subs", "LA");
        words.put("ABELHA", asset1);
        words.put("COELHO", asset2);
        words.put("TROFEU", asset3);
        words.put("VARINHA", asset4);
        words.put("RAINHA", asset5);
        words.put("ESCOLA", asset6);

    }


    public String getTestInfo() {
        return "Esse é o teste da soletração, nesse teste o individuo deve completar a palavra com a silába certa.";
    }

    public void info(View v) {
        Toast.makeText(getApplicationContext(), getTestInfo(), Toast.LENGTH_LONG).show();
    }

    public void right(View v) {
        saveTest(true);
    }

    public void wrong(View v) {
        saveTest(false);
    }

    public void saveTest(boolean hit) {
        Intent mIntent = new Intent(this, EndAllTest.class);
        mIntent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, idProfessional);
        startActivity(mIntent);
    }


    public void finishTest() {
        this.wrong.setEnabled(false);
        this.right.setEnabled(false);
        testCompleteWordTextArea.removeAllViews();
        Button nextButton = new Button(testCompleteWordTextArea.getContext());
        nextButton.setText(R.string.next);
        nextButton.setPadding(50, 50, 50, 50);
        nextButton.setGravity(Gravity.BOTTOM);
        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), CompleteWord.class));
            }
        });
        testCompleteWordTextArea.addView(nextButton);
    }


    @Override
    public void onBackPressed() {
        CommonEvents events = new CommonEvents(this);

        events.showConfirmationMessage(
                "Sair"
                , "Deseja mesmo terminar o teste?"
                , "Sim"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getBaseContext(), MainWindowIndividualActivity.class);
                        intent.putExtra(Constants.PROFESSIONAL_EMAIL_TAG, idProfessional);
                        startActivity(intent);
                    }
                }
                , "Não"
                , null
        );
    }
}