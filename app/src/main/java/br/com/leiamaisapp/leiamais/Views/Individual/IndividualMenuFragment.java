package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualMenuPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by erickson on 23/07/16.
 */
public class IndividualMenuFragment extends IndividualFragment {

    public IndividualMenuFragment() {
        setPresenter(new IndividualMenuPresenter(this));
    }

    @BindView(R.id.frg_individual_principal_menu_placeHolderImage)
    CircleImageView photo;
    @BindView(R.id.frg_individual_menu_name)
    TextView name;
    @BindView(R.id.frg_individual_principal_menu_apply_teste)
    Button applyTest;
    @BindView(R.id.frg_individual_principal_menu_personalData)
    Button personalData;
    @BindView(R.id.frg_individual_principal_menu_development)
    Button development;
    @BindView(R.id.frg_individual_principal_menu_exclusionCriteria)
    Button exclusionCriteria;
    @BindView(R.id.frg_individual_principal_menu_report)
    Button report;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_individual_menu, container, false);

        ButterKnife.bind(this, view);

        getPresenter().startPresenter();

        return view;
    }

    public ImageView getPhoto() {
        return photo;
    }

    public TextView getName() {
        return name;
    }

    public Button getApplyTest() {
        return applyTest;
    }

    public Button getPersonalData() {
        return personalData;
    }

    public Button getDevelopment() {
        return development;
    }

    public Button getExclusionCriteria() {
        return exclusionCriteria;
    }

    public Button getReport() {
        return report;
    }
}
