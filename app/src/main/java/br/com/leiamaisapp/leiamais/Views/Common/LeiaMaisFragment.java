package br.com.leiamaisapp.leiamais.Views.Common;

import android.support.v4.app.Fragment;

import br.com.leiamaisapp.leiamais.Presenters.Common.LeiaMaisPresenter;

/**
 * Created by erickson on 10/10/16.
 */

public abstract class LeiaMaisFragment extends Fragment {
    private LeiaMaisPresenter presenter;

    public LeiaMaisPresenter getPresenter() {
        return presenter;
    }

    public void setPresenter(LeiaMaisPresenter presenter) {
        this.presenter = presenter;
    }
}
