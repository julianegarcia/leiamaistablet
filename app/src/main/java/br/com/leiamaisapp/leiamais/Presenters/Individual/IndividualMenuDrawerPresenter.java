package br.com.leiamaisapp.leiamais.Presenters.Individual;

import android.view.View;
import android.widget.TextView;

import java.io.File;

import br.com.leiamaisapp.database.Model.Individual;
import br.com.leiamaisapp.database.Model.Professional;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Events.Common.ImageEvents;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualMenuDrawer;

/**
 * Created by erickson on 13/10/16.
 */

public class IndividualMenuDrawerPresenter extends IndividualFragmentPresenter {

    public IndividualMenuDrawerPresenter(IndividualMenuDrawer fragment) {
        this.fragment = fragment;
    }

    IndividualMenuDrawer fragment;

    @Override
    public void startPresenter() {
        setProfessionalPresentation();
        setAddIndividual();
        setSearchIndividual();
    }

    public void setProfessionalPresentation() {
        Professional professional = getMActivityPresenter().getProfessional();

        TextView professionalPresentation = fragment.getProfessionalPresentation();

        String text = String.format(
                professionalPresentation.getText().toString()
                , professional.getTitle()
                , professional.getName()
        );

        professionalPresentation.setText(text);

        ImageEvents events = new ImageEvents(fragment.getActivity());


        try {
            File file = new File(professional.getPhoto());
            events.setImageViewPhoto(
                    fragment.getProfessionalPhoto()
                    , file
            );
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void setAddIndividual() {
        fragment.getNewIndividual().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainWindowIndividualActivityPresenter mPresenter = getMActivityPresenter();
                mPresenter.changeMainWindowFragment(Constants.INDIVIDUAL_SIGN_UP_PERSONAL_DATA_TAG, false);
                mPresenter.changeMenuFragment(Constants.INDIVIDUAL_SIGN_UP_MENU_TAG);
                mPresenter.setIndividual(new Individual());
                mPresenter.closeDrawer();

            }
        });
    }

    public void setSearchIndividual() {
        fragment.getSearchIndividual().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMActivityPresenter().changeDrawerFragment(Constants.INDIVIDUAL_SEARCH_MENU_TAG);
            }
        });
    }
}
