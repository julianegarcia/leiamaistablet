package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualExclusionPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by erickson on 21/10/16.
 */

public class IndividualExclusionFragment extends IndividualFragment {

    public IndividualExclusionFragment() {
        setPresenter(new IndividualExclusionPresenter(this));
    }

    @BindView(R.id.frg_individual_exclusion_textViewMentalDisability)
    TextView mentalDisability;
    @BindView(R.id.frg_individual_exclusion_textViewNeurologicalDisorder)
    TextView neurologicalDisorder;
    @BindView(R.id.frg_individual_exclusion_textViewHearingDeficiency)
    TextView hearingDeficiency;
    @BindView(R.id.frg_individual_exclusion_textViewVisualImpairment)
    TextView visualImpairment;
    @BindView(R.id.frg_individual_exclusion_textViewNervousSystemDisease)
    TextView nervousSystemDisease;
    @BindView(R.id.frg_individual_exclusion_textViewPsychosis)
    TextView psychosis;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_individual_exclusion, container, false);
        ButterKnife.bind(this, view);
        getPresenter().startPresenter();
        return view;
    }

    public TextView getMentalDisability() {
        return mentalDisability;
    }

    public TextView getNeurologicalDisorder() {
        return neurologicalDisorder;
    }

    public TextView getHearingDeficiency() {
        return hearingDeficiency;
    }

    public TextView getVisualImpairment() {
        return visualImpairment;
    }

    public TextView getNervousSystemDisease() {
        return nervousSystemDisease;
    }

    public TextView getPsychosis() {
        return psychosis;
    }
}
