package br.com.leiamaisapp.leiamais.Presenters.Professional;

import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.leiamaisapp.database.Dao.ProfessionalDAO;
import br.com.leiamaisapp.database.Model.Professional;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Presenters.Common.FragmentPresenter;
import br.com.leiamaisapp.leiamais.Presenters.Common.LeiaMaisPresenter;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Common.DefaultMenuFragment;
import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisActivity;
import br.com.leiamaisapp.leiamais.Views.Professional.ProfessionalSignUpFragment;

/**
 * Created by erickson on 24/07/16.
 */
public class MainWindowProfessionalActivityPresenter extends LeiaMaisPresenter {

    private Professional professional;
    private LeiaMaisActivity activity;

    public MainWindowProfessionalActivityPresenter(LeiaMaisActivity activity) {
        super();
        this.activity = activity;
    }

    public Professional getProfessional() {
        return professional;
    }

    public void setProfessional(Professional professional) {
        this.professional = professional;
    }

    private Fragment menuFragment;
    private ProfessionalSignUpFragment mainFragment;
    private FragmentPresenter fragmentPresenter;

    @Override
    public void startPresenter() {

        Intent mIntent = activity.getIntent();

        menuFragment = new DefaultMenuFragment();
        mainFragment = new ProfessionalSignUpFragment();

        Long id = (Long) mIntent.getSerializableExtra(Constants.PROFESSIONAL_EMAIL_TAG);

        if (id != null) {
            ProfessionalDAO dao = new ProfessionalDAO();
            setProfessional(dao.findById(id));
        }

        fragmentPresenter = new FragmentPresenter();
        fragmentPresenter.changeFragments(activity,
                Constants.MENU_FRAGMENT,
                R.id.act_main_window_fragmentPrincipalMenu,
                menuFragment
        );

        fragmentPresenter.changeFragments(activity,
                Constants.MAIN_FRAGMENT,
                R.id.act_main_window_fragmentContent,
                mainFragment
        );
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mainFragment.getPresenter().onActivityResult(requestCode, resultCode, data);
    }
}
