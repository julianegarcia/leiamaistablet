package br.com.leiamaisapp.leiamais.Events.Individual;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;

/**
 * Created by erickson on 11/09/16.
 */
public class IndividualSingUpEvents {

    public void changeEditTextVisibility(CompoundButton button, EditText editText) {
        editText.setVisibility(button.isChecked() ? View.VISIBLE : View.INVISIBLE);
    }
}
