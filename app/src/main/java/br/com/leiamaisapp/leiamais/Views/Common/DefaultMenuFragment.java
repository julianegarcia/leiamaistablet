package br.com.leiamaisapp.leiamais.Views.Common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.leiamaisapp.leiamais.Presenters.Common.DefaultMenuPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.ButterKnife;

/**
 * Created by erickson on 10/10/16.
 */

public class DefaultMenuFragment extends LeiaMaisFragment {

    public DefaultMenuFragment() {
        setPresenter(new DefaultMenuPresenter(this));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_default_menu, container, false);

        ButterKnife.bind(this, view);

        getPresenter().startPresenter();

        return view;
    }
}
