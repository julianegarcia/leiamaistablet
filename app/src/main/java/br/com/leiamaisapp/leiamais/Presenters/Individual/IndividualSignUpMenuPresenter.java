package br.com.leiamaisapp.leiamais.Presenters.Individual;

import android.view.View;

import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Common.MVPInterfaces;
import br.com.leiamaisapp.leiamais.Events.Individual.IndividualMenuOptionEvents;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSignUpMenuFragment;

/**
 * Created by erickson on 08/09/16.
 */
public class IndividualSignUpMenuPresenter extends IndividualFragmentPresenter implements MVPInterfaces.IndividualMenuInterface {

    public IndividualSignUpMenuPresenter(IndividualSignUpMenuFragment fragment) {
        this.fragment = fragment;
    }

    private IndividualSignUpMenuFragment fragment;
    private IndividualMenuOptionEvents event;

    @Override
    public void startPresenter() {
        event = new IndividualMenuOptionEvents(this);
        setButtonPersonalData();
        setButtonDevelopment();
        setButtonTdha();
        setButtonExclusionCriteria();

        if (getIndividual().getTdhaTreatment() != null)
            getMActivityPresenter().hideTDHAButton(true);
    }

    private void setButtonPersonalData() {
        fragment.getPersonalData().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                event.onMenuOptionSelect(Constants.INDIVIDUAL_SIGN_UP_PERSONAL_DATA_TAG);
            }
        });
    }

    private void setButtonDevelopment() {
        fragment.getDevelopment().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                event.onMenuOptionSelect(Constants.INDIVIDUAL_SIGN_UP_PERSONAL_DEVELOPMENT_TAG);
            }
        });
    }

    private void setButtonTdha() {
        fragment.getTdha().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                event.onMenuOptionSelect(Constants.INDIVIDUAL_SIGN_UP_TDHA_TAG);
            }
        });
    }

    private void setButtonExclusionCriteria() {
        fragment.getExclusionCriteria().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                event.onMenuOptionSelect(Constants.INDIVIDUAL_SIGN_UP_EXCLUSION_TAG);
            }
        });
    }

    public void hideTDHAButton(boolean st) {
        fragment.getTdha().setVisibility(st ? View.INVISIBLE : View.VISIBLE);
    }
}
