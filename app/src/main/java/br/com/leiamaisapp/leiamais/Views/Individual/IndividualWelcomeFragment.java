package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualWelcomePresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by erickson on 18/09/16.
 */
public class IndividualWelcomeFragment extends IndividualFragment implements Serializable {

    public IndividualWelcomeFragment() {
        setPresenter(new IndividualWelcomePresenter(this));
    }

    @BindView(R.id.frg_individual_welcome_hello)
    TextView hello;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_individual_welcome, container, false);

        ButterKnife.bind(this, view);

        getPresenter().startPresenter();

        return view;
    }

    public TextView getHello() {
        return hello;
    }
}
