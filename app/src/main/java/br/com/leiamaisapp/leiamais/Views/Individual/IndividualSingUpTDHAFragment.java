package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualSingUpTDHAFragmentPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by erickson on 20/08/16.
 */
public class IndividualSingUpTDHAFragment extends IndividualFragment {

    public IndividualSingUpTDHAFragment() {
        setPresenter(new IndividualSingUpTDHAFragmentPresenter(this));
    }

    @BindView(R.id.frg_individual_signup_switchQuestion1)
    Switch question1;
    @BindView(R.id.frg_individual_signup_switchQuestion2)
    Switch question2;
    @BindView(R.id.frg_individual_signup_switchQuestion3)
    Switch question3;
    @BindView(R.id.frg_individual_signup_switchQuestion4)
    Switch question4;
    @BindView(R.id.frg_individual_signup_switchQuestion5)
    Switch question5;
    @BindView(R.id.frg_individual_signup_switchQuestion6)
    Switch question6;
    @BindView(R.id.frg_individual_signup_switchQuestion7)
    Switch question7;
    @BindView(R.id.frg_individual_signup_switchQuestion8)
    Switch question8;
    @BindView(R.id.frg_individual_signup_switchQuestion9)
    Switch question9;
    @BindView(R.id.frg_individual_signup_switchQuestion10)
    Switch question10;
    @BindView(R.id.frg_individual_signup_switchQuestion11)
    Switch question11;
    @BindView(R.id.frg_individual_signup_switchQuestion12)
    Switch question12;
    @BindView(R.id.frg_individual_signup_switchQuestion13)
    Switch question13;
    @BindView(R.id.frg_individual_signup_switchQuestion14)
    Switch question14;
    @BindView(R.id.frg_individual_signup_switchQuestion15)
    Switch question15;
    @BindView(R.id.frg_individual_signup_switchQuestion16)
    Switch question16;
    @BindView(R.id.frg_individual_signup_switchQuestion17)
    Switch question17;
    @BindView(R.id.frg_individual_signup_switchQuestion18)
    Switch question18;
    @BindView(R.id.frg_individual_signup_buttonTBack)
    Button back;
    @BindView(R.id.frg_individual_signup_buttonTNext)
    Button next;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_individual_signup_tdah, container, false);
        ButterKnife.bind(this, view);

        getPresenter().startPresenter();
        return view;
    }

    public Switch getQuestion1() {
        return question1;
    }

    public Switch getQuestion2() {
        return question2;
    }

    public Switch getQuestion3() {
        return question3;
    }

    public Switch getQuestion4() {
        return question4;
    }

    public Switch getQuestion5() {
        return question5;
    }

    public Switch getQuestion6() {
        return question6;
    }

    public Switch getQuestion7() {
        return question7;
    }

    public Switch getQuestion8() {
        return question8;
    }

    public Switch getQuestion9() {
        return question9;
    }

    public Switch getQuestion10() {
        return question10;
    }

    public Switch getQuestion11() {
        return question11;
    }

    public Switch getQuestion12() {
        return question12;
    }

    public Switch getQuestion13() {
        return question13;
    }

    public Switch getQuestion14() {
        return question14;
    }

    public Switch getQuestion15() {
        return question15;
    }

    public Switch getQuestion16() {
        return question16;
    }

    public Switch getQuestion17() {
        return question17;
    }

    public Switch getQuestion18() {
        return question18;
    }

    public Button getBack() {
        return back;
    }

    public Button getNext() {
        return next;
    }
}