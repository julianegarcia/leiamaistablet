package br.com.leiamaisapp.leiamais.Presenters.Individual;

import java.util.Date;

import br.com.leiamaisapp.database.Model.Individual;
import br.com.leiamaisapp.leiamais.Common.DateValidator;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualPersonalDataFragment;

/**
 * Created by erickson on 21/10/16.
 */

public class IndividualPersonalDataPresenter extends IndividualFragmentPresenter {

    public IndividualPersonalDataPresenter(IndividualPersonalDataFragment fragment) {
        this.fragment = fragment;
    }

    private IndividualPersonalDataFragment fragment;

    @Override
    public void startPresenter() {
        setTextViews();
    }

    private void setTextViews() {
        Individual individual = getIndividual();

        fragment.getAge().setText(getIndividualAge());
        fragment.getGender().setText(individual.getSex());
        fragment.getCpf().setText(individual.getCpf() != null ? individual.getCpf() : "Não");
        fragment.getFamiliarHistoric().setText(individual.getFamilyHistoric() != null ? individual.getFamilyHistoric() : "Não");
        fragment.getConsanguinityHistoric().setText(individual.getConsanguinityHistoric() != null ? individual.getConsanguinityHistoric() : "Não");
        fragment.getDesiredGestation().setText(individual.getDesiredGestation() != null ? individual.getDesiredGestation() : "Não");
        fragment.getNeonatalComplications().setText(individual.getNeonatalComplications() != null ? individual.getNeonatalComplications() : "Não");
        fragment.getMaternalHistory().setText(individual.getMaternalHistoric() != null ? individual.getMaternalHistoric() : "Não");
        fragment.getComorbidities().setText(individual.getComorbidities() != null ? individual.getComorbidities() : "Não");
        fragment.getPregnancyComplications().setText(individual.getPregnancyComplications() != null ? individual.getPregnancyComplications() : "Não");
    }

    private String getIndividualAge() {
        Date birthday = getIndividual().getBirthday();

        Date today = new Date();

        return String.valueOf(new DateValidator().getDiffYears(birthday, today));
    }
}
