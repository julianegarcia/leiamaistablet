package br.com.leiamaisapp.leiamais.Presenters.Common;

/**
 * Created by erickson on 24/07/16.
 */
public abstract class LeiaMaisPresenter {
    public LeiaMaisPresenter() {
    }

    public abstract void startPresenter();
}
