package br.com.leiamaisapp.leiamais.Views.Individual;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import br.com.leiamaisapp.leiamais.Presenters.Individual.IndividualSignUpMenuPresenter;
import br.com.leiamaisapp.leiamais.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by erickson on 23/07/16.
 */
public class IndividualSignUpMenuFragment extends IndividualFragment {

    public IndividualSignUpMenuFragment() {
        setPresenter(new IndividualSignUpMenuPresenter(this));
    }

    @BindView(R.id.frg_individual_signup_menu_personalData)
    Button personalData;
    @BindView(R.id.frg_individual_signup_menu_development)
    Button development;
    @BindView(R.id.frg_individual_signup_menu_tdha)
    Button tdha;
    @BindView(R.id.frg_individual_signup_menu_exclusionCriteria)
    Button exclusionCriteria;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_individual_signup_menu, container, false);

        ButterKnife.bind(this, view);

        getPresenter().startPresenter();

        return view;
    }

    public Button getPersonalData() {
        return personalData;
    }

    public Button getDevelopment() {
        return development;
    }

    public Button getTdha() {
        return tdha;
    }

    public Button getExclusionCriteria() {
        return exclusionCriteria;
    }

}
