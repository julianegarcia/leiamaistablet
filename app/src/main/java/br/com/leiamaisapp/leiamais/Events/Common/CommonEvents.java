package br.com.leiamaisapp.leiamais.Events.Common;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

/**
 * Created by erickson on 11/09/16.
 */
public class CommonEvents {
    Activity activity;

    public CommonEvents(Activity activity) {
        this.activity = activity;
    }

    public void showMessage(String message) {
        Toast.makeText(
                activity,
                message,
                Toast.LENGTH_SHORT
        ).show();
    }

    public void showConfirmationMessage(String title, String message,
                                        String buttonPositive,
                                        DialogInterface.OnClickListener listenerPositive,
                                        String buttonNegative,
                                        DialogInterface.OnClickListener listenerNegative) {

        AlertDialog.Builder adBuilder = new AlertDialog.Builder(activity);

        adBuilder.setTitle(title);
        adBuilder.setMessage(message);
        adBuilder.setPositiveButton(buttonPositive, listenerPositive);
        adBuilder.setNegativeButton(buttonNegative, listenerNegative);
        adBuilder.show();
    }
}
