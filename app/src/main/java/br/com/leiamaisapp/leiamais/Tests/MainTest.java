package br.com.leiamaisapp.leiamais.Tests;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import br.com.leiamaisapp.database.Dao.IndividualDAO;
import br.com.leiamaisapp.database.Dao.ProfessionalDAO;
import br.com.leiamaisapp.database.Model.Individual;
import br.com.leiamaisapp.database.Model.Professional;
import br.com.leiamaisapp.database.Model.Session;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Common.LeiaMaisActivity;

public class MainTest extends LeiaMaisActivity {

    private Session session;
    private Long idProfessional;
    private Long idIndividual;

    Professional professional;
    Individual individual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_main);

        Intent mIntent = getIntent();

        idProfessional = mIntent.getLongExtra(Constants.PROFESSIONAL_EMAIL_TAG, 0l);
        idIndividual = mIntent.getLongExtra(Constants.INDIVIDUAL_PERSONAL_DATA_TAG, 0l);

        if (idIndividual == 0 || idProfessional == 0) {

        }

        ProfessionalDAO daop = new ProfessionalDAO();

        professional = daop.findById(idProfessional);

        IndividualDAO daoi = new IndividualDAO();

        individual = daoi.findById(idIndividual);

        session = new Session();
        session.setIdProfessional(professional.getIdBase());
        session.setIdIndividual(individual.getIdBase());
    }

    public void startTests(View v) {
        startActivity(new Intent(this, Alphabet.class));
    }

    public Session getSession() {
        return session;
    }

    public Long getIdProfessional() {
        return idProfessional;
    }

    public Long getIdIndividual() {
        return idIndividual;
    }

    public Professional getProfessional() {
        return professional;
    }

    public Individual getIndividual() {
        return individual;
    }
}
