package br.com.leiamaisapp.leiamais.Presenters.Individual;

import br.com.leiamaisapp.database.Model.IndividualExclusion;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualExclusionFragment;

/**
 * Created by erickson on 28/10/16.
 */
public class IndividualExclusionPresenter extends IndividualFragmentPresenter {

    public IndividualExclusionPresenter(IndividualExclusionFragment fragment) {
        this.fragment = fragment;
    }

    IndividualExclusionFragment fragment;

    @Override
    public void startPresenter() {
        setTextViews();
    }

    private void setTextViews() {
        IndividualExclusion exclusion = getIndividual().getIndividualExclusion();

        fragment.getMentalDisability().setText(exclusion.getMentalDisability() ? "Sim" : "Não");
        fragment.getNeurologicalDisorder().setText(exclusion.getNeurologicalDisorder() ? "Sim" : "Não");
        fragment.getHearingDeficiency().setText(exclusion.getHearingDeficiency() ? "Sim" : "Não");
        fragment.getVisualImpairment().setText(exclusion.getVisualImpairment() ? "Sim" : "Não");
        fragment.getNervousSystemDisease().setText(exclusion.getNervousSystemDisease() ? "Sim" : "Não");
        fragment.getPsychosis().setText(exclusion.getPsychosis() ? "Sim" : "Não");
    }
}
