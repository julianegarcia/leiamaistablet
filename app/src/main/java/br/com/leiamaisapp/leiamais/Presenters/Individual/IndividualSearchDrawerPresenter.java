package br.com.leiamaisapp.leiamais.Presenters.Individual;

import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.com.leiamaisapp.database.Dao.IndividualDAO;
import br.com.leiamaisapp.database.Model.Individual;
import br.com.leiamaisapp.leiamais.Common.Constants;
import br.com.leiamaisapp.leiamais.Common.HMAux;
import br.com.leiamaisapp.leiamais.Events.Individual.IndividualSearchDrawerEvents;
import br.com.leiamaisapp.leiamais.R;
import br.com.leiamaisapp.leiamais.Views.Individual.IndividualSearchDrawer;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.RealmResults;

/**
 * Created by erickson on 09/10/16.
 */

public class IndividualSearchDrawerPresenter extends IndividualFragmentPresenter {

    public IndividualSearchDrawerPresenter(IndividualSearchDrawer fragment) {
        this.fragment = fragment;
        events = new IndividualSearchDrawerEvents(this);
    }

    @Override
    public void setIndividual(Individual individual) {
        super.setIndividual(individual);
    }

    private IndividualSearchDrawer fragment;

    private IndividualSearchDrawerEvents events;

    private CustomAdapter adapter;

    @Override
    public void startPresenter() {
        loadIndividualsList();
        setSearchButton();
        setGoBackButton();
    }

    public void loadIndividualsList() {
        fragment.getIndividualsList().setLayoutManager(
                new LinearLayoutManager(fragment.getActivity(), LinearLayoutManager.VERTICAL, false));
        fragment.getIndividualsList().setAdapter(getIndividualsAdapter(null));
        fragment.notifyDataSetChanged();
    }

    private CustomAdapter getIndividualsAdapter(@Nullable String letter) {
        if (adapter == null)
            adapter = new CustomAdapter();
        adapter.swap(getIndividuals(letter));
        return adapter;
    }

    private List<HMAux> getIndividuals(@Nullable String letter) {

        List<HMAux> individuals = new ArrayList<>();

        IndividualDAO dao = new IndividualDAO();

        RealmResults<Individual> results = dao.findAll(letter);

        for (Individual ind : results) {
            HMAux item = new HMAux();
            item.put(HMAux.ID, String.valueOf(ind.getId()));
            item.put(HMAux.KEY_01, ind.getPhoto());
            item.put(HMAux.KEY_02, ind.getName());

            Log.e("idBase", ind.getIdBase());

            individuals.add(item);
        }

        return individuals;
    }
    private List<HMAux> getIndividualsByName(@Nullable String name) {

        List<HMAux> individuals = new ArrayList<>();

        IndividualDAO dao = new IndividualDAO();

        RealmResults<Individual> results = dao.findAllByName(name);

        for (Individual ind : results) {
            HMAux item = new HMAux();
            item.put(HMAux.ID, String.valueOf(ind.getId()));
            item.put(HMAux.KEY_01, ind.getPhoto());
            item.put(HMAux.KEY_02, ind.getName());

            individuals.add(item);
        }

        return individuals;
    }

    private void setSearchButton() {
        fragment.getSearch().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = fragment.getSearchByName().getText().toString();

                swapIndividualsListByName(name);
            }
        });
    }

    private void setGoBackButton() {
        fragment.getGoBack().setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getMActivityPresenter().changeDrawerFragment(Constants.INDIVIDUAL_MENU_TAG);
                    }
                }
        );
    }

    public void swapIndividualsList(String letter) {
        adapter.swap(getIndividuals(letter));
        fragment.notifyDataSetChanged();
    }

    public void swapIndividualsListByName(String name) {
        adapter.swap(getIndividualsByName(name));
        fragment.notifyDataSetChanged();
    }

    class CustomAdapter extends RecyclerView.Adapter {

        List<HMAux> data;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(fragment.getActivity()).inflate(R.layout.drawer_individual_search_cell, parent, false);

            CustomViewHolder holder = new CustomViewHolder(view);

            return holder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            CustomViewHolder mHolder = (CustomViewHolder) holder;

            HMAux ind = data.get(position);

            mHolder.setId(ind.get(HMAux.ID));

            mHolder.drw_search_name.setText(ind.get(HMAux.KEY_02));

            File file = null;
            try {
                file = new File((data.get(position).get(HMAux.KEY_01)));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Picasso.with(fragment.getActivity()).load(file)
                    .placeholder(R.drawable.user_placehoder)
                    .error(R.drawable.user_placehoder)
                    .into(mHolder.drw_search_photo);
        }

        @Override
        public int getItemCount() {
            return data == null ? 0 : data.size();
        }

        public void swap(List<HMAux> nData) {

            if (this.data == null)
                data = nData;
            else {
                data.clear();
                data.addAll(nData);
            }

            notifyDataSetChanged();
        }
    }

    class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final CircleImageView drw_search_photo;
        final TextView drw_search_name;

        private String id;

        public void setId(String id) {
            this.id = id;
        }

        public CustomViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            drw_search_photo = (CircleImageView) itemView.findViewById(R.id.drw_search_photo);
            drw_search_name = (TextView) itemView.findViewById(R.id.drw_search_name);
        }

        @Override
        public void onClick(View v) {
            events.chooseIndividual(id);
        }
    }
}
