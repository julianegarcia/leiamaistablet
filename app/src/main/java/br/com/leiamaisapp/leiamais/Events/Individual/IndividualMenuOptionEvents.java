package br.com.leiamaisapp.leiamais.Events.Individual;

import br.com.leiamaisapp.leiamais.Common.MVPInterfaces;

/**
 * Created by erickson on 08/09/16.
 */
public class IndividualMenuOptionEvents {
    private MVPInterfaces.IndividualMenuInterface presenter;

    public IndividualMenuOptionEvents(MVPInterfaces.IndividualMenuInterface presenter) {
        this.presenter = presenter;
    }

    public void onMenuOptionSelect(String option) {
        if (presenter.getMActivityPresenter().getMainIndividualFragment().getPresenter().saveIndividual()) // Save the Individual changes on the current fragment
            presenter.getMActivityPresenter().changeMainWindowFragment(option, true);
    }
}
